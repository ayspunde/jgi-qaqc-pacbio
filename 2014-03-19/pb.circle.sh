#!/bin/bash

## needed before staring hacksaw, run JKH's circularization in the cluster (high-mem)
# assuming checked for duplications: pb.prelim_qc.sh
# bash /global/projectb/sandbox/rqc/ayspunde/pacbio/DEFAULT/pb.circle.sh

# ayspunde 2014-01-27

####################################################

# !! -- make sure deduplication was completed -- !!

####################################################

CLEAN_DIR=$(realpath `pwd`/..)
DIRTY_DIR=$(echo $CLEAN_DIR | sed  -e "s/-cleanP4.*//")
PB_JOB_ID=$(echo $(basename $DIRTY_DIR) | sed 's/^0//')
seqN=$( basename $(realpath $CLEAN_DIR/..))

date
printf "Starting circularization:\nTHIS_DIR=%s\n\n" $CLEAN_DIR

TRIM_AND_CIRCLE='/usr/common/jgi/qaqc/jgi-qaqc/detectCircle/trimAndTopologyUsingPb.py'
CIRCLE_INPUT=$CLEAN_DIR/'prelimQC/polished_assembly.deg.dedup.fasta'
FILT_READS_FASTA=$CLEAN_DIR/data/filtered_subreads.fasta
ls -al $FILT_READS_FASTA $CIRCLE_INPUT 

# $TRIM_AND_CIRCLE -r $CIRCLE_INPUT -f $FILT_READS_FASTA
if [[ ! -e $FILT_READS_FASTA || ! -e $CIRCLE_INPUT  ]]; then
	printf "Can't start circularization: missing files\nTHIS_DIR=%s\n" $CLEAN_DIR
	echo "filtered_fasta: $(ls -l $FILT_READS_FASTA)"
	echo "deduped assembly: $(ls -l $CIRCLE_INPUT)"
	date
	exit 1
else
	printf "Qsub-ing circularization:\nTHIS_DIR=%s\n\n" $CLEAN_DIR
	date
	qsub -S /bin/bash -cwd -j yes -w e -b yes \
	-l h_rt=4:00:00 -l ram.c=42G -N circ$seqN -P prok-assembly.p \
	"$TRIM_AND_CIRCLE -r $CIRCLE_INPUT -f $FILT_READS_FASTA" 
fi





