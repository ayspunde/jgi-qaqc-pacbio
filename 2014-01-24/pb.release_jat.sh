#!/bin/bash

# ayspunde 2014-01-27

# This is just a template for pre-release activities, no plans to script this

# - assuming sitting in ./final
#>  bash /global/u1/a/ayspunde/work/.myScripts/PacBio/pb.hacklinks.sh
# already run:
# raw.h5.reads.tar.gz
# filtered.subreads.fastq.gz
# error.corrected.reads.fastq.gz
# QC.finalReport.pdf - same as for other product types
# QD.finalReport.txt  - same as for other product types
# final.assembly.fasta - not zipped, since we haven't before
# final.assembly.topology.txt


# # cd ..
# # /global/projectb/sandbox/rqc/prod/pipelines/hacksaw/1.2.0/bin/hacksaw_pdf.pl -pp > project_desc_file.txt
# # /global/projectb/sandbox/rqc/prod/pipelines/hacksaw/1.2.0/bin/hacksaw_pdf.pl project_desc_file.txt 2>&1 >/dev/null
# # /global/projectb/sandbox/rqc/prod/pipelines/hacksaw/DEFAULT/bin/hacksaw_pdf.pl -pp > project_desc_file.txt
# # /global/projectb/sandbox/rqc/prod/pipelines/hacksaw/DEFAULT/bin/hacksaw_pdf.pl project_desc_file.txt 2>&1 >/dev/null
# #
mkdir .fasta && mv *fasta ./.fasta
mkdir .old && mv *old ./.old
rm metadata.json
mv *.o* ./log ; mv *.po* ./log ; mv *log ./log
cp hacksaw_stats.txt hacksaw_stats.txt.copy
cat hacksaw_stats.txt | sed 's/assemblyVersion.row.hgap=/assemblyVersion.row.hgap=2.0.1/' > hacksaw_stats.txt.tmp
mv hacksaw_stats.txt.tmp hacksaw_stats.txt
/global/projectb/sandbox/rqc/prod/pipelines/hacksaw/1.2.0/bin/hacksaw_pdf.pl project_desc_file.txt 2>&1 >/dev/null
proj_id_token='project.JGI Sequencing Project ID='
PROJ_ID=$(cat project_desc_file.txt | grep "$proj_id_token" | sed "s/$proj_id_token//")
submitanalysis.py $PROJ_ID | tee -a submit.py.log
chgrp -R genome `pwd` &&  chmod -R 775 `pwd` &


#
# # ## @@ ##
9) QC Report
Assembly size = GGG Mb; SSS contigs; 
SILVA:
Collab_16S:
Single-copy: mean=1, median=1

#
# # cd ..   # from the run dir
# # module load jamo/prod qaqc
# # submitanalysis.py $PROJ_ID
chgrp -R genome `pwd` &&  chmod -R 775 `pwd` &

# # # check content of metadata.json

# # --  jat import <template name> .
# # jat import microbial_isolate_improved_pacbio `pwd`; date