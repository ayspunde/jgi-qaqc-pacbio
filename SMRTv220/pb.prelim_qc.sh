#!/bin/bash -l

# assuming in the project (NNN-cleanP4)/prelimQC & hgap3 assembly/file structure
#> bash /global/projectb/sandbox/rqc/ayspunde/pacbio/SMRTv220/pb.prelim_qc.sh 

# ayspunde SMRTv220

## !! 
## -- deduplication currently manual process, make sure that 
## $ASM_dedup is ready before processing
## !!


########################################################################
## --- WHAT : define what to do: helpful for one-off/partials ---##
##   0=skip 1=do 2=megablast only=do special case 16S
doCopyAsmFasta=1
doNucmer=1
doMegablast=1
doCheckSpikein=1	
doCopyNumber=1
##
########################################################################
##


##
## SET ENV
##
module load mummer 
module load qaqc/prod
module load jigsaw 

VERSION2RUN='SMRTv220'
PATH2SCIPTS="/global/projectb/sandbox/rqc/ayspunde/pacbio/$VERSION2RUN"
P4_SPIKE_IN="$PATH2SCIPTS/../ref/spike/4kb_Control.fasta"
BVER='211'
BLASR2USE="$PATH2SCIPTS/../BLASR/blasr_$BVER/blasr"
SINGLE_COPY_PL='/projectb/sandbox/rqc/prod/pipelines/external_tools/singleGeneCopy/estimateGenomeCompleteness.pl'
SINGLE_COPY_HMM_BACTERIA='/projectb/sandbox/rqc/prod/pipelines/external_tools/singleGeneCopy/data/bacteria_hmms'
LLLL_16SREFSEQ='/global/projectb/sandbox/rqc/qcdb/collab16s/collab16s.fa'
# LLLL_16SREFSEQ='/global/projectb/scratch/kmdw/collab16s/collab16s.fa'
CUSTOM_16SREFSEQ='/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc41/s2408/029434-cleanP4/prelimQC/collab16S/XUHS.fa'
SILVA_16SEQ='/global/dna/shared/rqc/ref_databases/qaqc/databases/silva/2011.04.silva/SSURef_106_tax_silva.fasta'
REFSEQ_MICROBIAL='/global/dna/shared/rqc/ref_databases/ncbi/CURRENT/refseq.microbial/refseq.microbial'

CORRECTED_FASTA='corrected.fasta'
FILTERED_FASTA='filtered_subreads.fasta'
FILTERED_FASTA_NOCONTROL='no_control_filtered_subreads.fasta'
ASM='polished_assembly.fasta'
DEG='celera-assembler.deg.fasta'
ASM_deg='polished_assembly.deg.fasta'
ASM_dedup='polished_assembly.deg.dedup.fasta'

	
##
## -- WHERE we are
##
WORK_DIR=$(pwd);
seqID=$( basename $(realpath ./../..))
runID=$(echo $(basename $(realpath ./..)) | sed 's/-cleanP4//')


##
## DO
##
## -- link/copy assembly
##
ASM='polished_assembly.fasta'
ASM_dedup='polished_assembly.dedup.fasta'
if [[ $doCopyAsmFasta -eq '1' ]]; then
	if [ -e $WORK_DIR/../data/$ASM ]; then
		cp $WORK_DIR/../data/$ASM ./$ASM
	elif [ -e $WORK_DIR/../data/$ASM.gz ]; then
		gunzip -c $WORK_DIR/../data/$ASM.gz > ./$ASM
	else
		printf '\n\nERROR: no %s in DIR %s\n\n' $ASM $(realpath ..) 
		exit 1
	fi
	cat $ASM > $ASM_dedup
fi

##
## checking for duplications: mummer
## 
if [[ $doNucmer -eq '1' ]]; then
	csv_FN=nucmer.coord.$seqID.$runID.csv
	png_FN=nucmer.$seqID.$runID.png
	nucmer $ASM_dedup $ASM_dedup
	mummerplot -l --color -t png out.delta
	mv out.png $png_FN
	# S1 ,E1 ,S2 ,E2 ,LEN1 ,LEN2 ,'%_IDY' ,LEN_R ,LEN_Q ,COV_R ,COV_Q ,TAG1,TAG2
	printf ",S1 ,E1 ,S2 ,E2 ,LEN1 ,LEN2 ,PCT_IDY ,LEN_R ,LEN_Q ,COV_R ,COV_Q ,TAG1 ,TAG2\n" > $csv_FN
	show-coords -r -c -l out.delta >> $csv_FN
fi

##
## manual de-duplication after this step
##


##
## megablast: SILVA and Collab_16S '1'=run standard set of 16S, '2'= custom, one-off sequence
##

## module load jigsaw/2.3.2
if [[ $doMegablast -eq '1' ]]; then
	# on the node
	run_blast.pl -db $LLLL_16SREFSEQ  $ASM_dedup
	# # on the cluster
	# qsub -S /bin/bash -cwd -j yes -w e -b yes \
		# -l h_rt=1:00:00 -l ram.c=8G  -N blast$seqID -P prok-assembly.p \
		# "module load jigsaw  && run_blast.pl -db $SILVA_16SEQ  $ASM_dedup" 
	# qsub -S /bin/bash -cwd -j yes -w e -b yes \
		# -l h_rt=1:00:00 -l ram.c=8G  -N blast$seqID -P prok-assembly.p \
		# "module load jigsaw  && run_blast.pl -db $REFSEQ_MICROBIAL  $ASM_dedup" 	
fi
if [[ $doMegablast -eq '2' ]]; then
	run_blast.pl -db $CUSTOM_16SREFSEQ  $ASM_dedup
fi
	
##
## check that P4 spike-in were cleared
##
## do not check clean*h5, blasr ignores qFlag (set to zero by cleaner.py) 
## - and therefore detecting spike-in even in clean*h5s
if [[ $doCheckSpikein -eq '1'  ]]; then
	ALIGN_SUMM='spike.align2corr.txt'
	doAlign=1
	if [ -e $ALIGN_SUMM ]; then
		printf 'skip align2spike: %s already exists \n' $ALIGN_SUMM
		doAlign=0
	fi
	FASTA2CHECK="$(pwd)/../data2/$CORRECTED_FASTA"	
	if [ ! -e  $FASTA2CHECK ]; then
		printf 'skip align2spike: no input FASTA: %s \n' $FASTA2CHECK
		doAlign=0
	fi
	if [ $doAlign -eq 1 ]; then
		# "$BLASR2USE $FASTA2CHECK $P4_SPIKE_IN -bestn 1 > $ALIGN_SUMM"
		qsub -S /bin/bash -cwd -j yes -w e -b yes \
		-l h_rt=12:00:00 -l ram.c=8G  -N p4Corr$seqID -P prok-assembly.p \
		"$BLASR2USE $FASTA2CHECK $P4_SPIKE_IN -bestn 1 > $ALIGN_SUMM" 
	fi
	
	ALIGN_SUMM='spike.align2filt.txt'
	doAlign=1
	FASTA2CHECK="$(pwd)/../data2/$FILTERED_FASTA_NOCONTROL"
	if [ ! -e  $FASTA2CHECK ]; then
		printf 'skip align2spike: no input FASTA: %s \n' $FASTA2CHECK
		doAlign=0
	fi
	if [ $doAlign -eq 1 ]; then
		# $BLASR2USE $FASTA2CHECK $P4_SPIKE_IN -bestn 1 -header > $ALIGN_SUMM
		qsub -S /bin/bash -cwd -j yes -w e -b yes \
		-l h_rt=12:00:00 -l ram.c=40G  -N p4Filt$seqID -P prok-assembly.p \
		"$BLASR2USE $FASTA2CHECK $P4_SPIKE_IN -bestn 1 > $ALIGN_SUMM" 
	fi
fi

##
## check for single-copy
## 
if [[ $doCopyNumber -eq '1' ]]; then
	SINGLE_COPY_LIST_BACTERIA=$(pwd)/'singleCopyGeneList.bacteria.txt'
	SINGLE_COPY_SUMM=$(pwd)/'summary.bacteria.txt'
	qsub -S /bin/bash -cwd -j yes -w e -b yes \
	-l h_rt=12:00:00 -l ram.c=40G  -N copyN$seqID -P prok-assembly.p \
	"module load hmmer && $SINGLE_COPY_PL --header -countFile $SINGLE_COPY_LIST_BACTERIA $SINGLE_COPY_HMM_BACTERIA $ASM_dedup > $SINGLE_COPY_SUMM"
fi
