#!/bin/bash

# ayspunde 2014-01-24

# driver for pb.*.sh : looping over list of projects specified by jName, jID
## bash /global/u1/a/ayspunde/work/.myScripts/PacBio/2014-01-24/pb.DRIVER.sh

## change these:
# jName - my own ID(s) for the organism (derived from JIRA ticket N); free-form
# jID - jobID from SMRT Portal, exact

## for Jasmin
jName=(s1013 s1042 s1069)
jID=(  27693 27515 27518)


# PACBIO_LOCAL is your working dir
PACBIO_PORTAL='/global/projectb/shared/pacbio/jobs'
# -- PACBIO_LOCAL is your working dir -- #
# PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc'
PACBIO_LOCAL='/global/projectb/scratch/jasmynp/PacBio/Seqqc'

VERSION2RUN='/global/u1/a/ayspunde/work/.myScripts/PacBio/2014-01-24'
# VERSION2RUN='/global/u1/a/ayspunde/work/.myScripts/PacBio/DEFAULT'

things2do=(cleanP4)
# things2do=(cleanP4 final)

for ((i=0; i < ${#jName[@]} ; i++)); do
	LOC_NAME=${jName[$i]}
	PB_JOB_ID=${jID[$i]}
	LOC_DIR_DIRTY=$PACBIO_LOCAL/$LOC_NAME/0$PB_JOB_ID
	LOC_DIR_CLEAN=$LOC_DIR_DIRTY-cleanP4
	printf "%d\n%d\n%s\n%s\n" $i $PB_JOB_ID $LOC_DIR_DIRTY $LOC_DIR_CLEAN
	if [ ! -d $LOC_DIR_CLEAN ]; then 
		mkdir -m 775 $LOC_DIR_CLEAN
	fi
	cd $LOC_DIR_CLEAN
	for ((j=0; j < ${#things2do[@]} ; j++)); do
		todo=${things2do[$j]}
		echo $todo
		case $todo in
		'cleanP4')
			SCRIPT2RUN=$VERSION2RUN/pb.cleanp4.sh
			WORK_DIR=`pwd`	
			if [ -d $WORK_DIR ]; then
				cd $WORK_DIR; pwd 
				$SCRIPT2RUN
			else
				printf "\n\n --!!-- Not a valid directory: %s; skip --??-- \n\n" "$WORK_DIR"
				continue
			fi
			;;
		'final')
			SCRIPT2RUN=$VERSION2RUN/pb.hacklinks.sh
			WORK_DIR=`pwd`/final
			if [ -d $WORK_DIR ]; then
				cd $WORK_DIR; pwd 
				$SCRIPT2RUN
			else
				printf "\n\n --!!-- Not a valid directory: %s; skip --??-- \n\n" "$WORK_DIR"
				continue
			fi
			;;
		*)
			printf "\n\n -- !! -- No workflow for case: %s -- ?? -- \n\n" $todo
			;;
		esac
	done
	cd $PACBIO_LOCAL
done
