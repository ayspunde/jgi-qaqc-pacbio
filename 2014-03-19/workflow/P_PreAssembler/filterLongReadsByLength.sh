#!/bin/bash

########### TASK metadata #############
# Task            : filterLongReadsByLength
# Module          : P_PreAssembler
# TaskType        : None
# URL             : task://028069/P_PreAssembler/filterLongReadsByLength
# createdAt       : 2014-03-19 16:22:07.455272
# createdAt (UTC) : 2014-03-19 23:22:07.455297
# ncmds           : 6
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_PreAssembler/filterLongReadsByLength.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_PreAssembler/filterLongReadsByLength.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_subreads.fasta
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/filtered_longreads.fasta
#
########### END TASK metadata #############

# Writing to log file
cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_PreAssembler/filterLongReadsByLength.sh >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_PreAssembler/filterLongReadsByLength.log;



echo "Running task://028069/P_PreAssembler/filterLongReadsByLength on $(uname -a)"

echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_subreads.fasta ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_subreads.fasta'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_subreads.fasta.'
fi
echo 'Successfully validated input files'

# Task filterLongReadsByLength commands:


# Completed writing Task filterLongReadsByLength commands


# Task 1
l= || exit $?
echo "Task 1 completed at $(date)"
# Task 2
l=$(fastalength /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_subreads.fasta | cut -d' ' -f1 | sort -nr | awk '{t+=$1;if(t>=5000000*30){print $1;exit;}}') || exit $?
echo "Task 2 completed at $(date)"
# Task 3
l=${l:-6000} || exit $?
echo "Task 3 completed at $(date)"
# Task 4
echo $l > /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/filtered_longreads.fasta.cutoff || exit $?
echo "Task 4 completed at $(date)"
# Task 5
fastalength /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_subreads.fasta | awk -v len=$l '($1 < len ){ print $2 } '| fastaremove /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_subreads.fasta stdin > /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/filtered_longreads.fasta  || exit $?
echo "Task 5 completed at $(date)"
# Task 6
if [ ! -s /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/filtered_longreads.fasta ]; then echo 'Empty long reads'; exit 1; fi || exit $?
echo "Task 6 completed at $(date)"



echo "Finished on $(date -u)"
# Success
exit 0
