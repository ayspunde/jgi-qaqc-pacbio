#!/bin/bash

########### TASK metadata #############
# Task            : sort
# Module          : P_Mapping
# TaskType        : None
# URL             : task://028069/P_Mapping/sort
# createdAt       : 2014-04-10 15:59:17.455605
# createdAt (UTC) : 2014-04-10 22:59:17.455638
# ncmds           : 1
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_Mapping/sort.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_Mapping/sort.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5
#
########### END TASK metadata #############

# Writing to log file
cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_Mapping/sort.sh >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_Mapping/sort.log;



echo "Running task://028069/P_Mapping/sort on $(uname -a)"

echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5 ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5.'
fi
echo 'Successfully validated input files'

# Task sort commands:


# Completed writing Task sort commands


# Task 1
cmph5tools.py -vv sort --deep --inPlace /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5 || exit $?
echo "Task 1 completed at $(date)"



echo "Finished on $(date -u)"
# Success
exit 0
