#!/bin/bash -l

# do QC for mostly quiver-related metrics: corrN, corrList, quivFailConverge
# ayspunde 2014-03-20

## assertion: already in /NNN-cleanP4/asmQC
## bash /global/projectb/sandbox/rqc/ayspunde/pacbio/DEFAULT/pb.asm_qc.sh


WORK_DIR=`pwd`


pol_asm=polished_assembly.fasta
polish_asm_gz=$WORK_DIR/../data/$pol_asm.gz
if [ ! -e $WORK_DIR/$pol_asm ]; then
	gunzip -c $polish_asm_gz > $WORK_DIR/$pol_asm
fi
printf "%s\t" `pwd` > asm_summary.txt

corr_gff=corrections.gff
# head -n 20 $WORK_DIR/../data/$corr_gff | grep -v '#' | sed 's/;coverage.*//'
cat $WORK_DIR/../data/$corr_gff | grep -v '#' | sed 's/;coverage.*//' > $WORK_DIR/$corr_gff

corrN=$(cat $WORK_DIR/$corr_gff | wc -l)
printf "%d\n" $corrN > corrN.$corrN.txt
printf "corrN=%d\t" $corrN >> asm_summary.txt

quiverMergeLog=$WORK_DIR/../log/P_AssemblyPolishing/callConsensus.log
quivMergeStr='Quiver did not converge to MLE'
quivFailMergeN=$(grep "$quivMergeStr" $quiverMergeLog | wc -l)
printf "%d\n" $quivFailMergeN > quivFailMerge.$quivFailMergeN.txt
printf "quivFail=%d\t" $quivFailMergeN >> asm_summary.txt

pctGcLog=$WORK_DIR/../data/9-terminator/celera-assembler.qc
pctGcStr='Content='
pctGc=$(grep $pctGcStr $pctGcLog | sed "s/$pctGcStr//")
pctGc=$(printf "%2.0f" $pctGc)
printf "%d\n" $pctGc > pctGc.$pctGc.txt
printf "pctGc=%d\t" $pctGc >> asm_summary.txt

nScafStr='TotalScaffolds='
nScaf=$(grep $nScafStr $pctGcLog | sed "s/$nScafStr//")
printf "%d\n" $nScaf > nScaf.$nScaf.txt
printf "nScaf=%d\t" $nScaf >> asm_summary.txt

setXml=$WORK_DIR/../settings.xml
smrtProt=$(grep "protocol version" $setXml | sed -e 's/.*<//' -e 's/editable.*//')
printf "%s\t" "$smrtProt" >> asm_summary.txt

printf "\n" >> asm_summary.txt
cat asm_summary.txt



