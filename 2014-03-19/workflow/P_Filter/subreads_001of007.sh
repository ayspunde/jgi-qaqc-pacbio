#!/bin/bash

########### TASK metadata #############
# Task            : subreads_001of007
# Module          : P_Filter
# TaskType        : None
# URL             : task://028069/P_Filter/subreads_001of007
# createdAt       : 2014-04-10 15:59:18.180731
# createdAt (UTC) : 2014-04-10 22:59:18.180763
# ncmds           : 2
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_Filter/subreads_001of007.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_Filter/subreads_001of007.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.chunk001of007.fofn
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_regions.chunk001of007.fofn
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_subreads.chunk001of007.fasta
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_subreads.chunk001of007.fastq
#
########### END TASK metadata #############

# Writing to log file
cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_Filter/subreads_001of007.sh >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_Filter/subreads_001of007.log;



echo "Running task://028069/P_Filter/subreads_001of007 on $(uname -a)"

echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.chunk001of007.fofn ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.chunk001of007.fofn'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.chunk001of007.fofn.'
fi
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_regions.chunk001of007.fofn ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_regions.chunk001of007.fofn'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_regions.chunk001of007.fofn.'
fi
echo 'Successfully validated input files'

# Task subreads_001of007 commands:


# Completed writing Task subreads_001of007 commands


# Task 1
pls2fasta /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.chunk001of007.fofn /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_subreads.chunk001of007.fasta -trimByRegion -regionTable /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_regions.chunk001of007.fofn  || exit $?
echo "Task 1 completed at $(date)"
# Task 2
pls2fasta /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.chunk001of007.fofn /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_subreads.chunk001of007.fastq -trimByRegion -regionTable /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_regions.chunk001of007.fofn  -fastq || exit $?
echo "Task 2 completed at $(date)"



echo "Finished on $(date -u)"
# Success
exit 0
