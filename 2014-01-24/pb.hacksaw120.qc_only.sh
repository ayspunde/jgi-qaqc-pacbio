#!/bin/bash -l

# ayspunde 2014-01-06

# kick-in hacksaw
# assuming:
# rsync-ed SMRT portal jobs to scratch: pb.rsync.sh 
# cleaned P4 and re-run SMRTPipe: pb.cleanp4.sh
# de-duped, circularized: pb.prelim_qc.sh, pb.circle.sh

# - start in background, to avoid waiting on circularizition forever
## bash /global/projectb/sandbox/rqc/ayspunde/pacbio/2014-01-24/pb.hacksaw120.qc_only.sh &

RUN_HACKSAW='/global/projectb/sandbox/rqc/prod/pipelines/hacksaw/1.2.0/bin/run_hacksaw.py'
# HACKSAW_CONFIG='/global/projectb/sandbox/rqc/ayspunde/pacbio/config/hacksaw/hacksaw.120.qc-only.post-deg-dedup-circl.config'
HACKSAW_CONFIG='/global/projectb/sandbox/rqc/ayspunde/pacbio/config/hacksaw/hacksaw.120.qc-only.stats-final.config'

CLEAN_DIR=`pwd`
DIRTY_DIR=$(echo $CLEAN_DIR | sed  -e "s/-cleanP4.*//")
PB_JOB_ID=$(echo $(basename $DIRTY_DIR) | sed 's/^0//')
seqN=$(basename $(realpath ./..))

printf "Starting post-circularization hacksaw:\nTHIS_DIR=%s\n\n" $CLEAN_DIR

PB_XML=$CLEAN_DIR/input.xml
SETTINGS_XML=$CLEAN_DIR/settings.xml
ls -al $PB_XML $RUN_HACKSAW $HACKSAW_CONFIG $SETTINGS_XML   # for my own visual confirmation that paths were set
echo $PB_XML | grep `pwd`                               	# for my own visual confirmation that path was updated

CIRCLE_FASTA=$CLEAN_DIR/circularize/finalContigs.fasta
while [ ! -e $CIRCLE_FASTA ] ; do
	sleep 600
done
if [ -e $CIRCLE_FASTA ] ; then
	printf "\n\ncircularizitation completed: qsub hacksaw, CLEAN_DIR:\n ''%s''\n\n" $CLEAN_DIR
	qsub -S /bin/bash -cwd -j yes -w e -b yes \
	-l h_rt=72:00:00 -l ram.c=6G -pe pe_slots 8 -M `whoami`@lbl.gov -m bea -N hack$seqN -P prok-assembly.p \
	"$RUN_HACKSAW -config $HACKSAW_CONFIG -module -smrtportal -settingsXml $SETTINGS_XML $PB_XML"
fi




