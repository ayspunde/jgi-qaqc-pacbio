#!/bin/bash -l

##  SCRIPT_LOC='/global/projectb/sandbox/rqc/ayspunde/pacbio/DEFAULT'
## 	$SCRIPT_LOC/workflow/P_RePolishing 2>&1 | tee -a `pwd`/log/P_RePolishing.log

module load smrtanalysis/2.1.1

CLEAN_DIR=$(realpath `pwd`)
DIRTY_DIR=$(echo $CLEAN_DIR | sed  -e "s/-cleanP4.*//")
jobID=$(basename $DIRTY_DIR)
seqID=$(basename $(realpath $CLEAN_DIR/..))

oldDATA=$CLEAN_DIR/data
newDATA=$CLEAN_DIR/data2
# newDATA=$CLEAN_DIR/data3
locTEMP=$CLEAN_DIR/temp2
new_ref_str='reference_cir'
newREF=$CLEAN_DIR/$new_ref_str
newREF_FASTA=$CLEAN_DIR/prelimQC/polished_assembly.deg.dedup.fasta

#
#
#### P_ReferenceUploader.sh #####
#
#

########### TASK order #############
reference_upload=1
########### END TASK order #############

########### TASK metadata #############
# Task            : runUploaderHgap
# Module          : P_ReferenceUploader
# TaskType        : None
# URL             : task://028069/P_ReferenceUploader/runUploaderHgap
# createdAt       : 2014-03-19 16:22:07.473798
# createdAt (UTC) : 2014-03-19 23:22:07.473822
# ncmds           : 2
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_ReferenceUploader/runUploaderHgap.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_ReferenceUploader/runUploaderHgap.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/celera-assembler.scf.fasta
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/reference
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/.sentinel_reference_uploader_completed
#
########### END TASK metadata #############

if [[ $reference_upload -eq '1' ]]; then
    echo "Running task://028069/P_ReferenceUploader/runUploaderHgap on $(uname -a)"
    echo "Started on $(date -u)"
    echo 'Validating existence of Input Files'
    if [ -e $newREF_FASTA ]; then
        echo "Successfully found $newREF_FASTA"
    else
        echo "WARNING: Unable to find necessary input file, or dir $newREF_FASTA"
    fi
    echo 'Successfully validated input files'
    # Task runUploaderHgap commands:
    # Completed writing Task runUploaderHgap commands
    # Task 1
    referenceUploader  \
        --skipIndexUpdate  \
        -c -n "reference_cir" \
        -p $CLEAN_DIR \
        -f $newREF_FASTA  \
        --saw="sawriter -blt 8 -welter" --gatkDict="createSequenceDictionary" --samIdx="samtools faidx"  --jobId="$jobID"  --verbose \
        || exit $?
    echo "Task 1 completed at $(date)"
    # Task 2
    touch $CLEAN_DIR/.sentinel_reference_uploader_completed || exit $?
    echo "Task 2 completed at $(date)"
fi




#
#
#### P_Mapping.sh #####
#
#

########### TASK order #############
align=1
sort=1
loadChemistry=1
repack=1
covGFF=1
##gff2Bed
unmapped=1
samBam=1
########### END TASK order #############


# module load smrtanalysis/2.1.1

# CLEAN_DIR=$(realpath `pwd`)
# DIRTY_DIR=$(echo $CLEAN_DIR | sed  -e "s/-cleanP4.*//")
# jobID=$(basename $DIRTY_DIR)
# seqID=$(basename $(realpath $CLEAN_DIR/..))

# oldDATA=$CLEAN_DIR/data
# newDATA=$CLEAN_DIR/data2
# # newDATA=$CLEAN_DIR/data3
# locTEMP=$CLEAN_DIR/temp2
# newREF=$CLEAN_DIR/reference


########### TASK metadata #############
# Task            : align
# Module          : P_Mapping
# TaskType        : None
# URL             : task://$jobID/P_Mapping/align
# createdAt       : ###2014-04-10 15:59:17.677482
# createdAt (UTC) : ###2014-04-10 22:59:17.677516
# ncmds           : 4
# LogPath         : $CLEAN_DIR/log/P_Mapping/align.log
# Script Path     : $SCRIPT_LOC/workflow/P_Mapping/align.sh
#
# Input       : $CLEAN_DIR/input.fofn
# Input       : $CLEAN_DIR/data/filtered_regions.fofn
# Output      : $CLEAN_DIR/data/aligned_reads.cmp.h5
#
########### END TASK metadata #############

if [[ $align -eq '1' ]]; then
	echo "Running task://$jobID/P_Mapping/align on $(uname -a)"
	echo "Started on $(date -u)"
	echo 'Validating existence of Input Files'
	if [ -e "$CLEAN_DIR/input.fofn" ]; then
		echo "Successfully found $CLEAN_DIR/input.fofn"
	else
		echo "WARNING: Unable to find necessary input file, or dir $CLEAN_DIR/input.fofn"
	fi
	if [ -e "$CLEAN_DIR/data/filtered_regions.fofn" ]; then
		echo "Successfully found $CLEAN_DIR/data/filtered_regions.fofn"
	else
		echo "WARNING: Unable to find necessary input file, or dir $CLEAN_DIR/data/filtered_regions.fofn"
	fi
	echo 'Successfully validated input files'

	# Task align commands:
	# Completed writing Task align commands
	# Task 1
	compareSequences.py \
		--info --useGuidedAlign --algorithm=blasr --nproc=15  --noXML --h5mode=w \
		--h5fn=$newDATA/aligned_reads.cmp.h5 \
		--seed=1 --minAccuracy=0.75 --minLength=50 --useQuality  -x -minMatch 12 -x -bestn 10 -x -minPctIdentity 70.0 --placeRepeatsRandomly \
		--tmpDir=$locTEMP \
		--regionTable=$oldDATA/filtered_regions.fofn "$CLEAN_DIR/input.fofn" "$newREF" \
		|| exit $?
	echo "Task 1 completed at $(date)"

	# Task 2
	echo 'Alignment Complete' || exit $?
	echo "Task 2 completed at $(date)"

	# Task 3
	cp $oldDATA/chemistry_mapping.xml $newDATA/chemistry_mapping.xml
	loadPulses \
		$CLEAN_DIR/input.fofn \
		$newDATA/aligned_reads.cmp.h5 \
		-metrics DeletionQV,IPD,InsertionQV,PulseWidth,QualityValue,MergeQV,SubstitutionQV,DeletionTag -byread \
		|| exit $?
	echo "Task 3 completed at $(date)"

	# Task 4
	echo 'LoadPulses Complete' || exit $?
	echo "Task 4 completed at $(date)"
fi

########### TASK metadata #############
# Task            : sort
# Module          : P_Mapping
# TaskType        : None
# URL             : task://028069/P_Mapping/sort
# createdAt       : 2014-04-10 15:59:17.455605
# createdAt (UTC) : 2014-04-10 22:59:17.455638
# ncmds           : 1
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_Mapping/sort.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_Mapping/sort.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5
#
########### END TASK metadata #############

if [[ $sort -eq '1' ]]; then
	echo "Running task://028069/P_Mapping/sort on $(uname -a)"
	echo "Started on $(date -u)"
	echo 'Validating existence of Input Files'
	if [ -e "$newDATA/aligned_reads.cmp.h5" ]; then
		echo "Successfully found $newDATA/aligned_reads.cmp.h5"
	else
		echo "WARNING: Unable to find necessary input file, or dir $newDATA/aligned_reads.cmp.h5"
	fi
	echo 'Successfully validated input files'

	# Task sort commands:
	# Completed writing Task sort commands

	# Task 1
	cmph5tools.py -vv sort --deep --inPlace \
		$newDATA/aligned_reads.cmp.h5 \
		|| exit $?
	echo "Task 1 completed at $(date)"
fi


########### TASK metadata #############
# Task            : loadChemistry
# Module          : P_Mapping
# TaskType        : None
# URL             : task://028069/P_Mapping/loadChemistry
# createdAt       : 2014-04-10 15:59:17.312598
# createdAt (UTC) : 2014-04-10 22:59:17.312639
# ncmds           : 1
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_Mapping/loadChemistry.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_Mapping/loadChemistry.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/chemistry_mapping.xml
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5
#
########### END TASK metadata #############

if [[ $loadChemistry -eq '1' ]]; then
	echo "Running task://028069/P_Mapping/loadChemistry on $(uname -a)"
	echo "Started on $(date -u)"
	echo 'Validating existence of Input Files'
	if [ -e "$oldDATA/chemistry_mapping.xml" ]; then
		echo "Successfully found $oldDATA/chemistry_mapping.xml"
	else
		echo "WARNING: Unable to find necessary input file, or dir $oldDATA/chemistry_mapping.xml"
	fi
	if [ -e "$newDATA/aligned_reads.cmp.h5" ]; then
		echo "Successfully found $newDATA/aligned_reads.cmp.h5"
	else
		echo "WARNING: Unable to find necessary input file, or dir $newDATA/aligned_reads.cmp.h5"
	fi
	echo 'Successfully validated input files'

	# Task loadChemistry commands:
	# Completed writing Task loadChemistry commands

	# Task 1
	loadSequencingChemistryIntoCmpH5.py \
		--xml $oldDATA/chemistry_mapping.xml \
		--h5 $newDATA/aligned_reads.cmp.h5 \
		|| exit $?
	echo "Task 1 completed at $(date)"
fi


########### TASK metadata #############
# Task            : repack
# Module          : P_Mapping
# TaskType        : None
# URL             : task://028069/P_Mapping/repack
# createdAt       : 2014-04-10 15:59:17.834167
# createdAt (UTC) : 2014-04-10 22:59:17.834199
# ncmds           : 1
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_Mapping/repack.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_Mapping/repack.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5
#
########### END TASK metadata #############

if [[ $repack -eq '1' ]]; then
	echo "Running task://028069/P_Mapping/repack on $(uname -a)"
	echo "Started on $(date -u)"
	echo 'Validating existence of Input Files'
	if [ -e "$newDATA/aligned_reads.cmp.h5" ]; then
		echo "Successfully found $newDATA/aligned_reads.cmp.h5"
	else
		echo "WARNING: Unable to find necessary input file, or dir $newDATA/aligned_reads.cmp.h5"
	fi
	echo 'Successfully validated input files'

	# Task repack commands:
	# Completed writing Task repack commands

	# Task 1
	((which h5repack && \
		(h5repack -f GZIP=1 \
		$newDATA/aligned_reads.cmp.h5 \
		$newDATA/aligned_reads.cmp.h5_TMP \
		&& mv $newDATA/aligned_reads.cmp.h5_TMP $newDATA/aligned_reads.cmp.h5)) \
		|| echo 'no h5repack found, continuing w/out') \
		|| exit $?
	echo "Task 1 completed at $(date)"
fi


########### TASK metadata #############
# Task            : covGFF
# Module          : P_Mapping
# TaskType        : None
# URL             : task://028069/P_Mapping/covGFF
# createdAt       : 2014-04-10 15:59:17.923072
# createdAt (UTC) : 2014-04-10 22:59:17.923104
# ncmds           : 1
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_Mapping/covGFF.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_Mapping/covGFF.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/alignment_summary.gff
#
########### END TASK metadata #############

if [[ $covGFF -eq '1' ]]; then
	echo "Running task://028069/P_Mapping/covGFF on $(uname -a)"
	echo "Started on $(date -u)"
	echo 'Validating existence of Input Files'
	if [ -e "$newDATA/aligned_reads.cmp.h5" ]; then
		echo "Successfully found $newDATA/aligned_reads.cmp.h5"
	else
		echo "WARNING: Unable to find necessary input file, or dir $newDATA/aligned_reads.cmp.h5"
	fi
	echo 'Successfully validated input files'

	# Task covGFF commands:
	# Completed writing Task covGFF commands
	# Task 1
	summarizeCoverage.py \
		--reference $newREF \
		--numRegions=500 \
		$newDATA/aligned_reads.cmp.h5 \
		> $newDATA/alignment_summary.gff \
		|| exit $?
	echo "Task 1 completed at $(date)"
fi


########### TASK metadata #############
# Task            : unmapped
# Module          : P_Mapping
# TaskType        : None
# URL             : task://028069/P_Mapping/unmapped
# createdAt       : 2014-04-10 15:59:18.199346
# createdAt (UTC) : 2014-04-10 22:59:18.199378
# ncmds           : 1
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_Mapping/unmapped.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_Mapping/unmapped.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_subreads.fasta
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/unmappedSubreads.fasta
#
########### END TASK metadata #############

if [[ $unmapped -eq '1' ]]; then
	echo "Running task://028069/P_Mapping/unmapped on $(uname -a)"
	echo "Started on $(date -u)"
	echo 'Validating existence of Input Files'
	if [ -e "$oldDATA/filtered_subreads.fasta" ]; then
		echo "Successfully found $oldDATA/filtered_subreads.fasta"
	else
		echo "WARNING: Unable to find necessary input file, or dir $oldDATA/filtered_subreads.fasta"
	fi
	if [ -e "$newDATA/aligned_reads.cmp.h5" ]; then
		echo "Successfully found $newDATA/aligned_reads.cmp.h5"
	else
		echo "WARNING: Unable to find necessary input file, or dir $newDATA/aligned_reads.cmp.h5"
	fi
	echo 'Successfully validated input files'

	# Task unmapped commands:
	# Completed writing Task unmapped commands
	# Task 1
	extractUnmappedSubreads.py \
		$oldDATA/filtered_subreads.fasta \
		$newDATA/aligned_reads.cmp.h5  \
		> $newDATA/unmappedSubreads.fasta \
		|| exit $?
	echo "Task 1 completed at $(date)"
fi


########### TASK metadata #############
# Task            : samBam
# Module          : P_Mapping
# TaskType        : None
# URL             : task://028069/P_Mapping/samBam
# createdAt       : 2014-04-10 15:59:17.671938
# createdAt (UTC) : 2014-04-10 22:59:17.671971
# ncmds           : 1
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_Mapping/samBam.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_Mapping/samBam.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.bam.bai
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.bam
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.sam
#
########### END TASK metadata #############

if [[ $samBam -eq '1' ]]; then
	echo "Running task://028069/P_Mapping/samBam on $(uname -a)"
	echo "Started on $(date -u)"
	echo 'Validating existence of Input Files'
	if [ -e "/$newDATA/aligned_reads.cmp.h5" ]; then
		echo "Successfully found /$newDATA/aligned_reads.cmp.h5"
	else
		echo "WARNING: Unable to find necessary input file, or dir /$newDATA/aligned_reads.cmp.h5"
	fi
	echo 'Successfully validated input files'

	# Task samBam commands:
	# Completed writing Task samBam commands
	# Task 1
	pbsamtools.py --bam \
		--outfile $newDATA/aligned_reads.sam \
		--refrepos $newREF \
		--readGroup movie $newDATA/aligned_reads.cmp.h5 \
		|| exit $?
	echo "Task 1 completed at $(date)"
fi


# echo "Finished on $(date -u)"
# # Success
# exit 0


SCRIPT_LOC='/global/projectb/sandbox/rqc/ayspunde/pacbio/DEFAULT'
## 	$SCRIPT_LOC/workflow/P_AssemblyPolishing.sh 2>&1 | tee -a `pwd`/log/P_AssemblyPolishing.log


#
#
#### P_AssemblyPolishing.sh #####
#
#


########### TASK order #############
callConsensus=1
enrichAlnSummary=1
genPolishedReport=1
topCorrections=1
variantsRpt=1
##zipPolishedFasta=1
########### END TASK order #############


# module load smrtanalysis/2.1.1

# CLEAN_DIR=$(realpath `pwd`)
# DIRTY_DIR=$(echo $CLEAN_DIR | sed  -e "s/-cleanP4.*//")
# jobID=$(basename $DIRTY_DIR)
# seqID=$(basename $(realpath $CLEAN_DIR/..))

# oldDATA=$CLEAN_DIR/data
# # newDATA=$CLEAN_DIR/data2
# newDATA=$CLEAN_DIR/data3
# locTEMP=$CLEAN_DIR/temp2
# newREF=$CLEAN_DIR/reference


########### TASK metadata #############
# Task            : callConsensus
# Module          : P_AssemblyPolishing
# TaskType        : None
# URL             : task://028069/P_AssemblyPolishing/callConsensus
# createdAt       : 2014-04-10 15:59:17.629839
# createdAt (UTC) : 2014-04-10 22:59:17.629871
# ncmds           : 1
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_AssemblyPolishing/callConsensus.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_AssemblyPolishing/callConsensus.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrections.gff
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/polished_assembly.fasta
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/polished_assembly.fastq.gz
#
########### END TASK metadata #############

echo "Running task://028069/P_AssemblyPolishing/callConsensus on $(uname -a)"
echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e "$newDATA/aligned_reads.cmp.h5" ]; then
	echo "Successfully found $newDATA/aligned_reads.cmp.h5"
else
	echo "WARNING: Unable to find necessary input file, or dir $newDATA/aligned_reads.cmp.h5"
fi
echo 'Successfully validated input files'

# Task callConsensus commands:
# Completed writing Task callConsensus commands
# Task 1
variantCaller.py \
	-P /global/projectb/shared/software/smrtanalysis/2.1.1/current/analysis/etc/algorithm_parameters/2013-09 -v -j7 --algorithm=quiver \
	$newDATA/aligned_reads.cmp.h5 \
	-r $newREF/sequence/$new_ref_str.fasta \
	-o $newDATA/corrections.gff \
	-o $newDATA/polished_assembly.fasta \
	-o $newDATA/polished_assembly.fastq.gz \
	|| exit $?
echo "Task 1 completed at $(date)"
# Task 2
gzip \
	-c $newDATA/polished_assembly.fasta > $newDATA/polished_assembly.fasta.gz \
	|| exit $?
echo "Task 2 completed at $(date)"


########### TASK metadata #############
# Task            : enrichAlnSummary
# Module          : P_AssemblyPolishing
# TaskType        : None
# URL             : task://028069/P_AssemblyPolishing/enrichAlnSummary
# createdAt       : 2014-04-10 15:59:17.908165
# createdAt (UTC) : 2014-04-10 22:59:17.908197
# ncmds           : 2
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_AssemblyPolishing/enrichAlnSummary.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_AssemblyPolishing/enrichAlnSummary.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/alignment_summary.gff
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrections.gff
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/alignment_summary.gff
#
########### END TASK metadata #############

echo "Running task://028069/P_AssemblyPolishing/enrichAlnSummary on $(uname -a)"
echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e "$newDATA/alignment_summary.gff" ]; then
	echo "Successfully found $newDATA/alignment_summary.gff"
else
	echo "WARNING: Unable to find necessary input file, or $newDATA/alignment_summary.gff"
fi
if [ -e "$newDATA/corrections.gff" ]; then
	echo "Successfully found $newDATA/corrections.gff"
else
	echo "WARNING: Unable to find necessary input file, or dir $newDATA/corrections.gff"
fi
echo 'Successfully validated input files'

# Task enrichAlnSummary commands:
# Completed writing Task enrichAlnSummary commands
# Task 1
summarizeConsensus.py \
	--variantsGff $newDATA/corrections.gff \
	$newDATA/alignment_summary.gff \
	--output $locTEMP/$jobID.gff \
	|| exit $?
echo "Task 1 completed at $(date)"
# Task 2
mv \
	/$locTEMP/$jobID.gff \
	$newDATA/alignment_summary.gff \
	|| exit $?
echo "Task 2 completed at $(date)"



########### TASK metadata #############
# Task            : genPolishedReport
# Module          : P_AssemblyPolishing
# TaskType        : None
# URL             : task://028069/P_AssemblyPolishing/genPolishedReport
# createdAt       : 2014-04-10 15:59:17.787282
# createdAt (UTC) : 2014-04-10 22:59:17.787315
# ncmds           : 2
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_AssemblyPolishing/genPolishedReport.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_AssemblyPolishing/genPolishedReport.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/polished_assembly.fasta
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results/polished_report.xml
#
########### END TASK metadata #############

echo "Running task://028069/P_AssemblyPolishing/genPolishedReport on $(uname -a)"
echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e $newDATA/polished_assembly.fasta ]; then
	echo "Successfully found $newDATA/polished_assembly.fasta"
else
	echo "WARNING: Unable to find necessary input file, or dir $newDATA/polished_assembly.fasta"
fi
echo 'Successfully validated input files'

# Task genPolishedReport commands:
# Completed writing Task genPolishedReport commands
# Task 1
nbases=$(fastalength $newDATA/polished_assembly.fasta | awk '{t+=$1}END{print t}')
(
cat << EOF 
<?xml version="1.0" encoding="UTF-8"?><report><layout onecolumn="true"/><title>Polished Assembly</title><attributes><attribute id="1" name="Polished Contigs" value="$(grep -c '>' $newDATA/polished_assembly.fasta)">$(grep -c '>' $newDATA/polished_assembly.fasta)</attribute><attribute id="2" name="Max Contig Length" value="$(fastalength $newDATA/polished_assembly.fasta | sort -n | tail -1 | awk '{print $1}')">$(fastalength $newDATA/polished_assembly.fasta | sort -n | tail -1 | awk '{print $1}')</attribute><attribute id="3" name="N50 Contig Length" value="$(fastalength $newDATA/polished_assembly.fasta | cut -d' ' -f1 | sort -nr | awk -v nbases=$nbases '{t+=$1;if (t >= nbases / 2){print $1; exit;}}')">$(fastalength $newDATA/polished_assembly.fasta | cut -d' ' -f1 | sort -nr | awk -v nbases=$nbases '{t+=$1;if (t >= nbases / 2){print $1; exit;}}')</attribute><attribute id="4" name="Sum of Contig Lengths" value="$nbases">$nbases</attribute></attributes></report>
EOF
) > $newDATA/results/polished_report.xml || exit $?
echo "Task 1 completed at $(date)"
# Task 2
saxonb9 \
	-xsl:/global/projectb/shared/software/smrtanalysis/2.1.1/current/analysis/etc/xsl/brandedGraphReport.xsl \
	-s:$newDATA/results/polished_report.xml \
	-o:$newDATA/results/polished_report.html \
	|| exit $?
echo "Task 2 completed at $(date)"



########### TASK metadata #############
# Task            : topCorrections
# Module          : P_AssemblyPolishing
# TaskType        : None
# URL             : task://028069/P_AssemblyPolishing/topCorrections
# createdAt       : 2014-04-10 15:59:17.931700
# createdAt (UTC) : 2014-04-10 22:59:17.931733
# ncmds           : 2
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_AssemblyPolishing/topCorrections.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_AssemblyPolishing/topCorrections.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrections.gff
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results/topCorrections.xml
#
########### END TASK metadata #############

echo "Running task://028069/P_AssemblyPolishing/topCorrections on $(uname -a)"
echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e $newDATA/corrections.gff ]; then
	echo "Successfully found $newDATA/corrections.gff"
else
	echo "WARNING: Unable to find necessary input file, or dir $newDATA/corrections.gff"
fi
echo 'Successfully validated input files'

# Task topCorrections commands:
# Completed writing Task topCorrections commands
# Task 1
makeTopVariantsReportHgap.py \
	--refdir=$newREF \
	--output=$newDATA/results \
	$newDATA/corrections.gff \
	$newDATA/results/topCorrections.xml \
	|| exit $?
echo "Task 1 completed at $(date)"
# Task 2
saxonb9 \
	-xsl:/global/projectb/shared/software/smrtanalysis/2.1.1/current/analysis/etc/xsl/brandedGraphReport.xsl \
	-s:$newDATA/results/topCorrections.xml \
	-o:$newDATA/results/topCorrections.html \
	|| exit $?
echo "Task 2 completed at $(date)"


########### TASK metadata #############
# Task            : variantsRpt
# Module          : P_AssemblyPolishing
# TaskType        : None
# URL             : task://028069/P_AssemblyPolishing/variantsRpt
# createdAt       : 2014-04-10 15:59:18.177620
# createdAt (UTC) : 2014-04-10 22:59:18.177653
# ncmds           : 2
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_AssemblyPolishing/variantsRpt.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_AssemblyPolishing/variantsRpt.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrections.gff
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/alignment_summary.gff
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results/corrections.xml
#
########### END TASK metadata #############

echo "Running task://028069/P_AssemblyPolishing/variantsRpt on $(uname -a)"
echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e $newDATA/corrections.gff ]; then
	echo 'Successfully found $newDATA/corrections.gff'
else
	echo 'WARNING: Unable to find necessary input file, or dir $newDATA/corrections.gff.'
fi
if [ -e $newDATA/alignment_summary.gff ]; then
	echo 'Successfully found $newDATA/alignment_summary.gff'
else
	echo 'WARNING: Unable to find necessary input file, or dir $newDATA/alignment_summary.gff.'
fi
echo 'Successfully validated input files'

# Task variantsRpt commands:
# Completed writing Task variantsRpt commands
# Task 1
makeVariantReportFromGffHgap.py \
	--refdir=$newREF \
	--output=$newDATA/results \
	$newDATA/alignment_summary.gff \
	$newDATA/corrections.gff \
	$newDATA/results/corrections.xml \
	|| exit $?
echo "Task 1 completed at $(date)"
# Task 2
saxonb9 \
	-xsl:/global/projectb/shared/software/smrtanalysis/2.1.1/current/analysis/etc/xsl/brandedGraphReport.xsl \
	-s:$newDATA/results/corrections.xml \
	-o:$newDATA/results/corrections.html \
	|| exit $?
echo "Task 2 completed at $(date)"






echo "Finished on $(date -u)"
# Success
exit 0




