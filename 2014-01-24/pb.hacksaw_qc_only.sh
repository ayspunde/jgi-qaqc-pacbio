#!/bin/bash -l

# ayspunde 2014-01-06

# kick-in hacksaw
# assuming:
# rsync-ed SMRT portal jobs to scratch: pb.rsync.sh 
# cleaned P4 and re-run SMRTPipe: pb.cleanp4.sh
# de-duped, circularized: pb.prelim_qc.sh, pb.circle.sh

# - start in background, to avoid waiting on circularizition forever
## bash /global/projectb/sandbox/rqc/ayspunde/pacbio/2014-01-24/pb.hacksaw_qc_only.sh &

RUN_HACKSAW_120='/global/projectb/sandbox/rqc/prod/pipelines/hacksaw/1.2.0/bin/run_hacksaw.py'
HACKSAW_CONFIG_120='/global/projectb/sandbox/rqc/ayspunde/pacbio/config/hacksaw/hacksaw.120.qc-only.post-deg-dedup-circl.config'
# HACKSAW_CONFIG_120='/global/projectb/sandbox/rqc/ayspunde/pacbio/config/hacksaw/hacksaw.120.qc-only.stats-final.config'

# RUN_HACKSAW_130='/global/projectb/sandbox/rqc/prod/pipelines/hacksaw/1.3.0/bin/run_hacksaw.py'
# HACKSAW_CONFIG_130='/global/projectb/sandbox/rqc/ayspunde/pacbio/config/hacksaw/hacksaw.130.qc-only.stats-final.config'

#-- decide which version: 1.2.0 or 1.3.0
hack_ver=120
# hack_ver=130

# if [[ $hack_ver -eq '120' ]]; then
	# RUN_HACKSAW=$RUN_HACKSAW_120
	# HACKSAW_CONFIG=$HACKSAW_CONFIG_120	
# elif [[ $hack_ver -eq '130' ]]; then
	# RUN_HACKSAW=$RUN_HACKSAW_130
	# HACKSAW_CONFIG=$HACKSAW_CONFIG_130
	# echo $RUN_HACKSAW_130
# fi
# exit

CLEAN_DIR=`pwd`
DIRTY_DIR=$(echo $CLEAN_DIR | sed  -e "s/-cleanP4.*//")
PB_JOB_ID=$(echo $(basename $DIRTY_DIR) | sed 's/^0//')
seqN=$(basename $(realpath ./..))

printf "Starting post-circularization hacksaw:\nTHIS_DIR=%s\n\n" $CLEAN_DIR

INPUT_XML=$CLEAN_DIR/input.xml
SETTINGS_XML=$CLEAN_DIR/settings.xml
ls -al $INPUT_XML $RUN_HACKSAW $HACKSAW_CONFIG $SETTINGS_XML   # for my own visual confirmation that paths were set
echo $INPUT_XML | grep `pwd`                               	# for my own visual confirmation that path was updated

CIRCLE_FASTA=$CLEAN_DIR/circularize/finalContigs.fasta
if [[  ! -e $INPUT_XML || ! -e $SETTINGS_XML ]]; then
	printf "Can't start post-circularization hacksaw:\nmissing files\nTHIS_DIR=%s\n\n" $CLEAN_DIR
	date
	exit 1
fi
while [ ! -e $CIRCLE_FASTA ] ; do
	printf "Waiting to start post-circularization hacksaw:\nno finalContigs.fasta yet\nTHIS_DIR=%s\n\n" $CLEAN_DIR
	date
	sleep 600
done
if [ -e $CIRCLE_FASTA ] ; then
	printf "\n\ncircularizitation completed: qsub hacksaw, CLEAN_DIR:\n ''%s''\n\n" $CLEAN_DIR
	date
	if [[ $hack_ver -eq '120' ]]; then
		qsub -S /bin/bash -cwd -j yes -w e -b yes \
		-l h_rt=72:00:00 -l ram.c=6G -pe pe_slots 8 -M `whoami`@lbl.gov -m bea -N hack$seqN -P prok-assembly.p \
		"$RUN_HACKSAW_120 -config $HACKSAW_CONFIG_120 -module -smrtportal -settingsXml $SETTINGS_XML $INPUT_XML"
	elif [[ $hack_ver -eq '130' ]]; then
		qsub -S /bin/bash -cwd -j yes -w e -b yes \
		-l h_rt=72:00:00 -l ram.c=6G -pe pe_slots 8 -M `whoami`@lbl.gov -m bea -N hack$seqN -P prok-assembly.p \
		"$RUN_HACKSAW_130 -config $HACKSAW_CONFIG_130 $INPUT_XML $SETTINGS_XML"
	fi
fi




