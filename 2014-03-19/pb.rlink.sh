#!/bin/bash

# ayspunde 2014-03-03

# link, not rsync, successful SMRT portal jobs to scratch:
##> /global/projectb/sandbox/rqc/ayspunde/pacbio/DEFAULT/pb.rlink.sh

# to clean P4 spike-in call pb.cleanp4.sh
# to kick-in hacksaw call pb.hacksaw.sh

# PACBIO_PORTAL is SDM repository of SMRT Portl jobs
# PACBIO_LOCAL is your working dir
PACBIO_PORTAL='/global/projectb/shared/pacbio/jobs'
# PACBIO_PORTAL='/global/projectb/scratch/smrt_dev/userdata/jobs'
PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc41'

###
# jName - ID(s) for the organism; free-form; I'm using ID derived from JIRA ticket number: SEQQC-1234=>s1234 
# jID - jobID from SMRT Portal, exact

# 2014-04-02: re-running chemistry mis-matched data
# jName=(s331  s863    s420  s732 s367  s754  s939  s733  s649  s700  s933  s648 s103)
# jID=( 27031  27334  26979 27314  27023  27753  27438  27315  26852  27326  27338 26883  27182)
# jName=(s551  s344)
# jID=( 27022 26842)
# jName=(s1011 s1041 s1043 s1064 s1065 s1066 s1067 s1068 s370 s378 s703 s703 s735 s749 s750 s754 s755 s757 s758 s760 s764 s803 s804 s805 s861 s930 s940 s972 s973 s975)
# jID=(27731 27713 27513 27714 27715 27716 27717 27718 27349 27256 27313 27351 27390 27752 27800 27753 27677 27675 27678 27679 27680 27539 27538 27537 27332 28036 27706 27708 27721 27730)
# jName=(s1209=28040 s1209=28039 s1210=28038 s1210=28037 s1225=28036 s1225=28035 s1286=28034 s1286=28033 s1312=28032 s1312=28031 s781=28030 s781=28029)
jName=(s734=28052 s750=28051 s735=28050 s703=28049 s1064=28048 s1043=28047 s1011=28045)
jName=(s1313=28155 s1313=28156)
jName=(s971=28094 s971=28095 s972=27755 s973=27721 s1211=28150 s1211=28151 s1212=28152 s1212=28153 s1314=28157 s1314=28158 s1315=28160 s1315=28162 s1316=28164 s1316=28165 s1317=28181 s1317=28183)
## 0505
jName=(s1418=28665 s1419=28677 s1419=28678 s1420=28680 s1424=28685 s1424=28686 s1749=28690 s1896=28668)
jName=(s1208=28067 s1208=28147)
jName=(s619=28741 s619=28742)
jName=(s1226=28673 s1226=28674)
jName=(s2063=28809  s2064=28811)


for ((i=0; i < ${#jName[@]} ; i++)); do
    # option as a single list of key-value pairs
	J=${jName[$i]}
	JIRA_NAME=$(echo $J | sed 's/\(.*\)=\(.*\)/\1/')
    PB_JOB_ID=$(echo $J | sed 's/\(.*\)=\(.*\)/\2/')
	# # option as individual name/id lists
	# JIRA_NAME=${jName[$i]}
    # PB_JOB_ID=${jID[$i]}
	LOCAL_DIR=$PACBIO_LOCAL/$JIRA_NAME
	if [ ! -d $LOCAL_DIR ]; then 
		mkdir -m 775 -p $LOCAL_DIR
    fi
	LOCAL_JOB=$LOCAL_DIR/0${PB_JOB_ID}
 
    L1NN=$(echo $PB_JOB_ID | sed 's/\([0-9]\{2\}\).*/\1/')
    PORTAL_JOB=$PACBIO_PORTAL/0${L1NN}/0${PB_JOB_ID}
	
	ln -s $PORTAL_JOB $LOCAL_JOB
	chgrp -R genome $LOCAL_DIR &

done
