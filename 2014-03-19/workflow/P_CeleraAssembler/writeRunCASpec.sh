#!/bin/bash

########### TASK metadata #############
# Task            : writeRunCASpec
# Module          : P_CeleraAssembler
# TaskType        : None
# URL             : task://028069/P_CeleraAssembler/writeRunCASpec
# createdAt       : 2014-03-19 16:22:07.412933
# createdAt (UTC) : 2014-03-19 23:22:07.412958
# ncmds           : 1
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_CeleraAssembler/writeRunCASpec.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_CeleraAssembler/writeRunCASpec.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.fasta
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/runCA.spec
#
########### END TASK metadata #############

# Writing to log file
cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_CeleraAssembler/writeRunCASpec.sh >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_CeleraAssembler/writeRunCASpec.log;



echo "Running task://028069/P_CeleraAssembler/writeRunCASpec on $(uname -a)"

echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.fasta ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.fasta'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.fasta.'
fi
echo 'Successfully validated input files'

# Task writeRunCASpec commands:


# Completed writing Task writeRunCASpec commands


# Task 1
runCASpecWriter.py  --verbose --bitTable=/global/projectb/shared/software/smrtanalysis/2.1.1/current/analysis/etc/celeraAssembler/bitTable --interactiveTmpl=/global/projectb/shared/software/smrtanalysis/2.1.1/current/analysis/etc/cluster/SGE/interactive.tmpl --smrtpipeRc=/global/projectb/shared/software/smrtanalysis/2.1.1/current/analysis/etc/smrtpipe.rc --genomeSize=5000000 --defaultFrgMinLen=1000 --xCoverage=15 --ovlErrorRate=0.06 --ovlMinLen=40 --merSize=14 --corrReadsFasta=/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.fasta --specOut=/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/runCA.spec --sgeName=028069 --gridParams="useGrid:0,scriptOnGrid:0,frgCorrOnGrid:0,ovlCorrOnGrid:0" --maxSlotPerc=1 /global/projectb/shared/software/smrtanalysis/2.1.1/current/analysis/etc/celeraAssembler/template.spec  || exit $?
echo "Task 1 completed at $(date)"



echo "Finished on $(date -u)"
# Success
exit 0
