#!/bin/bash

# ayspunde 2014-01-24

# driver for pb.*.sh : looping over whole project
## /global/projectb/sandbox/rqc/ayspunde/pacbio/2014-01-24/pb.DRIVER.sh &

# PACBIO_LOCAL is your working dir
PACBIO_PORTAL='/global/projectb/shared/pacbio/jobs'
# -- PACBIO_LOCAL is your working dir -- #
# PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc1'
# PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc2'
PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc3'
# PACBIO_LOCAL='/global/projectb/scratch/jasmynp/PacBio/Seqqc'

VERSION2RUN='/global/projectb/sandbox/rqc/ayspunde/pacbio/2014-01-24'
# VERSION2RUN='/global/u1/a/ayspunde/work/projects/pacbio/DEFAULT'

cd $PACBIO_LOCAL
BIG_LOG=$(pwd)/log.$(date +%F.%H%M).txt


# jName=(s365 s367 s368 's371' s372 s420)
# jID=( 27011 27023 26219 '27217' 27008 26979)
# jName=(s365 s367 s368  s372 s420)
# jID=( 27011 27023 26219  27008 26979)
# jName=(s347)
# jID=( 26972)
# # # 2014-01-20
# jName=(s370  s378  s749  s754  s755  s757  s758  s760  s764   s861   s1043  s940 )
# jID=(  27349 27256 27752 27753 27677 27675 27678 27679 27680  27332  27513  27706)
# # # 2014-01-31
# # jName=(s750 s1011 s1041 s1064 s1065 s1066 s1067 s1068  s805  s973  s975)
# # jID=( 27800 27731 27713 27714 27715 27716 27717 27718 27537 27721 27730) 

# # 2014-02-26
# jName=(s703   s703  s735  s803   s804  s930   s972)
# jID=(  27313  27351 27390 27539  27538 28036  27708) 

# 2014-03-03
jName=(s781  s781  s782  s782  s783  s860  s860  s929  s929  s934  s934  s934  s970  s970  s972  s972  s974  s974  s975  s1209 s1209 s1210 s1210 s1225 s1225 s1225 s1286 s1286 s1312 s1312)
jID=(  28044 28045 28124 28048 28051 28057 28058 28055 28056 28099 28100 28101 28097 28098 28060 28061 28092 28090 28127 28069 28070 28071 28072 28102 28103 28104 28108 28110 28111 28112)

# 2014-03-10
jName=(s1209 s1209 s1210 s1210 s1225 s1225 s1225 s1286 s1286 s1312 s1312)
jID=(  28069 28070 28071 28072 28102 28103 28104 28108 28110 28111 28112)

# things2do=(prep4rerun prelim_qc)
# things2do=(cleanP4)
# things2do=(circle)
things2do=(prelim_qc)
# things2do=(circle run_hacksaw_qc_only)
# things2do=(final)

###
### choose how to run: for selected dirs, uncomment jName/jID; for the whole group, use $PACBIO_LOCAL/s* 
###


for ((i=0; i < ${#jName[@]} ; i++)); do
# for ((i=0; i < 1 ; i++)); do
# for ((i=1; i < ${#jName[@]} ; i++)); do
	LOC_NAME=${jName[$i]}
	PB_JOB_ID=${jID[$i]}
	DIRTY_DIR=$PACBIO_LOCAL/$LOC_NAME/0$PB_JOB_ID
	CLEAN_DIR=$DIRTY_DIR-cleanP4
	printf "\n\n\n%d\n%d\n%s\n%s\n\n\n" $i $PB_JOB_ID $DIRTY_DIR $CLEAN_DIR
	if [ ! -d $CLEAN_DIR ]; then
		mkdir -p -m 775 $CLEAN_DIR 
	fi
	cd $CLEAN_DIR
	
# for s_dir in $PACBIO_LOCAL/s* ; do
	# printf "checking for cleanP4:\n%s\n\n" $s_dir
	# LOC_NAME=$(basename $s_dir)	
	# for CLEAN_DIR in $s_dir/*cleanP4 ; do
		# if [  ! -d $CLEAN_DIR ]; then
			# printf "\nno CLEAN_DIR: skip to next project\n\n" 
			# continue	
		# fi
	# done
	# DIRTY_DIR=$(echo $CLEAN_DIR | sed  -e "s/-cleanP4.*//")
	# PB_JOB_ID=$(echo $(basename $DIRTY_DIR) | sed 's/^0//')
	# printf "ready for things2do:\nDIRTY_DIR=%s\nCLEAN_DIR=%s\n\n" $DIRTY_DIR $CLEAN_DIR
	# cd $CLEAN_DIR
	# # continue
	
	for ((j=0; j < ${#things2do[@]} ; j++)); do
		todo=${things2do[$j]}
		echo $todo 
		cd $CLEAN_DIR
		case $todo in
		'cleanP4')
			SCRIPT2RUN=$VERSION2RUN/pb.cleanp4.sh
			WORK_DIR=$CLEAN_DIR	
			$SCRIPT2RUN	2>&1 | tee -a $BIG_LOG		
			;;
		'prep4rerun')
			if [ -d $CLEAN_DIR/assemblyQC ]; then mv $CLEAN_DIR/assemblyQC $CLEAN_DIR/assemblyQC.old ; fi
			if [ -d $CLEAN_DIR/circularize ];then mv $CLEAN_DIR/circularize $CLEAN_DIR/circularize.old;fi
			if [ -d $CLEAN_DIR/final ]; then mv $CLEAN_DIR/final $CLEAN_DIR/final.old; fi
			;;
		'prelim_qc')
			SCRIPT2RUN=$VERSION2RUN/pb.prelim_qc.sh
			WORK_DIR=$CLEAN_DIR/prelimQC
			if [ ! -d $WORK_DIR ]; then 
				printf '\nSTART prelimQC:\n%s\n\n' $WORK_DIR
				mkdir -m 775 $WORK_DIR
				cd $WORK_DIR; pwd
				$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG
			# elif [ -d $WORK_DIR/prelimQC ]; then
				# printf '\n?? prelimQC: what is there:\n%s\n' $WORK_DIR
				# ls $WORK_DIR
				# rm -rf $WORK_DIR
			else 
				printf '\nSKIP prelimQC: already there:\n%s\n\n' $WORK_DIR
			fi
			;;
		'circle')
			SCRIPT2RUN=$VERSION2RUN/pb.circle.sh
			WORK_DIR=$CLEAN_DIR/circularize
			if [ ! -d $WORK_DIR ]; then mkdir -m 775 $WORK_DIR ; fi
			cd $WORK_DIR; pwd
			$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG
			;;
		'run_hacksaw_qc_only')
			SCRIPT2RUN=$VERSION2RUN/pb.hacksaw_qc_only.sh
			WORK_DIR=$CLEAN_DIR
			$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG &
			;;
		'my_qc')
			SCRIPT2RUN=$VERSION2RUN/pb.myqc.sh
			WORK_DIR=$CLEAN_DIR/myQC
			if [ ! -d $WORK_DIR ]; then  mkdir -m 775 $WORK_DIR ; fi
			cd $WORK_DIR; pwd
			$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG
			;;
		'final')
			SCRIPT2RUN=$VERSION2RUN/pb.hacklinks.sh
			WORK_DIR=$CLEAN_DIR/final
			if [ -d $WORK_DIR ]; then 
				cd $WORK_DIR; pwd 
				$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG
			else
				printf "\n\n --!!-- Not a valid directory: %s; skip --??-- \n\n" "$WORK_DIR" | tee -a $BIG_LOG
			fi
			;;
		*)
			printf "\n\n -- !! -- No workflow for case: %s -- ?? -- \n\n" $todo | tee -a $BIG_LOG
			;;
		esac
		cd $CLEAN_DIR
		chgrp -R genome `pwd` &&  chmod -R 775 `pwd` &
	done
done




