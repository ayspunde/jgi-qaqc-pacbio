#!/bin/env bash

# ayspunde 2014-03-19

# driver for pb.*.sh : looping over whole project
## bash /global/projectb/sandbox/rqc/ayspunde/pacbio/DEFAULT/pb.REWORK.driver.sh &

VERSION2RUN='/global/projectb/sandbox/rqc/ayspunde/pacbio/DEFAULT'

# -- PACBIO_LOCAL is your working dir -- #
PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc41'
PACBIO_NEW='/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc41'

cd $PACBIO_LOCAL
BIG_LOG=$(pwd)/log.$(date +%F.%H%M).txt



# things2do=(prep4_rerun)
# things2do=(copy4_qiuiver_rerun)
# things2do=(prep4_qiuiver_rerun)
# things2do=(qiuiver_test)
# things2do=(qiuiver_comp)
things2do=(asm_qc)


###
### choose how to run: for selected dirs, uncomment jName/jID; for the whole group, use $PACBIO_LOCAL/s* 
###
# 2014-04-02: re-running chemistry mis-matched data
jName=(s863  s420  s732 s367  s754  s939  s733  s649  s700  s933  s648 s103)
jID=( 27334  26979 27314  27023  27753  27438  27315  26852  27326  27338 26883  27182)
jName=(s367  s754  s939  s733  s649  s700  s933  s648 s103)
jID=( 27023  27753  27438  27315  26852  27326  27338 26883  27182)
jName=(s551  s344)
jID=( 27022 26842)

# # for ((i=0; i < 1 ; i++)); do
# for ((i=0; i < ${#jName[@]} ; i++)); do

	# LOC_NAME=${jName[$i]}
	# PB_JOB_ID=${jID[$i]}
	# DIRTY_DIR=$PACBIO_LOCAL/$LOC_NAME/0$PB_JOB_ID
	# CLEAN_DIR=$DIRTY_DIR-cleanP4

# i=0
# for s_dir in $PACBIO_LOCAL/s*/* ; do
	# (( i++ ))
	# DIRTY_DIR=$s_dir				## easy to switch to $PACBIO_LOCAL/s*/*-cleanP4
	# CLEAN_DIR=$DIRTY_DIR-cleanP4 	## DIRTY_DIR=$(echo $CLEAN_DIR | sed  -e "s/-cleanP4.*//")
	# PB_JOB_ID=$(echo $(basename $DIRTY_DIR) | sed 's/^0//')

i=0
for s_dir in $PACBIO_LOCAL/s*/*-cleanP4-x ; do	
	(( i++ ))
	DIRTY_DIR=$s_dir									## easy to switch to $PACBIO_LOCAL/s*/*-cleanP4
	CLEAN_DIR=$(echo $DIRTY_DIR | sed  -e "s/-x//") 	## DIRTY_DIR=$(echo $CLEAN_DIR | sed  -e "s/-cleanP4.*//")
	PB_JOB_ID=$(echo $(basename $(echo $DIRTY_DIR | sed  -e "s/-cleanP4-x//")) | sed 's/^0//')
	
	printf "\n\n\nN=%d\nJOB_ID=%d\nDIRTY_DIR=%s\nCLEAN_DIR=%s\n\n\n" $i $PB_JOB_ID $DIRTY_DIR $CLEAN_DIR
	if [ ! -d $CLEAN_DIR ]; then
		mkdir -p -m 775 $CLEAN_DIR 
	fi
	cd $CLEAN_DIR
	printf "Starting thigs2do in %s\n\n" `pwd` | tee -a $BIG_LOG	
	# break  		## to check on a single dir
	# continue	## just a list of dirs to check
	
	for ((j=0; j < ${#things2do[@]} ; j++)); do
		todo=${things2do[$j]}
		echo $todo 
		cd $CLEAN_DIR
		# continue
		case $todo in
		'prep4_rerun')
			cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			if [ -d $CLEAN_DIR/assemblyQC ]; then mv $CLEAN_DIR/assemblyQC $CLEAN_DIR/assemblyQC.old ; fi
			if [ -d $CLEAN_DIR/circularize ];then mv $CLEAN_DIR/circularize $CLEAN_DIR/circularize.old;fi
			if [ -d $CLEAN_DIR/final ]; then mv $CLEAN_DIR/final $CLEAN_DIR/final.old; fi
			if [ -d $CLEAN_DIR/prelimQC ]; then mv $CLEAN_DIR/prelimQC $CLEAN_DIR/prelimQC.old; fi
			mkdir '.old' && mv -R *old ./.old
			;;
		'copy4_qiuiver_rerun')
			MV2DIR=$CLEAN_DIR-q1
			printf "Starting %s: WORK_DIR %s\nMV2DIR %s\n\n"  $todo `pwd` $MV2DIR
			mkdir -p $MV2DIR
			rsync -rv $CLEAN_DIR/ $MV2DIR &
			;;		
		'prep4_qiuiver_rerun')
			SCRIPT2RUN=$VERSION2RUN/pb.cleanp4.redo.sh
			WORK_DIR=$(echo $CLEAN_DIR | sed "s:$PACBIO_LOCAL:$PACBIO_NEW:")		
			cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG
			;;
		'qiuiver_test')
			SCRIPT2RUN=$VERSION2RUN/pb.quiver_test.sh
			WORK_DIR=$CLEAN_DIR		
			cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG
			;;
		'qiuiver_comp')
			SCRIPT2RUN=$VERSION2RUN/pb.quiver_comp.sh
			WORK_DIR=$CLEAN_DIR		
			cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG
			;;
		'asm_qc')
			SCRIPT2RUN=$VERSION2RUN/pb.asm_qc.sh
			WORK_DIR=$CLEAN_DIR/asmQC		
			if [ ! -d $WORK_DIR ]; then mkdir $WORK_DIR ; fi
			cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG
			WORK_DIR=$DIRTY_DIR/asmQC		
			if [ ! -d $WORK_DIR ]; then mkdir $WORK_DIR ; fi
			cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG
			;;
		*)
			printf "\n\n -- !! -- No workflow for case: %s -- ?? -- \n\n" $todo | tee -a $BIG_LOG
			;;
		esac
		cd $CLEAN_DIR
		chgrp -R genome `pwd` &&  chmod -R 775 `pwd` &
	done
	# break  		## to check on a single dir
done
