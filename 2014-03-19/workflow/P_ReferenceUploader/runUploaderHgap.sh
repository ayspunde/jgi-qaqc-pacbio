#!/bin/bash

########### TASK metadata #############
# Task            : runUploaderHgap
# Module          : P_ReferenceUploader
# TaskType        : None
# URL             : task://028069/P_ReferenceUploader/runUploaderHgap
# createdAt       : 2014-03-19 16:22:07.473798
# createdAt (UTC) : 2014-03-19 23:22:07.473822
# ncmds           : 2
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_ReferenceUploader/runUploaderHgap.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_ReferenceUploader/runUploaderHgap.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/celera-assembler.scf.fasta
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/reference
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/.sentinel_reference_uploader_completed
#
########### END TASK metadata #############

# Writing to log file
cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_ReferenceUploader/runUploaderHgap.sh >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_ReferenceUploader/runUploaderHgap.log;



echo "Running task://028069/P_ReferenceUploader/runUploaderHgap on $(uname -a)"

echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/celera-assembler.scf.fasta ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/celera-assembler.scf.fasta'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/celera-assembler.scf.fasta.'
fi
echo 'Successfully validated input files'

# Task runUploaderHgap commands:


# Completed writing Task runUploaderHgap commands


# Task 1
referenceUploader  --skipIndexUpdate  -c -n "reference" -p /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4 -f /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/celera-assembler.scf.fasta  --saw="sawriter -blt 8 -welter" --gatkDict="createSequenceDictionary" --samIdx="samtools faidx"  --jobId="028069"    --verbose || exit $?
echo "Task 1 completed at $(date)"
# Task 2
touch /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/.sentinel_reference_uploader_completed || exit $?
echo "Task 2 completed at $(date)"



echo "Finished on $(date -u)"
# Success
exit 0
