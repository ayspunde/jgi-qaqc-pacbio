#!/bin/bash

## currently before kickin hacksaw need manually run circularization

# ayspunde 2014-01-27

# assuming rsync-ed: pb.rsync.sh
# cleaned P4, re-run SMRT Pipe: pb.cleanP4.sh
# checked for duplications: pb.prelim_qc.sh

# !! -- make sure deduplication was completed -- !!

# /global/projectb/sandbox/rqc/ayspunde/pacbio/2014-01-24/pb.circle.sh



CLEAN_DIR=$(realpath `pwd`/..)
DIRTY_DIR=$(echo $CLEAN_DIR | sed  -e "s/-cleanP4.*//")
PB_JOB_ID=$(echo $(basename $DIRTY_DIR) | sed 's/^0//')
seqN=$( basename $(realpath $CLEAN_DIR/..))

date
printf "Starting circulazitation:\nTHIS_DIR=%s\n\n" $CLEAN_DIR

TRIM_AND_CIRCLE='/usr/common/jgi/qaqc/jgi-qaqc/detectCircle/trimAndTopologyUsingPb.py'
CIRCLE_INPUT=$CLEAN_DIR/'prelimQC/polished_assembly.deg.dedup.fasta'
FILT_READS_FASTA=$CLEAN_DIR/data/filtered_subreads.fasta
ls -al $FILT_READS_FASTA $CIRCLE_INPUT 

# $TRIM_AND_CIRCLE -r $CIRCLE_INPUT -f $FILT_READS_FASTA
if [[ ! -e $FILT_READS_FASTA || ! -e $CIRCLE_INPUT  ]]; then
	printf "Can't start circularization:\nmissing files\nTHIS_DIR=%s\n\n" $CLEAN_DIR
	date
	exit 1
else
	printf "Qsub-ing circularization:\nTHIS_DIR=%s\n\n" $CLEAN_DIR
	date
	qsub -S /bin/bash -cwd -j yes -w e -b yes \
	-l h_rt=4:00:00 -l ram.c=40G -N circ$seqN -P prok-assembly.p \
	"$TRIM_AND_CIRCLE -r $CIRCLE_INPUT -f $FILT_READS_FASTA" 
fi





