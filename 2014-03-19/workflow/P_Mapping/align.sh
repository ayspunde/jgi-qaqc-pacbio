#!/bin/bash -l

SCRIPT_LOC='/global/projectb/sandbox/rqc/ayspunde/pacbio/DEAFULT'
# $SCRIPT_LOC/workflow/P_Mapping/align.sh

## old_version: /global/projectb/sandbox/rqc/ayspunde/pacbio/2014-03-19/pb.re-quiver.align.sh

module load smrtanalysis/2.1.1

CLEAN_DIR=$(realpath `pwd`)
DIRTY_DIR=$(echo $CLEAN_DIR | sed  -e "s/-cleanP4.*//")
jobID=$(basename $DIRTY_DIR)
seqID=$(basename $(realpath $CLEAN_DIR/..))


########### TASK metadata #############
# Task            : align
# Module          : P_Mapping
# TaskType        : None
# URL             : task://$jobID/P_Mapping/align
# createdAt       : ###2014-04-10 15:59:17.677482
# createdAt (UTC) : ###2014-04-10 22:59:17.677516
# ncmds           : 4
# LogPath         : $CLEAN_DIR/log/P_Mapping/align.log
# Script Path     : $SCRIPT_LOC/workflow/P_Mapping/align.sh
#
# Input       : $CLEAN_DIR/input.fofn
# Input       : $CLEAN_DIR/data/filtered_regions.fofn
# Output      : $CLEAN_DIR/data/aligned_reads.cmp.h5
#
########### END TASK metadata #############

# # Writing to log file
# cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_Mapping/align.sh >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_Mapping/align.log;



echo "Running task://$jobID/P_Mapping/align on $(uname -a)"

echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e "$CLEAN_DIR/input.fofn" ]; then
	echo "Successfully found $CLEAN_DIR/input.fofn"
else
	echo "WARNING: Unable to find necessary input file, or dir $CLEAN_DIR/input.fofn"
fi
if [ -e "$CLEAN_DIR/data/filtered_regions.fofn" ]; then
	echo "Successfully found $CLEAN_DIR/data/filtered_regions.fofn"
else
	echo "WARNING: Unable to find necessary input file, or dir $CLEAN_DIR/data/filtered_regions.fofn"
fi
echo 'Successfully validated input files'

# Task align commands:
# Completed writing Task align commands

# Task 1
oldOUT=$CLEAN_DIR/data
newOUT=$CLEAN_DIR/data2
locTEMP=$CLEAN_DIR/temp2
newREF=$CLEAN_DIR/reference
compareSequences.py \
	--info --useGuidedAlign --algorithm=blasr --nproc=15  --noXML --h5mode=w \
	--h5fn=$newOUT/aligned_reads.cmp.h5 \
	--seed=1 --minAccuracy=0.75 --minLength=50 --useQuality  -x -minMatch 12 -x -bestn 10 -x -minPctIdentity 70.0 --placeRepeatsRandomly \
	--tmpDir=$locTEMP \
	--regionTable=$oldOUT/filtered_regions.fofn "$CLEAN_DIR/input.fofn" "$newREF" \
|| exit $?
echo "Task 1 completed at $(date)"

# Task 2
echo 'Alignment Complete' || exit $?
echo "Task 2 completed at $(date)"

# Task 3
cp $oldOUT/chemistry_mapping.xml $newOUT/chemistry_mapping.xml
loadPulses \
	$CLEAN_DIR/input.fofn \
	$newOUT/aligned_reads.cmp.h5 \
	-metrics DeletionQV,IPD,InsertionQV,PulseWidth,QualityValue,MergeQV,SubstitutionQV,DeletionTag -byread \
|| exit $?
echo "Task 3 completed at $(date)"

# Task 4
echo 'LoadPulses Complete' || exit $?
echo "Task 4 completed at $(date)"



echo "Finished on $(date -u)"
# Success
exit 0



