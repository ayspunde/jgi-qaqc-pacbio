#!/bin/bash

########### TASK metadata #############
# Task            : covRpt
# Module          : P_MappingReports
# TaskType        : None
# URL             : task://028069/P_MappingReports/covRpt
# createdAt       : 2014-04-10 15:59:18.045523
# createdAt (UTC) : 2014-04-10 22:59:18.045556
# ncmds           : 2
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_MappingReports/covRpt.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_MappingReports/covRpt.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/alignment_summary.gff
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results/coverage.xml
#
########### END TASK metadata #############

# Writing to log file
cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_MappingReports/covRpt.sh >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_MappingReports/covRpt.log;



echo "Running task://028069/P_MappingReports/covRpt on $(uname -a)"

echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/alignment_summary.gff ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/alignment_summary.gff'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/alignment_summary.gff.'
fi
echo 'Successfully validated input files'

# Task covRpt commands:


# Completed writing Task covRpt commands


# Task 1
makeCoverageReportFromGff.py --refdir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/>reference_cir --output /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/alignment_summary.gff > /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results/coverage.xml || exit $?
echo "Task 1 completed at $(date)"
# Task 2
saxonb9 -xsl:/global/projectb/shared/software/smrtanalysis/2.1.1/current/analysis/etc/xsl/brandedGraphReport.xsl -s:/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results/coverage.xml -o:/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results/coverage.html || exit $?
echo "Task 2 completed at $(date)"



echo "Finished on $(date -u)"
# Success
exit 0
