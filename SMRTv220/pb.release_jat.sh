#!/bin/bash

# This is just a template for pre-release activities, no plans to script this
## RUN_DIR = **/*clean-P4
# already run: pb.hacklinks.sh

# ayspunde 2014-01-27

# module load jamo/prod qaqc


### Tidy up WORK_DIR=/*clean-P4, Make PDF Report
#
rm metadata.json
if [[ ! -d .fasta ]]; then mkdir .fasta; fi 
mv *fasta ./.fasta 
if [[ ! -d .old ]]; then mkdir .old; fi
mv *old ./.old
if [[ ! -d .log.loc ]]; then mkdir .log.loc; fi
mv *.o* .log.loc ; mv *.po* .log.loc ; mv *log .log.loc
chgrp -R genome `pwd` &&  chmod -R 755 `pwd` &

# ### Make PDF Report
/global/projectb/sandbox/rqc/prod/pipelines/hacksaw/DEFAULT/bin/hacksaw_pdf.pl -pp > project_desc_file.txt
exit 
#
### -- update project_desc_file.txt with metadata
#
sed -i 's/assemblyVersion.row.hgap=/assemblyVersion.row.hgap=2.2.0.p1/' hacksaw_stats.txt
/global/projectb/sandbox/rqc/prod/pipelines/hacksaw/DEFAULT/bin/hacksaw_pdf.pl project_desc_file.txt 2>&1 >/dev/null

### JAMO release
#
proj_id_token='project.JGI Sequencing Project ID='
PROJ_ID=$(cat project_desc_file.txt | grep "$proj_id_token" | sed "s/$proj_id_token//")
submitanalysis.py $PROJ_ID 
chgrp -R genome `pwd` &&  chmod -R 755 `pwd` &

### check content of metadata.json
#############################################################
jat import microbial_isolate_improved_pacbio `pwd`; date
#############################################################

#### QC Report Template ####
#
9) QC Report: PASS [FAIL]
Assembly size = MMM Mb; CCC contigs; 
Confirmed target genome:
SILVA: 
Collab_16S: 
Megan: correct Family-Genus-Species
Check for non-target genome(s): PASS [FAIL]
uni-modal gc-coverage distribution; single-copy: mean=1, median=1.  


