#!/bin/bash

########### TASK metadata #############
# Task            : subreadRpt
# Module          : P_FilterReports
# TaskType        : None
# URL             : task://028069/P_FilterReports/subreadRpt
# createdAt       : 2014-04-10 15:59:17.776599
# createdAt (UTC) : 2014-04-10 22:59:17.776633
# ncmds           : 2
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_FilterReports/subreadRpt.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_FilterReports/subreadRpt.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_subread_summary.csv
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results/filter_reports_filter_subread_stats.json
#
########### END TASK metadata #############

# Writing to log file
cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_FilterReports/subreadRpt.sh >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_FilterReports/subreadRpt.log;



echo "Running task://028069/P_FilterReports/subreadRpt on $(uname -a)"

echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_subread_summary.csv ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_subread_summary.csv'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_subread_summary.csv.'
fi
echo 'Successfully validated input files'

# Task subreadRpt commands:


# Completed writing Task subreadRpt commands


# Task 1
filter_subread.py --debug --report=/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results/filter_reports_filter_subread_stats.json --output=/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_subread_summary.csv || exit $?
echo "Task 1 completed at $(date)"
# Task 2
smrtreporter -basedir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results -headinclude /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results/.martin_header.html --html -o filter_reports_filter_subread_stats.html -rules /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results/.rules_filter_reports_filter_subread_stats.xml || exit $?
echo "Task 2 completed at $(date)"



echo "Finished on $(date -u)"
# Success
exit 0
