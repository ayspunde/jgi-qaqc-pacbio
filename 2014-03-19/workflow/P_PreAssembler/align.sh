#!/bin/bash

########### TASK metadata #############
# Task            : align
# Module          : P_PreAssembler
# TaskType        : None
# URL             : task://028069/P_PreAssembler/align
# createdAt       : 2014-03-19 16:22:07.591393
# createdAt (UTC) : 2014-03-19 23:22:07.591417
# ncmds           : 1
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_PreAssembler/align.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_PreAssembler/align.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/filtered_longreads.fasta
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_regions.fofn
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/align.b4
#
########### END TASK metadata #############

# Writing to log file
cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_PreAssembler/align.sh >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_PreAssembler/align.log;



echo "Running task://028069/P_PreAssembler/align on $(uname -a)"

echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn.'
fi
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/filtered_longreads.fasta ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/filtered_longreads.fasta'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/filtered_longreads.fasta.'
fi
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_regions.fofn ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_regions.fofn'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_regions.fofn.'
fi
echo 'Successfully validated input files'

# Task align commands:


# Completed writing Task align commands


# Task 1
blasr /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/filtered_longreads.fasta -m 4 -nproc 7 -out /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/align.b4 -minReadLength 200 -maxScore -1000 -bestn 24 -maxLCPLength 16 -nCandidates 24 -regionTable /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_regions.fofn || exit $?
echo "Task 1 completed at $(date)"



echo "Finished on $(date -u)"
# Success
exit 0
