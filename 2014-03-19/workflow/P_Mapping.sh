#!/bin/bash -l

SCRIPT_LOC='/global/projectb/sandbox/rqc/ayspunde/pacbio/DEFAULT'
## 	$SCRIPT_LOC/workflow/P_Mapping.sh 2>&1 | tee -a `pwd`/log/P_Mapping.log

########### TASK order #############
align=0
sort=0
loadChemistry=1
repack=1
covGFF=0
##gff2Bed
unmapped=0
samBam=0
########### END TASK order #############


module load smrtanalysis/2.1.1

CLEAN_DIR=$(realpath `pwd`)
DIRTY_DIR=$(echo $CLEAN_DIR | sed  -e "s/-cleanP4.*//")
jobID=$(basename $DIRTY_DIR)
seqID=$(basename $(realpath $CLEAN_DIR/..))

oldDATA=$CLEAN_DIR/data
newDATA=$CLEAN_DIR/data2
# newDATA=$CLEAN_DIR/data3
locTEMP=$CLEAN_DIR/temp2
newREF=$CLEAN_DIR/reference


########### TASK metadata #############
# Task            : align
# Module          : P_Mapping
# TaskType        : None
# URL             : task://$jobID/P_Mapping/align
# createdAt       : ###2014-04-10 15:59:17.677482
# createdAt (UTC) : ###2014-04-10 22:59:17.677516
# ncmds           : 4
# LogPath         : $CLEAN_DIR/log/P_Mapping/align.log
# Script Path     : $SCRIPT_LOC/workflow/P_Mapping/align.sh
#
# Input       : $CLEAN_DIR/input.fofn
# Input       : $CLEAN_DIR/data/filtered_regions.fofn
# Output      : $CLEAN_DIR/data/aligned_reads.cmp.h5
#
########### END TASK metadata #############

if [[ $align -eq '1' ]]; then
	echo "Running task://$jobID/P_Mapping/align on $(uname -a)"
	echo "Started on $(date -u)"
	echo 'Validating existence of Input Files'
	if [ -e "$CLEAN_DIR/input.fofn" ]; then
		echo "Successfully found $CLEAN_DIR/input.fofn"
	else
		echo "WARNING: Unable to find necessary input file, or dir $CLEAN_DIR/input.fofn"
	fi
	if [ -e "$CLEAN_DIR/data/filtered_regions.fofn" ]; then
		echo "Successfully found $CLEAN_DIR/data/filtered_regions.fofn"
	else
		echo "WARNING: Unable to find necessary input file, or dir $CLEAN_DIR/data/filtered_regions.fofn"
	fi
	echo 'Successfully validated input files'

	# Task align commands:
	# Completed writing Task align commands
	# Task 1
	compareSequences.py \
		--info --useGuidedAlign --algorithm=blasr --nproc=15  --noXML --h5mode=w \
		--h5fn=$newDATA/aligned_reads.cmp.h5 \
		--seed=1 --minAccuracy=0.75 --minLength=50 --useQuality  -x -minMatch 12 -x -bestn 10 -x -minPctIdentity 70.0 --placeRepeatsRandomly \
		--tmpDir=$locTEMP \
		--regionTable=$oldDATA/filtered_regions.fofn "$CLEAN_DIR/input.fofn" "$newREF" \
		|| exit $?
	echo "Task 1 completed at $(date)"

	# Task 2
	echo 'Alignment Complete' || exit $?
	echo "Task 2 completed at $(date)"

	# Task 3
	cp $oldDATA/chemistry_mapping.xml $newDATA/chemistry_mapping.xml
	loadPulses \
		$CLEAN_DIR/input.fofn \
		$newDATA/aligned_reads.cmp.h5 \
		-metrics DeletionQV,IPD,InsertionQV,PulseWidth,QualityValue,MergeQV,SubstitutionQV,DeletionTag -byread \
		|| exit $?
	echo "Task 3 completed at $(date)"

	# Task 4
	echo 'LoadPulses Complete' || exit $?
	echo "Task 4 completed at $(date)"
fi

########### TASK metadata #############
# Task            : sort
# Module          : P_Mapping
# TaskType        : None
# URL             : task://028069/P_Mapping/sort
# createdAt       : 2014-04-10 15:59:17.455605
# createdAt (UTC) : 2014-04-10 22:59:17.455638
# ncmds           : 1
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_Mapping/sort.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_Mapping/sort.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5
#
########### END TASK metadata #############

if [[ $sort -eq '1' ]]; then
	echo "Running task://028069/P_Mapping/sort on $(uname -a)"
	echo "Started on $(date -u)"
	echo 'Validating existence of Input Files'
	if [ -e "$newDATA/aligned_reads.cmp.h5" ]; then
		echo "Successfully found $newDATA/aligned_reads.cmp.h5"
	else
		echo "WARNING: Unable to find necessary input file, or dir $newDATA/aligned_reads.cmp.h5"
	fi
	echo 'Successfully validated input files'

	# Task sort commands:
	# Completed writing Task sort commands

	# Task 1
	cmph5tools.py -vv sort --deep --inPlace \
		$newDATA/aligned_reads.cmp.h5 \
		|| exit $?
	echo "Task 1 completed at $(date)"
fi


########### TASK metadata #############
# Task            : loadChemistry
# Module          : P_Mapping
# TaskType        : None
# URL             : task://028069/P_Mapping/loadChemistry
# createdAt       : 2014-04-10 15:59:17.312598
# createdAt (UTC) : 2014-04-10 22:59:17.312639
# ncmds           : 1
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_Mapping/loadChemistry.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_Mapping/loadChemistry.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/chemistry_mapping.xml
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5
#
########### END TASK metadata #############

if [[ $loadChemistry -eq '1' ]]; then
	echo "Running task://028069/P_Mapping/loadChemistry on $(uname -a)"
	echo "Started on $(date -u)"
	echo 'Validating existence of Input Files'
	if [ -e "$oldDATA/chemistry_mapping.xml" ]; then
		echo "Successfully found $oldDATA/chemistry_mapping.xml"
	else
		echo "WARNING: Unable to find necessary input file, or dir $oldDATA/chemistry_mapping.xml"
	fi
	if [ -e "$newDATA/aligned_reads.cmp.h5" ]; then
		echo "Successfully found $newDATA/aligned_reads.cmp.h5"
	else
		echo "WARNING: Unable to find necessary input file, or dir $newDATA/aligned_reads.cmp.h5"
	fi
	echo 'Successfully validated input files'

	# Task loadChemistry commands:
	# Completed writing Task loadChemistry commands

	# Task 1
	loadSequencingChemistryIntoCmpH5.py \
		--xml $oldDATA/chemistry_mapping.xml \
		--h5 $newDATA/aligned_reads.cmp.h5 \
		|| exit $?
	echo "Task 1 completed at $(date)"
fi


########### TASK metadata #############
# Task            : repack
# Module          : P_Mapping
# TaskType        : None
# URL             : task://028069/P_Mapping/repack
# createdAt       : 2014-04-10 15:59:17.834167
# createdAt (UTC) : 2014-04-10 22:59:17.834199
# ncmds           : 1
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_Mapping/repack.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_Mapping/repack.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5
#
########### END TASK metadata #############

if [[ $repack -eq '1' ]]; then
	echo "Running task://028069/P_Mapping/repack on $(uname -a)"
	echo "Started on $(date -u)"
	echo 'Validating existence of Input Files'
	if [ -e "$newDATA/aligned_reads.cmp.h5" ]; then
		echo "Successfully found $newDATA/aligned_reads.cmp.h5"
	else
		echo "WARNING: Unable to find necessary input file, or dir $newDATA/aligned_reads.cmp.h5"
	fi
	echo 'Successfully validated input files'

	# Task repack commands:
	# Completed writing Task repack commands

	# Task 1
	((which h5repack && \
		(h5repack -f GZIP=1 \
		$newDATA/aligned_reads.cmp.h5 \
		$newDATA/aligned_reads.cmp.h5_TMP \
		&& mv $newDATA/aligned_reads.cmp.h5_TMP $newDATA/aligned_reads.cmp.h5)) \
		|| echo 'no h5repack found, continuing w/out') \
		|| exit $?
	echo "Task 1 completed at $(date)"
fi


########### TASK metadata #############
# Task            : covGFF
# Module          : P_Mapping
# TaskType        : None
# URL             : task://028069/P_Mapping/covGFF
# createdAt       : 2014-04-10 15:59:17.923072
# createdAt (UTC) : 2014-04-10 22:59:17.923104
# ncmds           : 1
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_Mapping/covGFF.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_Mapping/covGFF.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/alignment_summary.gff
#
########### END TASK metadata #############

if [[ $covGFF -eq '1' ]]; then
	echo "Running task://028069/P_Mapping/covGFF on $(uname -a)"
	echo "Started on $(date -u)"
	echo 'Validating existence of Input Files'
	if [ -e "$newDATA/aligned_reads.cmp.h5" ]; then
		echo "Successfully found $newDATA/aligned_reads.cmp.h5"
	else
		echo "WARNING: Unable to find necessary input file, or dir $newDATA/aligned_reads.cmp.h5"
	fi
	echo 'Successfully validated input files'

	# Task covGFF commands:
	# Completed writing Task covGFF commands
	# Task 1
	summarizeCoverage.py \
		--reference $newREF \
		--numRegions=500 \
		$newDATA/aligned_reads.cmp.h5 \
		> $newDATA/alignment_summary.gff \
		|| exit $?
	echo "Task 1 completed at $(date)"
fi


########### TASK metadata #############
# Task            : unmapped
# Module          : P_Mapping
# TaskType        : None
# URL             : task://028069/P_Mapping/unmapped
# createdAt       : 2014-04-10 15:59:18.199346
# createdAt (UTC) : 2014-04-10 22:59:18.199378
# ncmds           : 1
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_Mapping/unmapped.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_Mapping/unmapped.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_subreads.fasta
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/unmappedSubreads.fasta
#
########### END TASK metadata #############

if [[ $unmapped -eq '1' ]]; then
	echo "Running task://028069/P_Mapping/unmapped on $(uname -a)"
	echo "Started on $(date -u)"
	echo 'Validating existence of Input Files'
	if [ -e "$oldDATA/filtered_subreads.fasta" ]; then
		echo "Successfully found $oldDATA/filtered_subreads.fasta"
	else
		echo "WARNING: Unable to find necessary input file, or dir $oldDATA/filtered_subreads.fasta"
	fi
	if [ -e "$newDATA/aligned_reads.cmp.h5" ]; then
		echo "Successfully found $newDATA/aligned_reads.cmp.h5"
	else
		echo "WARNING: Unable to find necessary input file, or dir $newDATA/aligned_reads.cmp.h5"
	fi
	echo 'Successfully validated input files'

	# Task unmapped commands:
	# Completed writing Task unmapped commands
	# Task 1
	extractUnmappedSubreads.py \
		$oldDATA/filtered_subreads.fasta \
		$newDATA/aligned_reads.cmp.h5  \
		> $newDATA/unmappedSubreads.fasta \
		|| exit $?
	echo "Task 1 completed at $(date)"
fi


########### TASK metadata #############
# Task            : samBam
# Module          : P_Mapping
# TaskType        : None
# URL             : task://028069/P_Mapping/samBam
# createdAt       : 2014-04-10 15:59:17.671938
# createdAt (UTC) : 2014-04-10 22:59:17.671971
# ncmds           : 1
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_Mapping/samBam.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_Mapping/samBam.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.bam.bai
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.bam
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.sam
#
########### END TASK metadata #############

if [[ $samBam -eq '1' ]]; then
	echo "Running task://028069/P_Mapping/samBam on $(uname -a)"
	echo "Started on $(date -u)"
	echo 'Validating existence of Input Files'
	if [ -e "/$newDATA/aligned_reads.cmp.h5" ]; then
		echo "Successfully found /$newDATA/aligned_reads.cmp.h5"
	else
		echo "WARNING: Unable to find necessary input file, or dir /$newDATA/aligned_reads.cmp.h5"
	fi
	echo 'Successfully validated input files'

	# Task samBam commands:
	# Completed writing Task samBam commands
	# Task 1
	pbsamtools.py --bam \
		--outfile $newDATA/aligned_reads.sam \
		--refrepos $newREF \
		--readGroup movie $newDATA/aligned_reads.cmp.h5 \
		|| exit $?
	echo "Task 1 completed at $(date)"
fi


echo "Finished on $(date -u)"
# Success
exit 0
