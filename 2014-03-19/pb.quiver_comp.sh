#!/bin/bash -l

# ayspunde 2014-03-19

# assertion: directory with already finished assembly
## bash /global/projectb/sandbox/rqc/ayspunde/pacbio/2014-03-19/pb.quiver_comp.sh


CLEAN_DIR=`pwd`
DIRTY_DIR=$(echo $CLEAN_DIR | sed  -e "s/-cleanP4.*//")
PB_JOB_ID=$(echo $(basename $DIRTY_DIR) | sed 's/^0//')

printf "Starting 'pb.quiver_comp.sh' in %s\n\n" $CLEAN_DIR
# exit

quiver_cond=('m0diN' 'm0diY' 'm20diN' 'm20diY')
quiver_summary='quiver_summary.txt'

if [ ! -d $CLEAN_DIR/data-m0diN ]; then
	printf "ERROR: no quiver folders: exiting\n\n"
	exit
fi
if [  -e $quiver_summary ]; then
	rm $quiver_summary
fi


for  ((i=0; i < ${#quiver_cond[@]} ; i++)); do
	printf "%s\t" `pwd` >> $quiver_summary
	this_cond=${quiver_cond[$i]}
	this_dir=$CLEAN_DIR/'data-'${this_cond}
	corr_gff=$this_dir/corrections.gff
	corr_gff_trunc=$this_dir/corrections_trunc.gff
	ls -al $corr_gff
	head -n 20 $corr_gff | grep -v '#' | sed 's/;coverage.*//'
	cat $corr_gff | grep -v '#' | sed 's/;coverage.*//' > $corr_gff_trunc

	corrN=$(cat $corr_gff | wc -l)
	printf "%d\n" $corrN
	printf "quiver_cond=%s, corrN=%d\n" $this_cond $corrN >> $quiver_summary

	# break
done
q1=$CLEAN_DIR/'data-m0diN/corrections_trunc.gff'
q2=$CLEAN_DIR/'data-m20diN/corrections_trunc.gff'
printf "diff: -y --suppress-common-lines *m0diN* *m20diN*\n" >> $quiver_summary
diff -yw --suppress-common-lines $q1 $q2 >> $quiver_summary

