#!/bin/env bash

# ayspunde 2014-03-19

# driver for pb.*.sh : looping over whole project
##> bash /global/projectb/sandbox/rqc/ayspunde/pacbio/DEFAULT/pb.MAIN.sh 


# -- PACBIO_LOCAL is your working dir -- #
# PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10'
# PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc21'
# PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31'
PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc41'
# PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc53'


VERSION2RUN='/global/projectb/sandbox/rqc/ayspunde/pacbio/DEFAULT'


cd $PACBIO_LOCAL
BIG_LOG=$(pwd)/log.$(date +%F.%H%M).txt



# things2do=(asm_qc)
# things2do=(repolish)
things2do=(cleanP4)
# things2do=(prelim_qc)
# things2do=(circle)
things2do=(run_hacksaw_qc_only)
# things2do=(final)

###
### choose how to run: for selected dirs, uncomment jName/jID; for the whole group, use $PACBIO_LOCAL/s* 
###
# 2014-04-02: re-running chemistry mis-matched data



# # seqqc21 - hacksaw-stats: no deg contigs
# jName=(s1067=27717 s749=27752 s754=27753 s755=27677 s758=27678 s760=27679 s861=27332)
# jName=(s804=27538)
# jName=(s803=27539 s1043=27513)
# jName=(s735=27390 s750=27800) 
# jName=(s930=28036) 
# jName=(s735=27390 s750=27800 s803=27539 s930=28036 s1043=27513)
## proceed to hacksaw:
# jName=(s940=27706)

# # seqqc31
# jName=(s1210=28072 s1225=28102 s1225=28103 s974=28092)
# jName=(s1209=28070 s1210=28071 s781=28044 s782=28048 s782=28124 s783=28051 s860=28057 s860=28058 s929=28055 s934=28099 s934=28100 s970=28097 s972=28060 s972=28061 s974=28090 s975=28127) 
# jName=(s781=28045 s782=28124 s783=28051 s929=28056 s970=28098 s975=28127)
# jName=(s970=28098 s975=28127)
# jName=(s1209=28069 s1209=28070 s1286=28108 s1286=28110 s1312=28111 s1312=28112 s1313=28155 s1313=28156 s1314=28157 s1314=28158 s1315=28160 s1315=28162 s1316=28164 s1317=28181)
# jName=(s971=28094 s971=28095 s972=27755 s973=27721 s1211=28150 s1211=28151 s1212=28152 s1212=28153 s1316=28165 s1317=28183 s1286=28108)
# jName=(s1316=28165 s1317=28183)

# proceed to hacksaw:
# jName=(s1209=28069 s1313=28155 s1314=28158 s1315=28162)
# jName=(s934=28100 s972=28061 s1211=28151 s1212=28153 s1286=28108 s1312=28112)


# # seqqc41
jName=(s1226=28673 s1226=28674)
# prelim/circ/hacksaw
jName=(s1418=28665 s1419=28678 s1420=28680 s1424=28685 s1424=28686 s1749=28690)
# jName=(s1208=28067 s1208=28147 s1749=28690)
# jName=(s619=28741 s619=28742)
# jName=(s1208=28147)

# # seqqc53
# jName=(s734=28052 s750=28051 s735=28050 s703=28049 s1064=28048 s1043=28047 s1011=28045)
## Hg2
# jName=(s2063=28809  s2064=28811)
# ## Hg3
jName=(s2063=28810 s2064=28812)



for ((i=0; i < ${#jName[@]} ; i++)); do
    # option as a single list of key-value pairs
	J=${jName[$i]}
	JIRA_NAME=$(echo $J | sed 's/\(.*\)=\(.*\)/\1/')
    PB_JOB_ID=$(echo $J | sed 's/\(.*\)=\(.*\)/\2/')
	# # # option as individual name/id lists
	# # JIRA_NAME=${jName[$i]}
    # # PB_JOB_ID=${jID[$i]}
	DIRTY_DIR=$PACBIO_LOCAL/$JIRA_NAME/0$PB_JOB_ID
	CLEAN_DIR=$DIRTY_DIR-cleanP4

# i=0
# for s_dir in $PACBIO_LOCAL/s*/*-cleanP4 ; do
# # for s_dir in $PACBIO_LOCAL/s*/* ; do
	# (( i++ ))
	# ## easy switch between $PACBIO_LOCAL/s*/*-cleanP4 [clean-workflow] | $PACBIO_LOCAL/s*/* [dirty-workflow]
	# CLEAN_DIR=$s_dir				
	# DIRTY_DIR=$(echo $CLEAN_DIR | sed  -e "s/-cleanP4.*//")
	# # DIRTY_DIR=$s_dir				
	# # CLEAN_DIR=$DIRTY_DIR-cleanP4 
	# # PB_JOB_ID=$(echo $(basename $DIRTY_DIR) | sed 's/^0//')
		
# i=0
# for s_dir in $PACBIO_LOCAL/s*/*-cleanP4-x ; do	
	# (( i++ ))
	# DIRTY_DIR=$s_dir									## easy to switch to $PACBIO_LOCAL/s*/*-cleanP4
	# CLEAN_DIR=$(echo $DIRTY_DIR | sed  -e "s/-x//") 	## DIRTY_DIR=$(echo $CLEAN_DIR | sed  -e "s/-cleanP4.*//")
	# PB_JOB_ID=$(echo $(basename $(echo $DIRTY_DIR | sed  -e "s/-cleanP4-x//")) | sed 's/^0//')
	
	printf "\n\n\nN=%d\nJOB_ID=%d\nDIRTY_DIR=%s\nCLEAN_DIR=%s\n\n\n" $i $PB_JOB_ID $DIRTY_DIR $CLEAN_DIR
	if [ ! -d $CLEAN_DIR ]; then
		mkdir -p -m 775 $CLEAN_DIR 
	fi
	cd $CLEAN_DIR
	printf "Starting thigs2do in %s\n\n" `pwd` | tee -a $BIG_LOG	
	# continue
	# break  ## to check on a single dir
	
	for ((j=0; j < ${#things2do[@]} ; j++)); do
		todo=${things2do[$j]}
		echo $todo 
		cd $CLEAN_DIR
		# continue
		case $todo in
		'cleanP4')
			SCRIPT2RUN=$VERSION2RUN/pb.cleanp4.sh
			WORK_DIR=$CLEAN_DIR
			cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			# $SCRIPT2RUN	2>&1 | tee -a $BIG_LOG		
			# option for large N of jobs: qsub, to avoid waiting on quiver
			qsub -S /bin/bash -cwd -j yes -w e -b yes -M `whoami`@lbl.gov -m bea \
				-l h_rt=12:00:00 -l ram.c=5G -pe pe_slots 8 -N clean${PB_JOB_ID} -P prok-assembly.p \
				"$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG"  			
			;;
		'asm_qc')
			SCRIPT2RUN=$VERSION2RUN/pb.asm_qc.sh
			WORK_DIR=$CLEAN_DIR/asmQC		
			if [ ! -d $WORK_DIR ]; then mkdir $WORK_DIR ; fi
			cd $WORK_DIR #&&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG
			;;
		'prelim_qc')
			SCRIPT2RUN=$VERSION2RUN/pb.prelim_qc.sh
			WORK_DIR=$CLEAN_DIR/prelimQC		
			if [ ! -d $WORK_DIR ]; then mkdir $WORK_DIR ; fi
			cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG
			;;
		'circle')
			SCRIPT2RUN=$VERSION2RUN/pb.circle.sh
			WORK_DIR=$CLEAN_DIR/circularize
			if [ ! -d $WORK_DIR ]; then mkdir -m 775 $WORK_DIR ; fi
			cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG
			;;
		'repolish')
			SCRIPT2RUN=$VERSION2RUN/pb.re_polish.sh
			WORK_DIR=$CLEAN_DIR
			if [ ! -d $WORK_DIR/data2 ]; then mkdir -m 775 $WORK_DIR/data2 ; fi
            if [ ! -d $WORK_DIR/temp2 ]; then mkdir -m 775 $WORK_DIR/temp2 ; fi
			cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG
			;;
		'run_hacksaw_qc_only')
			SCRIPT2RUN=$VERSION2RUN/pb.hacksaw_qc_only.sh
			WORK_DIR=$CLEAN_DIR
			cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG &
			;;
		'my_qc')
			SCRIPT2RUN=$VERSION2RUN/pb.myqc.sh
			WORK_DIR=$CLEAN_DIR/myQC
			if [ ! -d $WORK_DIR ]; then  mkdir -m 775 $WORK_DIR ; fi
			cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG
			;;
		'final')
			SCRIPT2RUN=$VERSION2RUN/pb.hacklinks.sh
			WORK_DIR=$CLEAN_DIR/final
			if [ -d $WORK_DIR ]; then 
				cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG 
				$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG
			else
				printf "\n\nFAILED %s: %s is not a valid dir\n\n"  $todo $WORK_DIR  | tee -a $BIG_LOG 
			fi
			;;
		*)
			printf "\n\n -- !! -- No workflow for case: %s -- ?? -- \n\n" $todo | tee -a $BIG_LOG
			;;
		esac
		cd $CLEAN_DIR
		chgrp -R genome `pwd` &&  chmod -R 775 `pwd` &
	done
	# break  ## to check on a single dir
done
