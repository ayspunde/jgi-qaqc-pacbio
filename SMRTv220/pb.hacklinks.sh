#!/bin/bash

# post-hacksaw hack :) 
# currently no final links produced in the /final; this is to make /final release-ready

## ASSUMPTIONS: 
#1	start from ./final
#2	specific for Hgap3/SMRT 2.2: custom-made no_control_filtered_reads.fast are in /data2

#>  bash /global/projectb/sandbox/rqc/ayspunde/pacbio/SMRTv220/pb.hacklinks.sh &

# ayspunde 2014-05-28

################################################################
### LIST OF FILES
################################################################
# raw.h5.reads.tar.gz
# filtered.subreads.fastq.gz
# error.corrected.reads.fastq.gz
# QC.finalReport.pdf - same as for other product types
# QD.finalReport.txt  - same as for other product types
# final.assembly.fasta - not zipped, since we haven't before
# final.assembly.topology.txt
#################################################################


LOC_DIR=`pwd`
LINK_LOG=$LOC_DIR/../create_links.log
just_check=0

# rm $LINK_LOG
# re-run final_report in case file was written before fixing nReads correction
# /global/projectb/sandbox/rqc/prod/pipelines/hacksaw/DEFAULT/bin/hacksaw_txt.pl ../hacksaw_stats.txt QD.finalReport.txt

printf "\n\nStarting LINKing for FINAL in %s\n\n" "$LOC_DIR"

# file2check=(rawh5 filtfqgz corrfqgz qcpdf qdtxt asmfa asmtopo)
file2check=(rawh5 filtfqgz corrfqgz asmfa asmtopo)
# file2check=(rawh5)

for ((i=0; i < ${#file2check[@]} ; i++)); do
	fileType=${file2check[$i]}
    echo $fileType
    case $fileType in
    'rawh5')
		OUT_FILE=raw.h5.reads.tar
        if [ ! -e  $OUT_FILE ]; then
			printf "Starting file_type=%s\n" $fileType | tee -a $LINK_LOG
			if (( $just_check )) ; then
				printf "just checking\n" | tee -a $LINK_LOG
			elif  [  -d $LOC_DIR/../runs ]; then
				printf "Found /runs: use clean*h5\n" | tee -a $LINK_LOG
				ls -al ../runs/*/*/Analysis_Results/*.h5 | tee -a $LINK_LOG 
				tar cpvh --file=$OUT_FILE ../runs/*/*/Analysis_Results/*.h5  2>&1 | tee -a $LINK_LOG &
				wait
				ls -al ../runs/*/*/*.metadata.xml | tee -a $LINK_LOG 
				tar rpvh --file=$OUT_FILE ../runs/*/*/*.metadata.xml  2>&1 | tee -a $LINK_LOG &
				wait
				gzip $OUT_FILE 2>&1 | tee -a $LINK_LOG
			else
				printf "No /runs: use original*h5\n" | tee -a $LINK_LOG
				INPUT_XML=$LOC_DIR/../input.xml
				while read xml_lane; do
					if (( $(expr "$xml_lane" : '<location>') > 0 )) ; then
						h5_path=$(echo $xml_lane | sed  -e 's/<location>//' -e  's/<\/location>//')
						echo $h5_path
						ln -s $h5_path ./
						meta_xml=$(dirname $h5_path)/../*metadata.xml
						echo $meta_xml
						ln -s $meta_xml ./
					fi
				done < $INPUT_XML
				ls -al ./*ba[xs].h5 | tee -a $LINK_LOG 
				tar cpvh --file=$OUT_FILE ./*ba[xs].h5  2>&1 | tee -a $LINK_LOG &
				wait
				ls -al ./*metadata.xml | tee -a $LINK_LOG 
				tar rpvh --file=$OUT_FILE ./*metadata.xml  2>&1 | tee -a $LINK_LOG &
				wait
				gzip $OUT_FILE 2>&1 | tee -a $LINK_LOG
				rm *bax.h5 *metadata.xml
			fi
			printf "Done with file_type=%s\n\n" $fileType | tee -a $LINK_LOG			
        else
			printf "Already been done: file_type=%s\n\n" $fileType | tee -a $LINK_LOG
		fi
		;;
    'filtfqgz')
		OUT_FILE=filtered.subreads.fastq.gz
		INP_FILE=$LOC_DIR/../data2/no_control_filtered_subreads.fastq
        if [ ! -e  $OUT_FILE ]; then
			printf "Starting file_type=%s\n" $fileType | tee -a $LINK_LOG
			if (( $just_check )) ; then
				printf "just checking\n" | tee -a $LINK_LOG
			elif [ -e  $INP_FILE ]; then
				printf "Found no_control_filtered_reads.fastq: gzip and copy to /final\n" | tee -a $LINK_LOG
				ls -al $INP_FILE | tee -a $LINK_LOG 
				gzip -c $INP_FILE > $OUT_FILE & #2>&1 | tee -a $LINK_LOG &
				wait	
			else
				printf "\NWARNING: no 'no_control_filtered_reads.fastq' in DIR=%s\n\n" $LOC_DIR | tee -a $LINK_LOG
			fi
			printf 'Done with file_type=%s\n\n' $fileType | tee -a $LINK_LOG		
        else
			printf "Already been done: file_type=%s\n\n" $fileType | tee -a $LINK_LOG
		fi
        ;;  
    'corrfqgz')
		OUT_FILE=error.corrected.reads.fastq.gz
		INP_FILE=$LOC_DIR/../data2/no_control_corrected.fastq
        if [ ! -e $OUT_FILE ]; then
			printf "Starting file_type=%s\n" $fileType | tee -a $LINK_LOG
			if (( $just_check )) ; then
				printf "just checking\n" | tee -a $LINK_LOG
			else
				ls -al $INP_FILE | tee -a $LINK_LOG 
				gzip -c $INP_FILE > $OUT_FILE 2>&1 | tee -a $LINK_LOG & #2>&1 
				wait	
			fi
			printf 'Done with file_type=%s\n\n' $fileType | tee -a $LINK_LOG
        else
			printf "Already been done: file_type=%s\n\n" $fileType | tee -a $LINK_LOG
		fi        
		;;          
    'qcpdf')
        if [  ! -e QC.finalReport.pdf ]; then
			printf "Starting file_type=%s\n" $fileType | tee -a $LINK_LOG  
			if (( $just_check )) ; then
				printf "just checking\n" | tee -a $LINK_LOG
			else			
				mv -v QC*pdf ./QC.finalReport.pdf 2>&1 | tee -a $LINK_LOG &
				wait       
			fi
			printf 'Done with file_type=%s\n\n' $fileType | tee -a $LINK_LOG
		else
			printf "Already been done: file_type=%s\n\n" $fileType | tee -a $LINK_LOG
		fi
        ;;  
    'qdtxt')
        if [ ! -e QD.finalReport.txt ]; then
			printf "Starting file_type=%s\n" $fileType | tee -a $LINK_LOG        
			if (( $just_check )) ; then
				printf "just checking\n" | tee -a $LINK_LOG
			else		
				mv -v QD*txt ./QD.finalReport.txt 2>&1 | tee -a $LINK_LOG &
				wait
			fi
			printf 'Done with file_type=%s\n\n' $fileType | tee -a $LINK_LOG	 
        else
			printf "Already been done: file_type=%s\n\n" $fileType | tee -a $LINK_LOG
		fi
        ;;              
    'asmfa')
		OUT_FILE=final.assembly.fasta
		INP_FILE=$LOC_DIR/../data2/polished_assembly.fasta.gz
        if [ ! -e $OUT_FILE ]; then
			printf "Starting file_type=%s\n" $fileType | tee -a $LINK_LOG
			if (( $just_check )) ; then
				printf "just checking\n" | tee -a $LINK_LOG
			else
				if [ -e $INP_FILE ]; then 
					printf "Found 'polished_assembly.fasta.gz': copy to /final\n" | tee -a $LINK_LOG
					ls -al $INP_FILE | tee -a $LINK_LOG &
					gunzip -c $INP_FILE > $OUT_FILE 2>&1 | tee -a $LINK_LOG &
				else
					printf "\n\nWARNING: No '%s' in DIR=%sl\n" $OUT_FILE  $LOC_DIR| tee -a $LINK_LOG
				fi
				wait       
			fi
			printf 'Done with file_type=%s\n\n' $fileType | tee -a $LINK_LOG	
		else
			printf "Already been done: file_type=%s\n\n" $fileType | tee -a $LINK_LOG
		fi
        ;;  
    'asmtopo')
        if [ ! -e final.assembly.topology.txt ]; then
			printf "Starting file_type=%s\n" $fileType | tee -a $LINK_LOG		
            if (( $just_check )) ; then
				printf "just checking\n" | tee -a $LINK_LOG
			else
				ls -al ../circularize/finalContigs.fasta.topo.txt | tee -a $LINK_LOG	 
				cp -v ../circularize/finalContigs.fasta.topo.txt ./final.assembly.topology.txt 2>&1 | tee -a $LINK_LOG &
				wait    
			fi
			printf 'Done with file_type=%s\n\n' $fileType | tee -a $LINK_LOG
		else
			printf "Already been done: file_type=%s\n\n" $fileType | tee -a $LINK_LOG
		fi
        ;;      
    esac
done
wait

printf "Checking *gz's in DIR=%s\n\n" "$LOC_DIR"
file *gz | tee -a $LINK_LOG
printf "Checking all file types in DIR=%s\n\n" "$LOC_DIR"
file * | tee -a $LINK_LOG

printf "\n\nDone LINKing for FINAL in DIR=%s\n\n" "$LOC_DIR"
