#!/bin/bash

########### TASK metadata #############
# Task            : outputFiles
# Module          : P_PreAssembler
# TaskType        : None
# URL             : task://028069/P_PreAssembler/outputFiles
# createdAt       : 2014-03-19 16:22:07.656272
# createdAt (UTC) : 2014-03-19 23:22:07.656297
# ncmds           : 3
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_PreAssembler/outputFiles.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_PreAssembler/outputFiles.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/corrected.bnk
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.fasta
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.fastq
#
########### END TASK metadata #############

# Writing to log file
cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_PreAssembler/outputFiles.sh >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_PreAssembler/outputFiles.log;



echo "Running task://028069/P_PreAssembler/outputFiles on $(uname -a)"

echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/corrected.bnk ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/corrected.bnk'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/corrected.bnk.'
fi
echo 'Successfully validated input files'

# Task outputFiles commands:


# Completed writing Task outputFiles commands


# Task 1
bank2fasta -b /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/corrected.bnk -fastq /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.fastq > /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.fasta || exit $?
echo "Task 1 completed at $(date)"
# Task 2
trimFastqByQVWindow.py /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.fastq --qvCut=59.5 --minSeqLen=500 --out /global/projectb/shared/software/smrtanalysis/2.1.1/tmpdir/tmpqhUGjy/tmp.fastq --fastaOut /global/projectb/shared/software/smrtanalysis/2.1.1/tmpdir/tmpqhUGjy/tmp.fasta || exit $?
echo "Task 2 completed at $(date)"
# Task 3
mv /global/projectb/shared/software/smrtanalysis/2.1.1/tmpdir/tmpqhUGjy/tmp.fastq /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.fastq; mv /global/projectb/shared/software/smrtanalysis/2.1.1/tmpdir/tmpqhUGjy/tmp.fasta /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.fasta || exit $?
echo "Task 3 completed at $(date)"



echo "Finished on $(date -u)"
# Success
exit 0
