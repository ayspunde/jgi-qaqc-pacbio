#!/bin/bash

########### TASK metadata #############
# Task            : genFrgFile
# Module          : P_CeleraAssembler
# TaskType        : None
# URL             : task://028069/P_CeleraAssembler/genFrgFile
# createdAt       : 2014-03-19 16:22:07.699365
# createdAt (UTC) : 2014-03-19 23:22:07.699389
# ncmds           : 1
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_CeleraAssembler/genFrgFile.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_CeleraAssembler/genFrgFile.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.fastq
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.frg
#
########### END TASK metadata #############

# Writing to log file
cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_CeleraAssembler/genFrgFile.sh >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_CeleraAssembler/genFrgFile.log;



echo "Running task://028069/P_CeleraAssembler/genFrgFile on $(uname -a)"

echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.fastq ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.fastq'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.fastq.'
fi
echo 'Successfully validated input files'

# Task genFrgFile commands:


# Completed writing Task genFrgFile commands


# Task 1
fastqToCA -technology sanger -type sanger -reads /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.fastq -libraryname 028069 > /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.frg || exit $?
echo "Task 1 completed at $(date)"



echo "Finished on $(date -u)"
# Success
exit 0
