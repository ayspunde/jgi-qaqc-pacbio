#!/bin/env bash

# driver for pb.*.sh : looping over whole project
# version for SMRT 2.2.0p2 (Hgap3)

# ayspunde 2014-03-19

##>  bash /global/projectb/sandbox/rqc/ayspunde/pacbio/SMRTv220/pb.MAIN.sh

VERSION2RUN='SMRTv220'
PATH2SCIPTS="/global/projectb/sandbox/rqc/ayspunde/pacbio/$VERSION2RUN"


### --- WHERE: directories to keep the results --- ###
# PACBIO_PORTAL is SDM repository of SMRT Portl jobs
# PACBIO_LOCAL is your working dir
#

# PACBIO_PORTAL='/global/projectb/scratch/smrt_dev/userdata/jobs'
# PACBIO_PORTAL='/global/projectb/scratch/smrt_dev/install_smrt_2.2/smrtanalysis2.2/userdata/jobs'
PACBIO_PORTAL='/global/projectb/shared/pacbio/jobs'

# PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10'
# PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc13'
# PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc21'
# PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc53'
# PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc41'
PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc42'


### --- WHAT: function to perform --- ###
###
# USE_PB_FILT=0
USE_PB_FILT=1

###
# things2do=(rlink)
# things2do=(clean_p4)
# things2do=(cp2data2)
# #!$ - run matlab cleaning script - $!#
# things2do=(prelim_qc)
# things2do=(circle)
# things2do=(run_hacksaw_qc_only)
things2do=(final)



### --- WHO: individual data sets --- ###
### choose how to run: for selected dirs, uncomment jName/jID; for the whole group, use $PACBIO_LOCAL/s* 
###

## seqqc41
# jName=(s2713=29572)
# jName=(s2801=29649 s2802=29672 s2802=29673 s2804=29674  s2806=29869 s2807=29677 s2807=29678 s2808=29657 s2808=29680)
jName=(s2806=29869 s2807=29870 s2848=29788 s2850=29790 s2851=29791)
jName=(s2848=29788 s2850=29790 s2851=29791)
jName=(s2848=29788 s2850=29790 s2851=29791 s3440=29974 s3441=29972 s3442=29973 s3443=29971)
# jName=(s3440=29974 s3441=29972)

cd $PACBIO_LOCAL
BIG_LOG=$(pwd)/log.$(date +%F).txt


for ((i=0; i < ${#jName[@]} ; i++)); do
    # option as a single list of key-value pairs
	J=${jName[$i]}
	JIRA_NAME=$(echo $J | sed 's/\(.*\)=\(.*\)/\1/')
    PB_JOB_ID=$(echo $J | sed 's/\(.*\)=\(.*\)/\2/')
	# # # option as individual name/id lists
	# # JIRA_NAME=${jName[$i]}
    # # PB_JOB_ID=${jID[$i]}
	L1NN=$(echo $PB_JOB_ID | sed 's/\([0-9]\{2\}\).*/\1/')
    PORTAL_JOB=$PACBIO_PORTAL/0${L1NN}/0${PB_JOB_ID}
	DIRTY_DIR=$PACBIO_LOCAL/$JIRA_NAME/0$PB_JOB_ID
	CLEAN_DIR=$PACBIO_LOCAL/$JIRA_NAME/0$PB_JOB_ID-cleanP4

# i=0
# for s_dir in $PACBIO_LOCAL/s*/*-cleanP4 ; do
# # for s_dir in $PACBIO_LOCAL/s*/* ; do
	# (( i++ ))
	# ## easy switch between $PACBIO_LOCAL/s*/*-cleanP4 [clean-workflow] | $PACBIO_LOCAL/s*/* [dirty-workflow]
	# CLEAN_DIR=$s_dir				
	# DIRTY_DIR=$(echo $CLEAN_DIR | sed  -e "s/-cleanP4.*//")
	# # DIRTY_DIR=$s_dir				
	# # CLEAN_DIR=$DIRTY_DIR-cleanP4 
	# # PB_JOB_ID=$(echo $(basename $DIRTY_DIR) | sed 's/^0//')
		
	
	printf "\n\n\nN=%d\nJOB_ID=%d\nDIRTY_DIR=%s\nCLEAN_DIR=%s\n\n\n" $(($i+1)) $PB_JOB_ID $DIRTY_DIR $CLEAN_DIR
	if [ ! -d $CLEAN_DIR ]; then
		mkdir -p -m 775 $CLEAN_DIR
	fi
	cd $CLEAN_DIR
	printf "Starting things2do in %s\n\n" `pwd` | tee -a $BIG_LOG	
	# continue
	# break  ## to check on a single dir
	
	for ((j=0; j < ${#things2do[@]} ; j++)); do
		todo=${things2do[$j]}
		echo $todo 
		cd $CLEAN_DIR
		# continue
		case $todo in
		'rlink')
			SCRIPT2RUN=$PATH2SCIPTS/pb.rlink.sh
			WORK_DIR=$CLEAN_DIR		
			cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			$SCRIPT2RUN $PORTAL_JOB $DIRTY_DIR $CLEAN_DIR $USE_PB_FILT 2>&1 | tee -a $BIG_LOG
			printf "Finished %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			;;		
		'prelim_qc')
			SCRIPT2RUN=$PATH2SCIPTS/pb.prelim_qc.sh
			WORK_DIR=$CLEAN_DIR/prelimQC		
			if [ ! -d $WORK_DIR ]; then mkdir $WORK_DIR ; fi
			cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG
			printf "Finished %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			;;		
		'clean_p4')
			SCRIPT2RUN=$PATH2SCIPTS/pb.cleanp4.sh
			WORK_DIR=$CLEAN_DIR
			cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			
			# ### === on the login node (-l exclusive.c || -l ram.c=5G -pe pe_slots 8)
			# $SCRIPT2RUN	$USE_PB_FILT 2>&1 | tee -a $BIG_LOG		
			
			### === qsub @norm.q (option for large N of jobs, to avoid waiting on quiver) 
			# qsub -S /bin/bash -cwd -j yes -w e -b yes -M `whoami`@lbl.gov -m bea \
				# -l h_rt=1:00:00 -l ram.c=5G -pe pe_slots 8 -N clean${PB_JOB_ID} -P prok-assembly.p \
				# "$SCRIPT2RUN $USE_PB_FILT 2>&1 | tee -a $BIG_LOG"  	
			
			### === qsub @normal_excl.q (option for large N of jobs, to avoid waiting on quiver) 
			qsub -S /bin/bash -cwd -j yes -w e -b yes -M `whoami`@lbl.gov -m bea \
				-l h_rt=1:00:00 -l ram.c=6G -pe pe_slots 16 -N clean${PB_JOB_ID} -P prok-assembly.p \
				"$SCRIPT2RUN $USE_PB_FILT 2>&1 | tee -a $BIG_LOG"  
			printf "Finished %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			;;
		'circle')
			SCRIPT2RUN=$PATH2SCIPTS/pb.circle.sh
			WORK_DIR=$CLEAN_DIR/circularize
			if [ ! -d $WORK_DIR ]; then mkdir -m 775 $WORK_DIR ; fi
			cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG
			printf "Finished %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			;;
		'run_hacksaw_qc_only')
			SCRIPT2RUN=$PATH2SCIPTS/pb.hacksaw_qc_only.sh
			WORK_DIR=$CLEAN_DIR
			cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG
			printf "Finished %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			;;
		'final')
			SCRIPT2RUN=$PATH2SCIPTS/pb.hacklinks.sh
			WORK_DIR=$CLEAN_DIR/final
			if [ ! -d $WORK_DIR ]; then mkdir -m 775 $WORK_DIR ; fi 
			printf "Starting pre-final in WORK_DIR=%s\n\n"  $todo `pwd` | tee -a $BIG_LOG 
			cd $CLEAN_DIR && if [ ! -e 'project_desc_file.txt' ]; then
				/global/projectb/sandbox/rqc/prod/pipelines/hacksaw/DEFAULT/bin/hacksaw_pdf.pl -pp > project_desc_file.txt
			else
				sed -i 's/assemblyVersion.row.hgap=/assemblyVersion.row.hgap=2.2.0.p1/' hacksaw_stats.txt
				/global/projectb/sandbox/rqc/prod/pipelines/hacksaw/DEFAULT/bin/hacksaw_pdf.pl project_desc_file.txt 2>&1 >/dev/null
			fi
			rm metadata.json
			mkdir .fasta && mv *fasta ./.fasta 
			mkdir .old && mv *old ./.old
			mkdir .log.loc && mv *.o* .log.loc ; mv *.po* .log.loc ; mv *log .log.loc
			cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG 
			$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG &
			printf "Finished %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG			
			;;
		'cp2data2')
			SCRIPT2RUN=$PATH2SCIPTS/pb.cp2data2.sh
			WORK_DIR=$CLEAN_DIR/data2
			if [ ! -d $WORK_DIR ]; then mkdir -m 775 $WORK_DIR ; fi
			cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG 
			$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG
			printf "Finished %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			;;
		*)
			printf "\n\n -- !! -- No workflow for case: %s -- ?? -- \n\n" $todo | tee -a $BIG_LOG
			;;
		esac
		cd $CLEAN_DIR
		chgrp -R genome `pwd` &&  chmod -R 775 `pwd` &
	done
	# break  ## to check on a single dir
done
