#!/bin/bash

## needed before staring hacksaw, run JKH's circularization on the cluster (high-mem)
# assuming checked for duplications: pb.prelim_qc.sh
# and completed de-duplication
## WORK_DIR=$CLEAN_DIR/circularize

##> bash /global/projectb/sandbox/rqc/ayspunde/pacbio/SMRTv220/pb.circle.sh

# ayspunde 2014-05-08: hgap3 version

####################################################

# !! -- make sure deduplication was completed -- !!

####################################################

##
## -- WHERE we are
##
WORK_DIR=$(pwd);
seqID=$( basename $(realpath ./../..))
runID=$(echo $(basename $(realpath ./..)) | sed 's/-cleanP4//')

##
## SET ENV
##
TRIM_AND_CIRCLE='/usr/common/jgi/qaqc/jgi-qaqc/detectCircle/trimAndTopologyUsingPb.py'
CIRCLE_INPUT="$WORK_DIR/../prelimQC/polished_assembly.dedup.fasta"
FILTERED_FASTA="$WORK_DIR/../data/filtered_subreads.fasta"
FILTERED_FASTA_NOCONTROL="$WORK_DIR/../data2/no_control_filtered_subreads.fasta"

##
## DO THE JOB
##

date
printf "Starting circularization:\nCLEAN_DIR=%s\n\n" $WORK_DIR/..

if [ -e $FILTERED_FASTA_NOCONTROL ]; then
	FILT_READS_FASTA=$FILTERED_FASTA_NOCONTROL
else
	FILT_READS_FASTA=$FILTERED_FASTA
fi
ls -al $FILT_READS_FASTA $CIRCLE_INPUT 
# $TRIM_AND_CIRCLE -r $CIRCLE_INPUT -f $FILT_READS_FASTA
if [[ ! -e $FILT_READS_FASTA || ! -e $CIRCLE_INPUT  ]]; then
	printf "Can't start circularization in WORK_DIR=%s\n" $WORK_DIR
	printf "missing one of these:\n"
	printf "FILT_READS_FASTA=%s\n"  $FILT_READS_FASTA
	printf "CIRCLE_INPUT=%s\n"     $CIRCLE_INPUT
	date
	printf "\n\n"
	exit 1
else
	printf "Start circularization:\nTHIS_DIR=%s\n\n" $WORK_DIR
	date
	
	### == no QSUB
	# $TRIM_AND_CIRCLE -r $CIRCLE_INPUT -f $FILT_READS_FASTA
	
	### == normal.q
	# qsub -S /bin/bash -cwd -j yes -w e -b yes \
	# -l h_rt=1:00:00 -l ram.c=5G -pe pe_slots 8 -N circ$runID -P prok-assembly.p \
	# "$TRIM_AND_CIRCLE -r $CIRCLE_INPUT -f $FILT_READS_FASTA" 
	
	## == normal_excl.q
	qsub -S /bin/bash -cwd -j yes -w e -b yes \
	-l h_rt=1:00:00 -l ram.c=7.5G -pe pe_slots 16 -N circ$runID -P prok-assembly.p \
	"$TRIM_AND_CIRCLE -r $CIRCLE_INPUT -f $FILT_READS_FASTA" 
fi
exit 0




