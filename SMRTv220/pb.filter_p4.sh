#!/bin/bash

# ayspunde 2014-05-21

# temp solution for SMRT 2.2 bugs:
## no no_control_filtered.reads.FASTQ && \
## no_control_filtered.reads.FASTA still have many hits to P4 controls 

## - assumptions: TODO
#>  bash /global/projectb/sandbox/rqc/ayspunde/pacbio/SMRTv220/pb.filter_p4.sh

##
## SET ENV
##
WORK_DIR=`pwd`

P4_ALIGN=$WORK_DIR/../cleanP4/p4c.alignment.*.txt
FILT_READS_FA_FP_DIRTY=$WORK_DIR/filtered_subreads.fasta
FILT_READS_FQ_FP_DIRTY=$WORK_DIR/filtered_subreads.fastq
FILT_READS_FA_FP_CLEAN=$WORK_DIR/no_control_filtered_subreads.fasta
FILT_READS_FQ_FP_CLEAN=$WORK_DIR/no_control_filtered_subreads.fastq
FILT_READS_FA_FP_CONTR=$WORK_DIR/control_filtered_subreads.fasta
FILT_READS_FQ_FP_CONTR=$WORK_DIR/control_filtered_subreads.fastq


# 

p4align=$(cat $P4_ALIGN)
	
	
# breakx=3
while read fasta_line; do
	isHeader=$(expr "$fasta_line" : '>')
	if [[ $isHeader -gt '0' ]]; then
		echo $fasta_line
		readID=$(echo $fasta_line | sed 's/>//')
		# echo $readID
		isSpike=$(grep -c $readID $P4_ALIGN)
		printf "isSpike=%s\n" $isSpike
		if [[ $isSpike -gt '0' ]]; then
			write2clean='Y'
			# (( breakx-- ))
			echo $breakx
		else
			write2clean='N'
		fi
	fi
	[[ $write2clean -eq 'Y' ]] && printf "%s\n" $fasta_line >> $FILT_READS_FA_FP_CLEAN
	[[ $write2clean -eq 'N' ]] && printf "%s\n" $fasta_line >> $FILT_READS_FA_FP_CONTR
	# [[ $breakx -lt '1' ]] && break
done < $FILT_READS_FA_FP_DIRTY