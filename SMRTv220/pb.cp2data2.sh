#!/bin/bash

# copy fast[a|q] to remove controls - temp solution for broken SMRT 2.2.0 cmd-line
## assume: in $CLEAN_DIR/data2
##> /global/projectb/sandbox/rqc/ayspunde/pacbio/SMRTv220/pb.cp2data2.sh

# ayspunde 2014-05-28

WORK_DIR=`pwd`

rsync -v ../data/corrected.fasta ./
rsync -v ../data/corrected.fastq ./
rsync -v ../data/filtered_subreads.fasta ./
rsync -v ../data/filtered_subreads.fastq ./

rsync -v ../cleanP4/p4c.alignment.211.txt ./

printf 'Finished rsync in WORK_DIR=%s\n\n' $WORK_DIR 
chgrp -R genome `pwd` &&  chmod -R 775 `pwd` &