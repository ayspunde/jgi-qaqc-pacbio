#!/bin/bash

########### TASK metadata #############
# Task            : adapterRpt
# Module          : P_Fetch
# TaskType        : None
# URL             : task://028069/P_Fetch/adapterRpt
# createdAt       : 2014-04-10 15:59:17.902874
# createdAt (UTC) : 2014-04-10 22:59:17.902906
# ncmds           : 2
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_Fetch/adapterRpt.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_Fetch/adapterRpt.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results/filter_reports_adapters.json
#
########### END TASK metadata #############

# Writing to log file
cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_Fetch/adapterRpt.sh >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_Fetch/adapterRpt.log;



echo "Running task://028069/P_Fetch/adapterRpt on $(uname -a)"

echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn.'
fi
echo 'Successfully validated input files'

# Task adapterRpt commands:


# Completed writing Task adapterRpt commands


# Task 1
pbreport.py adapter /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results filter_reports_adapters.json /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn || exit $?
echo "Task 1 completed at $(date)"
# Task 2
smrtreporter -basedir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results -headinclude /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results/.martin_header.html --html -o filter_reports_adapters.html -rules /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results/.rules_filter_reports_adapters.xml || exit $?
echo "Task 2 completed at $(date)"



echo "Finished on $(date -u)"
# Success
exit 0
