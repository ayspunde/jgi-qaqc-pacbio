#!/bin/bash

########### TASK metadata #############
# Task            : makeAfg.fofn.Scatter
# Module          : P_PreAssembler
# TaskType        : None
# URL             : task://028069/P_PreAssembler/makeAfg.fofn.Scatter
# createdAt       : 2014-03-19 16:22:07.362119
# createdAt (UTC) : 2014-03-19 23:22:07.362144
# ncmds           : 8
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_PreAssembler/makeAfg.fofn.Scatter.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_PreAssembler/makeAfg.fofn.Scatter.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.chunk005of007.fofn
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.chunk001of007.fofn
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.chunk004of007.fofn
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.chunk006of007.fofn
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.chunk007of007.fofn
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.chunk002of007.fofn
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.chunk003of007.fofn
#
########### END TASK metadata #############

# Writing to log file
cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_PreAssembler/makeAfg.fofn.Scatter.sh >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_PreAssembler/makeAfg.fofn.Scatter.log;



echo "Running task://028069/P_PreAssembler/makeAfg.fofn.Scatter on $(uname -a)"

echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn.'
fi
echo 'Successfully validated input files'

# Task makeAfg.fofn.Scatter commands:


# Completed writing Task makeAfg.fofn.Scatter commands


# Task 1
TOTAL_LINES=`cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn | wc -l` || exit $?
echo "Task 1 completed at $(date)"
# Task 2
awk "($TOTAL_LINES-NR+1)%7==0" /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn > /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.chunk001of007.fofn || exit $?
echo "Task 2 completed at $(date)"
# Task 3
awk "($TOTAL_LINES-NR+1)%7==1" /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn > /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.chunk002of007.fofn || exit $?
echo "Task 3 completed at $(date)"
# Task 4
awk "($TOTAL_LINES-NR+1)%7==2" /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn > /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.chunk003of007.fofn || exit $?
echo "Task 4 completed at $(date)"
# Task 5
awk "($TOTAL_LINES-NR+1)%7==3" /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn > /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.chunk004of007.fofn || exit $?
echo "Task 5 completed at $(date)"
# Task 6
awk "($TOTAL_LINES-NR+1)%7==4" /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn > /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.chunk005of007.fofn || exit $?
echo "Task 6 completed at $(date)"
# Task 7
awk "($TOTAL_LINES-NR+1)%7==5" /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn > /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.chunk006of007.fofn || exit $?
echo "Task 7 completed at $(date)"
# Task 8
awk "($TOTAL_LINES-NR+1)%7==6" /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn > /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.chunk007of007.fofn || exit $?
echo "Task 8 completed at $(date)"



echo "Finished on $(date -u)"
# Success
exit 0
