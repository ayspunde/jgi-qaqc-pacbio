#!/bin/bash

########### TASK metadata #############
# Task            : genPolishedReport
# Module          : P_AssemblyPolishing
# TaskType        : None
# URL             : task://028069/P_AssemblyPolishing/genPolishedReport
# createdAt       : 2014-04-10 15:59:17.787282
# createdAt (UTC) : 2014-04-10 22:59:17.787315
# ncmds           : 2
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_AssemblyPolishing/genPolishedReport.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_AssemblyPolishing/genPolishedReport.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/polished_assembly.fasta
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results/polished_report.xml
#
########### END TASK metadata #############

# Writing to log file
cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_AssemblyPolishing/genPolishedReport.sh >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_AssemblyPolishing/genPolishedReport.log;



echo "Running task://028069/P_AssemblyPolishing/genPolishedReport on $(uname -a)"

echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/polished_assembly.fasta ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/polished_assembly.fasta'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/polished_assembly.fasta.'
fi
echo 'Successfully validated input files'

# Task genPolishedReport commands:


# Completed writing Task genPolishedReport commands


# Task 1
nbases=$(fastalength /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/polished_assembly.fasta | awk '{t+=$1}END{print t}')
(
cat << EOF 
<?xml version="1.0" encoding="UTF-8"?><report><layout onecolumn="true"/><title>Polished Assembly</title><attributes><attribute id="1" name="Polished Contigs" value="$(grep -c '>' /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/polished_assembly.fasta)">$(grep -c '>' /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/polished_assembly.fasta)</attribute><attribute id="2" name="Max Contig Length" value="$(fastalength /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/polished_assembly.fasta | sort -n | tail -1 | awk '{print $1}')">$(fastalength /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/polished_assembly.fasta | sort -n | tail -1 | awk '{print $1}')</attribute><attribute id="3" name="N50 Contig Length" value="$(fastalength /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/polished_assembly.fasta | cut -d' ' -f1 | sort -nr | awk -v nbases=$nbases '{t+=$1;if (t >= nbases / 2){print $1; exit;}}')">$(fastalength /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/polished_assembly.fasta | cut -d' ' -f1 | sort -nr | awk -v nbases=$nbases '{t+=$1;if (t >= nbases / 2){print $1; exit;}}')</attribute><attribute id="4" name="Sum of Contig Lengths" value="$nbases">$nbases</attribute></attributes></report>
EOF
) > /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results/polished_report.xml || exit $?
echo "Task 1 completed at $(date)"
# Task 2
saxonb9 -xsl:/global/projectb/shared/software/smrtanalysis/2.1.1/current/analysis/etc/xsl/brandedGraphReport.xsl -s:/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results/polished_report.xml -o:/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results/polished_report.html || exit $?
echo "Task 2 completed at $(date)"



echo "Finished on $(date -u)"
# Success
exit 0
