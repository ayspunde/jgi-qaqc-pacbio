#!/bin/bash

# ayspunde 2014-03-27

# assertion: directory with quiver_test already finished
## bash /global/projectb/sandbox/rqc/ayspunde/pacbio/2014-03-19/quiver_test.count.sh

# QT='quiver_test.0427.csv'
q_cond='m0diN,m0diY,m20diN,m20diY'

printf "dir_path,$q_cond\n" 

i=0
for qd in  /global/projectb/scratch/ayspunde/qc/impQD/pacbio/*/*/*-cleanP4/data*-m*; do
	qd_dir=$(dirname $qd)
	if [  $i -eq 0 ]; then
		printf "%s," $qd_dir
	fi
	if [ $i -lt  4 ]; then
		nCorr=$(grep -c -v '#' $qd/corrections.gff)
		printf "%d," $nCorr 
	fi
	(( i++ ))
	if [ $i -eq  4 ]; then
		i=0 
		printf "\n"
	fi
done
printf "\n"


# qsub -S /bin/bash -cwd -j yes -w e -b yes -M `whoami`@lbl.gov -m bea \
	# -l h_rt=12:00 -l ram.c=5G -pe pe_slots 8 -N quiv -P prok-assembly.p \
	# "bash /global/projectb/sandbox/rqc/ayspunde/pacbio/2014-03-19/quiver.m0diN.sh"  
	
# qsub -S /bin/bash -cwd -j yes -w e -b yes -M `whoami`@lbl.gov -m bea \
	# -l h_rt=12:00 -l ram.c=5G -pe pe_slots 8 -N quiv -P prok-assembly.p \
	# "bash /global/projectb/sandbox/rqc/ayspunde/pacbio/2014-03-19/quiver.m0diY.sh"  

# qsub -S /bin/bash -cwd -j yes -w e -b yes -M `whoami`@lbl.gov -m bea \
	# -l h_rt=12:00 -l ram.c=5G -pe pe_slots 8 -N quiv -P prok-assembly.p \
	# "bash /global/projectb/sandbox/rqc/ayspunde/pacbio/2014-03-19/quiver.m20diN.sh"  
	
# qsub -S /bin/bash -cwd -j yes -w e -b yes -M `whoami`@lbl.gov -m bea \
	# -l h_rt=12:00 -l ram.c=5G -pe pe_slots 8 -N quiv -P prok-assembly.p \
	# "bash /global/projectb/sandbox/rqc/ayspunde/pacbio/2014-03-19/quiver.m20diY.sh"