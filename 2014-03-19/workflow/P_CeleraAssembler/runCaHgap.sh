#!/bin/bash

########### TASK metadata #############
# Task            : runCaHgap
# Module          : P_CeleraAssembler
# TaskType        : None
# URL             : task://028069/P_CeleraAssembler/runCaHgap
# createdAt       : 2014-03-19 16:22:07.577679
# createdAt (UTC) : 2014-03-19 23:22:07.577704
# ncmds           : 5
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_CeleraAssembler/runCaHgap.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_CeleraAssembler/runCaHgap.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.frg
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/runCA.spec
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/celera-assembler.singleton.fasta
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/celera-assembler.scf.fasta
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/celera-assembler.asm
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/celera-assembler.deg.fasta
#
########### END TASK metadata #############

# Writing to log file
cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_CeleraAssembler/runCaHgap.sh >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_CeleraAssembler/runCaHgap.log;



echo "Running task://028069/P_CeleraAssembler/runCaHgap on $(uname -a)"

echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.frg ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.frg'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.frg.'
fi
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/runCA.spec ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/runCA.spec'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/runCA.spec.'
fi
echo 'Successfully validated input files'

# Task runCaHgap commands:


# Completed writing Task runCaHgap commands


# Task 1
runCA -d /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data -p celera-assembler -s /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/runCA.spec /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.frg || exit $?
echo "Task 1 completed at $(date)"
# Task 2
fileWatcher.py -vv --time=600 --period=10 /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/celera-assembler.asm || exit $?
echo "Task 2 completed at $(date)"
# Task 3
ln -s /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/9-terminator/celera-assembler.scf.fasta /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/celera-assembler.scf.fasta || exit $?
echo "Task 3 completed at $(date)"
# Task 4
ln -s /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/9-terminator/celera-assembler.deg.fasta /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/celera-assembler.deg.fasta || exit $?
echo "Task 4 completed at $(date)"
# Task 5
ln -s /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/9-terminator/celera-assembler.singleton.fasta /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/celera-assembler.singleton.fasta || exit $?
echo "Task 5 completed at $(date)"



echo "Finished on $(date -u)"
# Success
exit 0
