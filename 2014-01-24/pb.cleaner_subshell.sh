#!/bin/bash -l

module load jamo biopython smrtanalysis

printf "Starting CLEANER (in cleaner_subshell)\nWORK_DIR=%s\n\n"  $(pwd)
ls -al $(pwd)

cleaner_full.py input.fofn p4c.alignment*txt

printf "Done with CLEANER (in cleaner_subshell):\nWORK_DIR=%s\n\n"  $(pwd)
ls -al $(pwd)
