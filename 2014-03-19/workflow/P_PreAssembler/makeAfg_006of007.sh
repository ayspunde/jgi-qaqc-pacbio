#!/bin/bash

########### TASK metadata #############
# Task            : makeAfg_006of007
# Module          : P_PreAssembler
# TaskType        : None
# URL             : task://028069/P_PreAssembler/makeAfg_006of007
# createdAt       : 2014-03-19 16:22:07.441914
# createdAt (UTC) : 2014-03-19 23:22:07.441939
# ncmds           : 1
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_PreAssembler/makeAfg_006of007.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_PreAssembler/makeAfg_006of007.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.chunk006of007.fofn
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_regions.chunk006of007.fofn
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/shortreads.chunk006of007.afg
#
########### END TASK metadata #############

# Writing to log file
cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_PreAssembler/makeAfg_006of007.sh >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_PreAssembler/makeAfg_006of007.log;



echo "Running task://028069/P_PreAssembler/makeAfg_006of007 on $(uname -a)"

echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.chunk006of007.fofn ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.chunk006of007.fofn'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.chunk006of007.fofn.'
fi
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_regions.chunk006of007.fofn ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_regions.chunk006of007.fofn'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_regions.chunk006of007.fofn.'
fi
echo 'Successfully validated input files'

# Task makeAfg_006of007 commands:


# Completed writing Task makeAfg_006of007 commands


# Task 1
toAfg /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.chunk006of007.fofn /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/shortreads.chunk006of007.afg  -regionTable /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_regions.chunk006of007.fofn || exit $?
echo "Task 1 completed at $(date)"



echo "Finished on $(date -u)"
# Success
exit 0
