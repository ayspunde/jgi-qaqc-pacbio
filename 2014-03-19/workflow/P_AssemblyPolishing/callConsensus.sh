#!/bin/bash

########### TASK metadata #############
# Task            : callConsensus
# Module          : P_AssemblyPolishing
# TaskType        : None
# URL             : task://028069/P_AssemblyPolishing/callConsensus
# createdAt       : 2014-04-10 15:59:17.629839
# createdAt (UTC) : 2014-04-10 22:59:17.629871
# ncmds           : 1
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_AssemblyPolishing/callConsensus.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_AssemblyPolishing/callConsensus.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrections.gff
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/polished_assembly.fasta
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/polished_assembly.fastq.gz
#
########### END TASK metadata #############

# Writing to log file
cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_AssemblyPolishing/callConsensus.sh >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_AssemblyPolishing/callConsensus.log;



echo "Running task://028069/P_AssemblyPolishing/callConsensus on $(uname -a)"

echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5 ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5.'
fi
echo 'Successfully validated input files'

# Task callConsensus commands:


# Completed writing Task callConsensus commands


# Task 1
variantCaller.py -P/global/projectb/shared/software/smrtanalysis/2.1.1/current/analysis/etc/algorithm_parameters/2013-09 -v -j7 --algorithm=quiver /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5 -r /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/>reference_cir/sequence/>reference_cir.fasta -o /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrections.gff -o /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/polished_assembly.fasta -o /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/polished_assembly.fastq.gz || exit $?
echo "Task 1 completed at $(date)"



echo "Finished on $(date -u)"
# Success
exit 0
