#!/bin/bash -l

# ayspunde 2014-03-19

# assertion: directory with already finished assembly
## bash /global/projectb/sandbox/rqc/ayspunde/pacbio/2014-03-19/pb.quiver_test.sh


CLEAN_DIR=`pwd`
DIRTY_DIR=$(echo $CLEAN_DIR | sed  -e "s/-cleanP4.*//")
PB_JOB_ID=$(echo $(basename $DIRTY_DIR) | sed 's/^0//')

printf "Starting 'pb.quiver_test.sh' in %s\n\n" $CLEAN_DIR
# exit

ls -al $CLEAN_DIR/data/aligned_reads.cmp.h5
ls -al $CLEAN_DIR/reference/sequence/reference.fasta
if [[ ! -e $CLEAN_DIR/data/aligned_reads.cmp.h5 ||  ! -e $CLEAN_DIR/reference/sequence/reference.fasta ]]; then
	printf "ERROR: Missing inpit file(s): exiting\n\n"
	exit
fi
# exit


printf "Starting QUIVER:\n"

qsub -S /bin/bash -cwd -j yes -w e -b yes -M `whoami`@lbl.gov -m bea \
	-l h_rt=12:00 -l ram.c=5G -pe pe_slots 8 -N quiv -P prok-assembly.p \
	"bash /global/projectb/sandbox/rqc/ayspunde/pacbio/2014-03-19/quiver.m0diN.sh"  
	
qsub -S /bin/bash -cwd -j yes -w e -b yes -M `whoami`@lbl.gov -m bea \
	-l h_rt=12:00 -l ram.c=5G -pe pe_slots 8 -N quiv -P prok-assembly.p \
	"bash /global/projectb/sandbox/rqc/ayspunde/pacbio/2014-03-19/quiver.m0diY.sh"  

qsub -S /bin/bash -cwd -j yes -w e -b yes -M `whoami`@lbl.gov -m bea \
	-l h_rt=12:00 -l ram.c=5G -pe pe_slots 8 -N quiv -P prok-assembly.p \
	"bash /global/projectb/sandbox/rqc/ayspunde/pacbio/2014-03-19/quiver.m20diN.sh"  
	
qsub -S /bin/bash -cwd -j yes -w e -b yes -M `whoami`@lbl.gov -m bea \
	-l h_rt=12:00 -l ram.c=5G -pe pe_slots 8 -N quiv -P prok-assembly.p \
	"bash /global/projectb/sandbox/rqc/ayspunde/pacbio/2014-03-19/quiver.m20diY.sh"