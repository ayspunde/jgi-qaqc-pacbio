#!/bin/bash -l

# kick-in hacksaw
# assuming:
# de-duped, circularized: pb.prelim_qc.sh, pb.circle.sh (will check if latter completed)

# - start in background, to avoid waiting on circularizition forever
## bash /global/projectb/sandbox/rqc/ayspunde/pacbio/SMRTv220/pb.hacksaw_qc_only.sh &

# ayspunde 2014-05-08: hgap3 version


#-- decide which version: 1.2.0 or 1.3.0, and how to deal with re-polishing
# hack_ver=120
hack_ver=130		# full hacksaw analysis
# hack_ver=131		# option to re-run only final stats
# hack_ver=132		# one-off for running out of time after 72 h of megan

RUN_HACKSAW_120='/global/projectb/sandbox/rqc/prod/pipelines/hacksaw/1.2.0/bin/run_hacksaw.py'
# HACKSAW_CONFIG_120='/global/projectb/sandbox/rqc/ayspunde/pacbio/config/hacksaw/hacksaw.120.qc-only.stats-final.config'
HACKSAW_CONFIG_120='/global/projectb/sandbox/rqc/ayspunde/pacbio/config/hacksaw/hacksaw.120.qc-only.post-deg-dedup-circl.config'

RUN_HACKSAW_130='/global/projectb/sandbox/rqc/prod/pipelines/hacksaw/1.3.0/bin/run_hacksaw.py'
HACKSAW_CONFIG_130='/global/projectb/sandbox/rqc/ayspunde/pacbio/SMRTv220/hacksaw.130.smrt220.post-circ.config'
HACKSAW_CONFIG_131='/global/projectb/sandbox/rqc/ayspunde/pacbio/SMRTv220/hacksaw.131.smrt220.redo-final.config'
HACKSAW_CONFIG_132='/global/projectb/sandbox/rqc/ayspunde/pacbio/SMRTv220/hacksaw.132.smrt220.redo-qc-post-megan.config'


if [ $hack_ver -eq '120' ]; then
	RUN_HACKSAW=$RUN_HACKSAW_120
	HACKSAW_CONFIG=$HACKSAW_CONFIG_120
elif [ $hack_ver -eq '130' ]; then
	RUN_HACKSAW=$RUN_HACKSAW_130
	HACKSAW_CONFIG=$HACKSAW_CONFIG_130
elif [ $hack_ver -eq '131' ]; then
	RUN_HACKSAW=$RUN_HACKSAW_130
	HACKSAW_CONFIG=$HACKSAW_CONFIG_131
elif [ $hack_ver -eq '132' ]; then
	RUN_HACKSAW=$RUN_HACKSAW_130
	HACKSAW_CONFIG=$HACKSAW_CONFIG_132
fi
# exit

CLEAN_DIR=`pwd`
DIRTY_DIR=$(echo $CLEAN_DIR | sed  -e "s/-cleanP4.*//")
PB_JOB_ID=$(echo $(basename $DIRTY_DIR) | sed 's/^0//')
seqN=$(basename $(realpath ./..))

printf "Starting post-circularization hacksaw:\nTHIS_DIR=%s\n\n" $CLEAN_DIR

INPUT_XML=$CLEAN_DIR/input.xml
SETTINGS_XML=$CLEAN_DIR/settings.xml
ls -al $INPUT_XML $RUN_HACKSAW $HACKSAW_CONFIG $SETTINGS_XML   # for my own visual confirmation that paths were set
echo $INPUT_XML | grep `pwd`                               	# for my own visual confirmation that path was updated


REQUIVER_OUT=$CLEAN_DIR/data2
CIRCLE_FASTA=$CLEAN_DIR/circularize/finalContigs.fasta
REQUIVER_FASTA=$REQUIVER_OUT/polished_assembly.fasta
REQUIVER_FASTA_GZ=$REQUIVER_OUT/polished_assembly.fasta.gz
if [[ ! -d $REQUIVER_OUT ]]; then  # workflow without re-polishing (no deg files): no /data2 
	mkdir $REQUIVER_OUT
fi
if [[ -e $REQUIVER_FASTA ]]; then 	# prevent conflict with ST's handling of *fasta.gz
	mv $REQUIVER_FASTA $REQUIVER_FASTA.copy
fi
if [[ -e $CIRCLE_FASTA  &&  ! -e $REQUIVER_FASTA_GZ ]]; then  
	gzip -c $CIRCLE_FASTA > $REQUIVER_FASTA_GZ
fi
if [[ ! -e $REQUIVER_FASTA_GZ || ! -e $INPUT_XML || ! -e $SETTINGS_XML ]]; then
	printf "Can't start hacksaw in WORK_DIR=%s\nmissing one of these:\n" $CLEAN_DIR
	printf "ASM_FASTA_GZ=%s\n"  $REQUIVER_FASTA_GZ
	printf "INPUT_XML=%s\n"     $INPUT_XML
	printf "SETTINGS_XML=%s\n"  $SETTINGS_XML
	date
	printf "\n\n"
	exit 1
fi


# wait_limit=24 # 24 (15 min cycles) = 6 hours is a max wait for circularization to complete
# while [ ! -e $CIRCLE_FASTA ] ; do
	# printf "Waiting to start post-circularization hacksaw:\nno $CIRCLE_FASTA yet\nTHIS_DIR=%s\n\n" $CLEAN_DIR
	# date
	# (( wait_limit-- ))
	# if [ $wait_limit -le '0' ]; then # most of the time, nothing to run circularization on ...
		# printf "Can't start post-circularization hacksaw:\nrun out of wait time\nTHIS_DIR=%s\n\n" $CLEAN_DIR
		# date
		# exit 1
	# fi
	# sleep 900
# done
# gzip -c $CLEAN_DIR/circularize/finalContigs.fasta > $CLEAN_DIR/circularize/finalContigs.fasta.gz



printf "\n\ncircularizitation completed: qsub hacksaw, CLEAN_DIR:\n ''%s''\n\n" $CLEAN_DIR
date
if [ $hack_ver -eq '120' ]; then
	"qsub command: $RUN_HACKSAW_120 -config $HACKSAW_CONFIG_120 -module -smrtportal -settingsXml $SETTINGS_XML $INPUT_XML"
	qsub -S /bin/bash -cwd -j yes -w e -b yes \
	-l h_rt=72:00:00 -l ram.c=7.5G -pe pe_slots 16 -M `whoami`@lbl.gov -m bea -N hack$seqN -P prok-assembly.p \
	"$RUN_HACKSAW_120 -config $HACKSAW_CONFIG_120 -module -smrtportal -settingsXml $SETTINGS_XML $INPUT_XML"
# elif [ $hack_ver -eq '130' ]; then
	# "qsub command: $RUN_HACKSAW_130 -config $HACKSAW_CONFIG_130 $INPUT_XML $SETTINGS_XML"
	# qsub -S /bin/bash -cwd -j yes -w e -b yes \
	# -l h_rt=72:00:00 -l ram.c=6G -pe pe_slots 16 -M `whoami`@lbl.gov -m bea -N hack$seqN -P prok-assembly.p \
	# "$RUN_HACKSAW_130 -config $HACKSAW_CONFIG_130 $INPUT_XML $SETTINGS_XML"
elif [ $hack_ver -eq '130' ]; then
	echo "qsub command: $RUN_HACKSAW_130 -config $HACKSAW_CONFIG_130 $INPUT_XML $SETTINGS_XML"
	qsub -S /bin/bash -cwd -j yes -w e -b yes \
	-l h_rt=120:00:00 -l ram.c=6G -pe pe_slots 16 -M `whoami`@lbl.gov -m bea -N hack$seqN -P prok-assembly.p \
	"$RUN_HACKSAW_130 -config $HACKSAW_CONFIG_130 $INPUT_XML $SETTINGS_XML"
elif [ $hack_ver -eq '131' ]; then
	echo "qsub command: $RUN_HACKSAW_130 -config $HACKSAW_CONFIG_131 $INPUT_XML $SETTINGS_XML"
	qsub -S /bin/bash -cwd -j yes -w e -b yes \
	-l h_rt=12:00:00 -l ram.c=5G -pe pe_slots 8 -M `whoami`@lbl.gov -m bea -N hack$seqN -P prok-assembly.p \
	"$RUN_HACKSAW_130 -config $HACKSAW_CONFIG_131 $INPUT_XML $SETTINGS_XML"
elif [ $hack_ver -eq '132' ]; then
	echo "qsub command: $RUN_HACKSAW_130 -config $HACKSAW_CONFIG_132 $INPUT_XML $SETTINGS_XML"
	qsub -S /bin/bash -cwd -j yes -w e -b yes \
	-l h_rt=12:00:00 -l ram.c=12G -pe pe_slots 8 -M `whoami`@lbl.gov -m bea -N hack$seqN -P prok-assembly.p \
	"$RUN_HACKSAW_130 -config $HACKSAW_CONFIG_132 $INPUT_XML $SETTINGS_XML"
fi





