#!/bin/bash

########### TASK metadata #############
# Task            : preAssemblerJsonReport
# Module          : P_PreAssembler
# TaskType        : None
# URL             : task://028069/P_PreAssembler/preAssemblerJsonReport
# createdAt       : 2014-03-19 16:22:07.382089
# createdAt (UTC) : 2014-03-19 23:22:07.382115
# ncmds           : 2
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_PreAssembler/preAssemblerJsonReport.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_PreAssembler/preAssemblerJsonReport.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_subreads.fasta
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/filtered_longreads.fasta
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.fasta
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results/preassembler_report.json
#
########### END TASK metadata #############

# Writing to log file
cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_PreAssembler/preAssemblerJsonReport.sh >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_PreAssembler/preAssemblerJsonReport.log;



echo "Running task://028069/P_PreAssembler/preAssemblerJsonReport on $(uname -a)"

echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_subreads.fasta ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_subreads.fasta'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_subreads.fasta.'
fi
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/filtered_longreads.fasta ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/filtered_longreads.fasta'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/filtered_longreads.fasta.'
fi
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.fasta ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.fasta'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.fasta.'
fi
echo 'Successfully validated input files'

# Task preAssemblerJsonReport commands:


# Completed writing Task preAssemblerJsonReport commands


# Task 1
preassembly_report.py --debug --length-cutoff $(cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/filtered_longreads.fasta.cutoff) /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/filtered_subreads.fasta /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/filtered_longreads.fasta /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrected.fasta /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results/preassembler_report.json || exit $?
echo "Task 1 completed at $(date)"
# Task 2
smrtreporter -basedir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results -headinclude /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results/.martin_header.html --html -o preassembler_report.html -rules /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/results/.rules_preassembler_report.xml || exit $?
echo "Task 2 completed at $(date)"



echo "Finished on $(date -u)"
# Success
exit 0
