#!/bin/bash

# ayspunde 2014-01-06
# assuming in the project (NNN-cleanP4)
# bash /global/u1/a/ayspunde/work/projects/pacbio/2014-01-24/pb.myqc.sh

mkdir -m 775 myQC
cd myQC
ln -s ../final/final.assembly.fasta ./

LLLL_16SREFSEQ='/global/projectb/sandbox/rqc/qcdb/collab16s/collab16s.fa'
SILVA_16SEQ='/global/dna/shared/rqc/ref_databases/qaqc/databases/silva/2011.04.silva/SSURef_106_tax_silva.fasta'
run_megablast.sh final.assembly.fasta $LLLL_16SREFSEQ -p 90 &
run_megablast.sh final.assembly.fasta $SILVA_16SEQ -p 90 & 

module load mummer
seqN=$( basename $(realpath ./../..))
nucmer final.assembly.fasta final.assembly.fasta
mummerplot -l --color -t png out.delta
# S1 ,E1 ,S2 ,E2 ,LEN1 ,LEN2 ,'%_IDY' ,LEN_R ,LEN_Q ,COV_R ,COV_Q ,TAG1,TAG2
show-coords -r -c -l out.delta > nucmer.coord.$seqN.csv
printf 'S1 ,E1 ,S2 ,E2 ,LEN1 ,LEN2 ,PCT_IDY ,LEN_R ,LEN_Q ,COV_R ,COV_Q ,TAG1 ,TAG2' >> nucmer.coord.$seqN.csv
mv out.png out.$seqN.png

cd ..