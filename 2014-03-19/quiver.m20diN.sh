#!/bin/bash -l


module load smrtanalysis

minmapq=20
cond_dir=$(pwd)/"data-m${minmapq}diN"

if [ ! -d $cond_dir ]; then 
	mkdir $cond_dir
fi

variantCaller.py -P /global/projectb/shared/software/smrtanalysis/2.1.1/current/analysis/etc/algorithm_parameters/2013-09 -v -j 7 --algorithm=quiver \
 $(pwd)/data/aligned_reads.cmp.h5 \
 -m ${minmapq} \
 -r $(pwd)/reference/sequence/reference.fasta \
 -o $cond_dir/corrections.gff \
 -o $cond_dir/polished_assembly.fasta \
 -o $cond_dir/polished_assembly.fastq.gz \
 -d 'variants' --evidenceDirectory $cond_dir 
 