#!/bin/bash

# ayspunde 2014-01-06

# assuming rsync-ed, cleaned P4, re-run SMRT Pipe: pb.rsync.sh ; pb.cleanP4.sh 
# currently before kickin hacksaw need manually run circularization

# called by DRIVER.sh, after cd to $CLEAN_DIR


# PACBIO_LOCAL is your working dir
PACBIO_PORTAL='/global/projectb/shared/pacbio/jobs'
PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc'

RUN_HACKSAW='/projectb/sandbox/rqc/prod/pipelines/hacksaw/DEFAULT/bin/run_hacksaw.py'
HACKSAW_CONFIG='/global/u1/a/ayspunde/work/qcqd/config/hacksaw/hacksaw.qc-only.post-circ-quiv-degen.config'


# assuming already:     cd $CLEAN_DIR
 
CLEAN_DIR=`pwd`	
DIR2WORK='dedup'	
mkdir -m 775 $DIR2WORK
cd $DIR2WORK; pwd

POL_ASM_ORIG=$CLEAN_DIR/data/polished_assembly.fasta
POL_ASM_COPY=`pwd`/polished_assembly.fasta
cp $POL_ASM_ORIG $POL_ASM_COPY
DEG_ORIG=$CLEAN_DIR/data/9-terminator/celera-assembler.deg.fasta
DEG_COPY=`pwd`/polished_assembly.fasta
cp $DEG_ORIG $DEG_COPY

ASM_MERGED=`pwd`/polished_assembly_deg.fasta
cat $POL_ASM_COPY $DEG_COPY > $ASM_MERGED
# ddd - degenerate de duped - for manual editing after 
ASM_DEDUP=`pwd`/polished_assembly.ddd.fasta

module load mummer
seqN=$( basename $(realpath ./../..))
nucmer polished_assembly_deg.fasta polished_assembly_deg.fasta
mummerplot -l --color -t png out.delta
# S1 ,E1 ,S2 ,E2 ,LEN1 ,LEN2 ,'%_IDY' ,LEN_R ,LEN_Q ,COV_R ,COV_Q ,TAG1,TAG2
show-coords -r -c -l out.delta > nucmer.coord.$seqN.csv
printf 'S1 ,E1 ,S2 ,E2 ,LEN1 ,LEN2 ,PCT_IDY ,LEN_R ,LEN_Q ,COV_R ,COV_Q ,TAG1 ,TAG2' >> nucmer.coord.$seqN.csv
mv out.png out.$seqN.png

# manuall exam results, make changes to $ASM_DEDUP
# ? try BB's dedup

gzip -c $ASM_DEDUP > $ASM_DEDUP.gz




