#!/bin/bash

########### TASK metadata #############
# Task            : consensus.layoutIds.Scatter
# Module          : P_PreAssembler
# TaskType        : None
# URL             : task://028069/P_PreAssembler/consensus.layoutIds.Scatter
# createdAt       : 2014-03-19 16:22:07.616536
# createdAt (UTC) : 2014-03-19 23:22:07.616561
# ncmds           : 8
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_PreAssembler/consensus.layoutIds.Scatter.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_PreAssembler/consensus.layoutIds.Scatter.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.ids
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.chunk006of007.ids
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.chunk007of007.ids
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.chunk005of007.ids
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.chunk002of007.ids
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.chunk003of007.ids
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.chunk001of007.ids
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.chunk004of007.ids
#
########### END TASK metadata #############

# Writing to log file
cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_PreAssembler/consensus.layoutIds.Scatter.sh >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_PreAssembler/consensus.layoutIds.Scatter.log;



echo "Running task://028069/P_PreAssembler/consensus.layoutIds.Scatter on $(uname -a)"

echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.ids ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.ids'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.ids.'
fi
echo 'Successfully validated input files'

# Task consensus.layoutIds.Scatter commands:


# Completed writing Task consensus.layoutIds.Scatter commands


# Task 1
TOTAL_LINES=`cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.ids | wc -l` || exit $?
echo "Task 1 completed at $(date)"
# Task 2
awk "($TOTAL_LINES-NR+1)%7==0" /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.ids > /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.chunk001of007.ids || exit $?
echo "Task 2 completed at $(date)"
# Task 3
awk "($TOTAL_LINES-NR+1)%7==1" /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.ids > /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.chunk002of007.ids || exit $?
echo "Task 3 completed at $(date)"
# Task 4
awk "($TOTAL_LINES-NR+1)%7==2" /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.ids > /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.chunk003of007.ids || exit $?
echo "Task 4 completed at $(date)"
# Task 5
awk "($TOTAL_LINES-NR+1)%7==3" /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.ids > /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.chunk004of007.ids || exit $?
echo "Task 5 completed at $(date)"
# Task 6
awk "($TOTAL_LINES-NR+1)%7==4" /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.ids > /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.chunk005of007.ids || exit $?
echo "Task 6 completed at $(date)"
# Task 7
awk "($TOTAL_LINES-NR+1)%7==5" /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.ids > /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.chunk006of007.ids || exit $?
echo "Task 7 completed at $(date)"
# Task 8
awk "($TOTAL_LINES-NR+1)%7==6" /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.ids > /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.chunk007of007.ids || exit $?
echo "Task 8 completed at $(date)"



echo "Finished on $(date -u)"
# Success
exit 0
