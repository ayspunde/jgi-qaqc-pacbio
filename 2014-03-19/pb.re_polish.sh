#!/bin/bash -l

# ayspunde 2014-04-14
# re-run polishing after adding deg.fasta to polished_assembly.fa

# assumptions: first round of Hgap completed, assembly and deg fasta's merged in /prelimQC/polished_assembly.deg.dedup.fasta
# called from clean-P4 (s123/012345-cleanP4) directory: manually or by DRIVER 

## bash /global/projectb/sandbox/rqc/ayspunde/pacbio/DEFAULT/pb.re_polish.sh


CURRENT_VER='/global/projectb/sandbox/rqc/ayspunde/pacbio/DEFAULT'
SMRT_VERSION='2.1.1'
CLEAN_DIR=$(realpath `pwd`)
DIRTY_DIR=$(echo $CLEAN_DIR | sed  -e "s/-cleanP4.*//")
jobID=$(basename $DIRTY_DIR)
seqID=$(basename $(realpath $CLEAN_DIR/..))

printf "Qsub-ing repolish (re-quiver):\nWORK_DIR=%s\njob name = %s\n\n" $(pwd) "quiv${jobID}"
qsub -S /bin/bash -cwd -j yes -w e -b yes -M `whoami`@lbl.gov -m bea \
	-l h_rt=12:00:00 -l ram.c=7.5G -pe pe_slots 16 -N quiv${jobID} -P prok-assembly.p \
	"$CURRENT_VER/workflow/P_RePolishing.sh"  



