#!/bin/bash

# # /global/projectb/sandbox/rqc/ayspunde/pacbio/hgap.validation/frame_shifts/get_corr_overload.sh

cd /global/projectb/scratch/ayspunde/qc/impQD/pacbio

# for CLEAN_DIR in ./seqqc[12]/s*/*-cleanP4; do
CLEAN_DIR=/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc1/s347/026972-cleanP4

	printf 'CLEAN_DIR=%s\n' $CLEAN_DIR
	gff=$CLEAN_DIR/data/corrections.gff
	if [ ! -e $gff ]; then
		printf 'CLEAN_DIR=%s\t SKIP: no corrections.gff\n' $CLEAN_DIR
		continue
	fi
	nCorr=$(grep -v '##' $gff | wc -l)
	echo $nCorr
	load_xml=$CLEAN_DIR/results/filterReports_loading.xml
	# assuming it's always there
	ln2r=0
	while read xml_lane; do
		echo $xml_lane
		echo $ln2r
		if (( $(expr "$xml_lane" : '*rowname*') > 0 )) ; then
			ln2r=7
			echo $ln2r
			continue
		fi	
		if (( $ln2r > 0 )); then 
			(( ln2r -= 1 ))
			echo $ln2r
		else
			continue
			echo $ln2r
		fi
		case $ln2r in 
		'6')
			smrt_cell=$(echo $xml_lane | sed -e 's/<td>//' -e 's/<\/td>//')
			;;
		'5')
			n_zmw=$(echo $xml_lane | sed -e 's/<td>//' -e 's/<\/td>//')
			;;
		'4')
			prod0=$(echo $xml_lane | sed -e 's/<td>//' -e 's/<\/td>//')
			;;
		'3')
			prod1=$(echo $xml_lane | sed -e 's/<td>//' -e 's/<\/td>//')
			;;
		'2')
			prod2=$(echo $xml_lane | sed -e 's/<td>//' -e 's/<\/td>//')
			;;
		'1')
			printf 'CLEAN_DIR=%s\tn_corr=%d\t smrt_cell=%s\t n_zmw=%d\t prod1=%2.1f\t prod2=%2.1f\t prod3=%2.1f \n' \
			  $CLEAN_DIR $nCorr $smrt_cell $n_zmw $prod0 $prod1 $prod2 $prod3
			;;
		esac	
	done < $load_xml
# done
