#!/bin/bash

# ayspunde 2014-01-06

# rsync successful SMRT portal jobs to scratch:
## /global/projectb/sandbox/rqc/ayspunde/pacbio/2014-03-19/pb.rsync.sh

# to clean P4 spike-in call pb.cleanp4.sh
# to kick-in hacksaw call pb.hacksaw.sh

## change these:
# jName - my own ID(s) for the organism (derived from JIRA ticket N); free-form
# jID - jobID from SMRT Portal, exact


# 2014-04-02: re-running chemistry mis-matched data
jName=(s331  s863    s420  s732 s367  s754  s939  s733  s649  s700  s933  s648 s103)
jID=( 27031  27334  26979 27314  27023  27753  27438  27315  26852  27326  27338 26883  27182)
jName=(s372)
jID=( 28442)



# PACBIO_LOCAL is your working dir
PACBIO_PORTAL='/global/projectb/shared/pacbio/jobs'
PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10'
# PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc20'
# PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc41'

for ((i=0; i < ${#jName[@]} ; i++)); do
    LOC_NAME=${jName[$i]}
    PROJECT_DIR=$PACBIO_LOCAL/$LOC_NAME
    PB_JOB_ID=${jID[$i]}
    echo $i $PROJECT_DIR $PB_JOB_ID
    if [ -d $PROJECT_DIR ]; then printf "$PROJECT_DIR already exits: ready for rsync\n"
    else mkdir -m 775 -p $PROJECT_DIR
    fi
    
    cd $PROJECT_DIR
    pwd
    L1NN=$(echo $PB_JOB_ID | sed 's/\([0-9]\{2\}\).*/\1/')
    PORTAL_JOB=$PACBIO_PORTAL/0$L1NN/0${PB_JOB_ID}
    if [ -d $PORTAL_JOB ]; then
        printf "$PORTAL_JOB\n\n"
        printf "$PORTAL_JOB\n\n" > $(basename $PORTAL_JOB).rsync.log
        rsync -vr $PORTAL_JOB ./ >> $(basename $PORTAL_JOB).rsync.log &
    fi
    cd ..
done
