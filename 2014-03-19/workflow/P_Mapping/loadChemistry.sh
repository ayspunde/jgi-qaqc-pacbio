#!/bin/bash

########### TASK metadata #############
# Task            : loadChemistry
# Module          : P_Mapping
# TaskType        : None
# URL             : task://028069/P_Mapping/loadChemistry
# createdAt       : 2014-04-10 15:59:17.312598
# createdAt (UTC) : 2014-04-10 22:59:17.312639
# ncmds           : 1
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_Mapping/loadChemistry.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_Mapping/loadChemistry.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/chemistry_mapping.xml
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5
#
########### END TASK metadata #############

# Writing to log file
cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_Mapping/loadChemistry.sh >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_Mapping/loadChemistry.log;



echo "Running task://028069/P_Mapping/loadChemistry on $(uname -a)"

echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/chemistry_mapping.xml ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/chemistry_mapping.xml'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/chemistry_mapping.xml.'
fi
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5 ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5.'
fi
echo 'Successfully validated input files'

# Task loadChemistry commands:


# Completed writing Task loadChemistry commands


# Task 1
loadSequencingChemistryIntoCmpH5.py --xml /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/chemistry_mapping.xml --h5 /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5 || exit $?
echo "Task 1 completed at $(date)"



echo "Finished on $(date -u)"
# Success
exit 0
