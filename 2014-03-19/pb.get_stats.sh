#!/bin/bash

# get some stats over the data set
# # /global/projectb/sandbox/rqc/ayspunde/pacbio/2014-01-24/pb.get_stats.sh

# ayspunde 2014-03-07

# what and where
THIS_SCRIPT=`basename $0`
printf "Starting %s in %s\n\n" $THIS_SCRIPT `pwd`


things2do=(corrections proj_desc qd_report celera_qc)
things2do=(corrections proj_desc celera_qc)
# # assuming: /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc2
# for CLEAN_DIR in ./s*/*-cleanP4; do
# assuming: /global/projectb/scratch/ayspunde/qc/impQD/pacbio
for CLEAN_DIR in ./seqqc[12]/s*/*-cleanP4; do
	# if [ ! -d $CLEAN_DIR/'final' ] ; then
		# printf "%s\tSKIP - not a release dir\n" $CLEAN_DIR
		# continue
	# fi
	for ((j=0; j < ${#things2do[@]} ; j++)); do
		todo=${things2do[$j]}
		case $todo in
			'corrections')
				fileFP=$CLEAN_DIR/'data/corrections.gff'
				n_corr=$(grep -v '##' $fileFP | wc -l)
				;;
			'proj_desc')
				fileFP=$CLEAN_DIR/'project_desc_file.txt'
				if [ -e $fileFP ]; then
					seq_id_str='project.JGI Sequencing Project ID='
					seq_id=$(grep "$seq_id_str" $fileFP | sed "s/$seq_id_str//")
				else
					seq_id='NotReleasedYet'
				fi
				;;
			'qd_report')
				fileFP=$CLEAN_DIR/'final/QD.finalReport.txt'
				gc_str='Avg GC Content: '
				gc=$(grep "$gc_str" $fileFP | sed "s/$gc_str//")
				n_contig_str='Main genome contig total: '
				n_contig=$(grep "$n_contig_str" $fileFP | sed "s/$n_contig_str//")
				gen_sz_str='Main genome scaffold sequence total: '
				gen_sz=$(grep "$gen_sz_str" $fileFP | sed "s/$gen_sz_str//")
				;;
			'celera_qc')
				fileFP=$CLEAN_DIR/'data/celera-assembler.qc'
				gc_str='Content='
				gc=$(grep "$gc_str" $fileFP | sed "s/$gc_str//")
				n_contig_str='TotalScaffolds='
				n_contig=$(grep -m 1 "$n_contig_str" $fileFP | sed "s/$n_contig_str//")
				gen_sz_str='TotalBasesInScaffolds='
				gen_sz=$(grep -m 1 "$gen_sz_str" $fileFP | sed "s/$gen_sz_str//")
				;;
		esac
	done
	printf '%s\tn_corr=%d\tseq_id=%s\tgc="%s"\tn_contig=%s\tsize=%s\n' $CLEAN_DIR $n_corr $seq_id $gc $n_contig $gen_sz
done



# for gff in ./s*/*-cleanP4/data/corrections.gff; do
# >     printf '%s\t%d\n' $gff $(grep -v '##' $gff | wc -l)
# > done