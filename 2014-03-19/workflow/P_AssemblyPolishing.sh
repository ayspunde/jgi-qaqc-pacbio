#!/bin/bash -l

SCRIPT_LOC='/global/projectb/sandbox/rqc/ayspunde/pacbio/DEFAULT'
## 	$SCRIPT_LOC/workflow/P_AssemblyPolishing.sh 2>&1 | tee -a `pwd`/log/P_AssemblyPolishing.log

########### TASK order #############
callConsensus=1
enrichAlnSummary=1
genPolishedReport=1
topCorrections=1
variantsRpt=1
zipPolishedFasta=1
########### END TASK order #############


module load smrtanalysis/2.1.1

CLEAN_DIR=$(realpath `pwd`)
DIRTY_DIR=$(echo $CLEAN_DIR | sed  -e "s/-cleanP4.*//")
jobID=$(basename $DIRTY_DIR)
seqID=$(basename $(realpath $CLEAN_DIR/..))

oldDATA=$CLEAN_DIR/data
# newDATA=$CLEAN_DIR/data2
newDATA=$CLEAN_DIR/data3
locTEMP=$CLEAN_DIR/temp2
newREF=$CLEAN_DIR/reference


########### TASK metadata #############
# Task            : callConsensus
# Module          : P_AssemblyPolishing
# TaskType        : None
# URL             : task://028069/P_AssemblyPolishing/callConsensus
# createdAt       : 2014-04-10 15:59:17.629839
# createdAt (UTC) : 2014-04-10 22:59:17.629871
# ncmds           : 1
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_AssemblyPolishing/callConsensus.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_AssemblyPolishing/callConsensus.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/aligned_reads.cmp.h5
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrections.gff
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/polished_assembly.fasta
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/polished_assembly.fastq.gz
#
########### END TASK metadata #############

echo "Running task://028069/P_AssemblyPolishing/callConsensus on $(uname -a)"
echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e "$newDATA/aligned_reads.cmp.h5" ]; then
	echo "Successfully found $newDATA/aligned_reads.cmp.h5"
else
	echo "WARNING: Unable to find necessary input file, or dir $newDATA/aligned_reads.cmp.h5"
fi
echo 'Successfully validated input files'

# Task callConsensus commands:
# Completed writing Task callConsensus commands
# Task 1
variantCaller.py \
	-P /global/projectb/shared/software/smrtanalysis/2.1.1/current/analysis/etc/algorithm_parameters/2013-09 -v -j7 --algorithm=quiver \
	$newDATA/aligned_reads.cmp.h5 \
	-r $newREF/sequence/reference.fasta \
	-o $newDATA/corrections.gff \
	-o $newDATA/polished_assembly.fasta \
	-o $newDATA/polished_assembly.fastq.gz \
	|| exit $?
echo "Task 1 completed at $(date)"


########### TASK metadata #############
# Task            : enrichAlnSummary
# Module          : P_AssemblyPolishing
# TaskType        : None
# URL             : task://028069/P_AssemblyPolishing/enrichAlnSummary
# createdAt       : 2014-04-10 15:59:17.908165
# createdAt (UTC) : 2014-04-10 22:59:17.908197
# ncmds           : 2
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_AssemblyPolishing/enrichAlnSummary.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_AssemblyPolishing/enrichAlnSummary.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/alignment_summary.gff
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrections.gff
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/alignment_summary.gff
#
########### END TASK metadata #############

echo "Running task://028069/P_AssemblyPolishing/enrichAlnSummary on $(uname -a)"
echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e "$newDATA/alignment_summary.gff" ]; then
	echo "Successfully found $newDATA/alignment_summary.gff"
else
	echo "WARNING: Unable to find necessary input file, or $newDATA/alignment_summary.gff"
fi
if [ -e "$newDATA/corrections.gff" ]; then
	echo "Successfully found $newDATA/corrections.gff"
else
	echo "WARNING: Unable to find necessary input file, or dir $newDATA/corrections.gff"
fi
echo 'Successfully validated input files'

# Task enrichAlnSummary commands:
# Completed writing Task enrichAlnSummary commands
# Task 1
summarizeConsensus.py \
	--variantsGff $newDATA/corrections.gff \
	$newDATA/alignment_summary.gff \
	--output $locTEMP/$jobID.gff \
	|| exit $?
echo "Task 1 completed at $(date)"
# Task 2
mv \
	/$locTEMP/$jobID.gff \
	$newDATA/alignment_summary.gff \
	|| exit $?
echo "Task 2 completed at $(date)"
