#!/bin/bash

# ayspunde 2014-01-27

# This is just a template for pre-release activities, no plans to script this

# - assuming sitting in ./final
#>  bash /global/u1/a/ayspunde/work/.myScripts/PacBio/pb.hacklinks.sh
# already run:
# raw.h5.reads.tar.gz
# filtered.subreads.fastq.gz
# error.corrected.reads.fastq.gz
# QC.finalReport.pdf - same as for other product types
# QD.finalReport.txt  - same as for other product types
# final.assembly.fasta - not zipped, since we haven't before
# final.assembly.topology.txt


# # cd ..
# # /global/projectb/sandbox/rqc/prod/pipelines/hacksaw/1.2.0/bin/hacksaw_pdf.pl -pp > project_desc_file.txt
# # /global/projectb/sandbox/rqc/prod/pipelines/hacksaw/1.2.0/bin/hacksaw_pdf.pl project_desc_file.txt 2>&1 >/dev/null
# # /global/projectb/sandbox/rqc/prod/pipelines/hacksaw/DEFAULT/bin/hacksaw_pdf.pl -pp > project_desc_file.txt
# /global/projectb/sandbox/rqc/prod/pipelines/hacksaw/DEFAULT/bin/hacksaw_pdf.pl project_desc_file.txt 2>&1 >/dev/null
# #
rm metadata.json
mkdir .fasta && mv *fasta ./.fasta 
mkdir .old && mv *old ./.old
mkdir .log && mv *.o* ./log ; mv *.po* ./log ; mv *log ./log

/global/projectb/sandbox/rqc/prod/pipelines/hacksaw/DEFAULT/bin/hacksaw_pdf.pl -pp > project_desc_file.txt
chgrp -R genome `pwd` &&  chmod -R 775 `pwd` &

sed -i 's/assemblyVersion.row.hgap=/assemblyVersion.row.hgap=2.1.1/' hacksaw_stats.txt
/global/projectb/sandbox/rqc/prod/pipelines/hacksaw/DEFAULT/bin/hacksaw_pdf.pl project_desc_file.txt 2>&1 >/dev/null

proj_id_token='project.JGI Sequencing Project ID='
PROJ_ID=$(cat project_desc_file.txt | grep "$proj_id_token" | sed "s/$proj_id_token//")
submitanalysis.py $PROJ_ID | tee -a submit.py.log
chgrp -R genome `pwd` &&  chmod -R 775 `pwd` &

## https://docs.google.com/a/lbl.gov/document/d/1gyZqFt5_HZz-naktMN-_DlMeHR9dlJ7umM_zk7OQUIo/edit#
## 4)To ReRelease:
# # Edit/recreate the metadata with the new files
# submitanalysis.py projectid=1020218 --rereleasetoIMG=<submission id to be replaced>
# jat import <template name> .


#
# # ## @@ ##
9) QC Report
Assembly size = MMM Mb; CCC contigs; 
Confirmed target genome:
SILVA: 
Collab_16S: 
Megan: correct Family-Genus-Species
No indications of non-target genome(s): uni-modal gc-coverage distribution; single-copy: mean=1, median=1.  

#
# # cd ..   # from the run dir
# # module load jamo/prod qaqc
# # submitanalysis.py $PROJ_ID
chgrp -R genome `pwd` &&  chmod -R 775 `pwd`

# # # check content of metadata.json

# # --  jat import <template name> .
# # jat import microbial_isolate_improved_pacbio `pwd`; date