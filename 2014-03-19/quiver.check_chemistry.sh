#!/bin/bash

# ayspunde 2014-03-27

# assertion: directory with quiver_test already finished
## bash /global/projectb/sandbox/rqc/ayspunde/pacbio/2014-03-19/quiver.check_chemistry.sh

d2check=(/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s699/028280-cleanP4
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s331/027031
/global/projectb/scratch/kmdw/microbes/Teredinibacterturnerae1675/027022-cleanP4
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s863/027334-cleanP4
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s420/026979-cleanP4
/global/projectb/scratch/kmdw/microbes/Butyrivibrio_AE2032
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s732/027314-cleanP4
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s367/027023-cleanP4
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc20/s754/027753-cleanP4
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s939/027438-cleanP4
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s733/027315-cleanP4
/global/projectb/scratch/kmdw/microbes/Prevotella/026852-cleanP4
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s700/027326-cleanP4
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s933/027338-cleanP4
/global/projectb/scratch/kmdw/microbes/Selenomonas_rum_AB3002/026883-cleanP4
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s103/027182-cleanP4-man
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc20/s749/027752-cleanP4
/global/projectb/scratch/kmdw/microbes/ClostridiummangenotiiLM2/026850-cleanP4
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s365/027011-cleanP4
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s701/027312-cleanP4
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s784/027330-cleanP4
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s950/027439-cleanP4
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s364/027068
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s372/027008-cleanP4
/global/projectb/scratch/kmdw/microbes/AcidobacteriaceaeKBS146
/global/projectb/scratch/kmdw/microbes/StaphylococcusepidermidisAG42/027555
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s651/027319-cleanP4
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s698/027324-cleanP4
/global/projectb/scratch/kmdw/microbes/Selenomonas_ruminantium_AC2024/026847-cleanP4
/global/projectb/scratch/kmdw/Alteromonadaceaebacterium2141
/global/projectb/scratch/kmdw/microbes/ClostridiumalgidicarnisB3/027548-cleanP4
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s333/027056
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s702/027318-cleanP4
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s652/027320-cleanP4
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s862/027333-cleanP4
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s671/027322-cleanP4
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s928/027335-cleanP4
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc10/s653/027321-cleanP4
)

for  ((i=0; i < ${#d2check[@]} ; i++)); do
	run_dir=${d2check[$i]}
	chem_xml=$run_dir/'data/chemistry_mapping.xml'
	ls -al $chem_xml
done

# i=0
# for qd in  /global/projectb/scratch/ayspunde/qc/impQD/pacbio/*/*/*-cleanP4/data*-m*; do
	# qd_dir=$(dirname $qd)
	# if [  $i -eq 0 ]; then
		# printf "%s," $qd_dir
	# fi
	# if [ $i -lt  4 ]; then
		# nCorr=$(grep -c -v '#' $qd/corrections.gff)
		# printf "%d," $nCorr 
	# fi
	# (( i++ ))
	# if [ $i -eq  4 ]; then
		# i=0 
		# printf "\n"
	# fi
# done
# printf "\n"
