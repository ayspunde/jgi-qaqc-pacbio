#!/bin/bash

########### TASK metadata #############
# Task            : enrichAlnSummary
# Module          : P_AssemblyPolishing
# TaskType        : None
# URL             : task://028069/P_AssemblyPolishing/enrichAlnSummary
# createdAt       : 2014-04-10 15:59:17.908165
# createdAt (UTC) : 2014-04-10 22:59:17.908197
# ncmds           : 2
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_AssemblyPolishing/enrichAlnSummary.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_AssemblyPolishing/enrichAlnSummary.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/alignment_summary.gff
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrections.gff
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/alignment_summary.gff
#
########### END TASK metadata #############

# Writing to log file
cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_AssemblyPolishing/enrichAlnSummary.sh >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_AssemblyPolishing/enrichAlnSummary.log;



echo "Running task://028069/P_AssemblyPolishing/enrichAlnSummary on $(uname -a)"

echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/alignment_summary.gff ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/alignment_summary.gff'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/alignment_summary.gff.'
fi
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrections.gff ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrections.gff'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrections.gff.'
fi
echo 'Successfully validated input files'

# Task enrichAlnSummary commands:


# Completed writing Task enrichAlnSummary commands


# Task 1
summarizeConsensus.py --variantsGff /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/corrections.gff /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/alignment_summary.gff --output /global/projectb/shared/software/smrtanalysis/2.1.1/tmpdir/tmpKGrHsG.gff || exit $?
echo "Task 1 completed at $(date)"
# Task 2
mv /global/projectb/shared/software/smrtanalysis/2.1.1/tmpdir/tmpKGrHsG.gff /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/alignment_summary.gff || exit $?
echo "Task 2 completed at $(date)"



echo "Finished on $(date -u)"
# Success
exit 0
