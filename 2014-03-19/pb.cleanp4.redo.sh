#!/bin/bash -l

# ayspunde 2014-03-19

# workaround for incorporating metadata.xml in the data layout
## bash /global/projectb/sandbox/rqc/ayspunde/pacbio/2014-03-19/pb.cleanp4.redo.sh

CURRENT_ROOT='/global/projectb/sandbox/rqc/ayspunde/pacbio'
SMRT_VERSION='2.1.1'

# blasr, smrtpipe
module load smrtanalysis
    

CLEAN_DIR=`pwd`
DIRTY_DIR=$(echo $CLEAN_DIR | sed  -e "s/-cleanP4.*//")
PB_JOB_ID=$(echo $(basename $DIRTY_DIR) | sed 's/^0//')

printf "Starting cleanP4:\nDIRTY_DIR=%s\nCLEAN_DIR=%s\n\n" $DIRTY_DIR $CLEAN_DIR
# exit

# workaround for incorporating metadata.xml in the data layout
printf "Starting OUT_XML:\nWORK_DIR=%s\n\n" $(pwd)
INPUT_XML=$CLEAN_DIR/dirty.input.xml ; 	ls -al $INPUT_XML
OUT_XML=$CLEAN_DIR/input.xml ;   		
if [ -e $OUT_XML ]; then mv $OUT_XML $OUT_XML.old; fi

while read xml_lane; do
	OUT_XML_lane=$(echo $xml_lane | sed -e "s:##48h:.CMD:" )
	OUT_XML_lane=$(echo $OUT_XML_lane | sed -e "s:/projectb/shared/pacbio:"$CLEAN_DIR":")
	printf "%s\n" "$OUT_XML_lane" >> $OUT_XML
	if (( $(expr "$OUT_XML_lane" : '<data ref=') > 0 )) ; then
		cell_meta_in=$(echo $xml_lane | sed  -e 's/<data ref="//' -e  's/">//')'/*metadata.xml'
		for mm in $cell_meta_in; do
			cell_meta_out=$(echo $mm | sed -e "s:/projectb/shared/pacbio:"$CLEAN_DIR":")
			ls -al $mm
			echo $cell_meta_out
			cp $mm $cell_meta_out
		done
	fi
	if (( $(expr "$OUT_XML_lane" : '<location>') > 0 )) ; then
		h5_path_out=$(echo $OUT_XML_lane | sed  -e 's/<location>//' -e  's/<\/location>//')
		# h5_base=$(basename $h5_path) ; 
		h5_path_in=$(echo $h5_path_out | sed -e 's:/Analysis_Results/:/Analysis_Results/clean.:')
		# echo $h5_path_in
		# echo $h5_path_out
		if [ -e $h5_path_in ]; then
			mv $h5_path_in $h5_path_out
		fi
	fi
done < $INPUT_XML

cd $CLEAN_DIR
chgrp -R genome $CLEAN_DIR ; chmod -R 775  $CLEAN_DIR
ls -al $OUT_XML
printf "DONE with OUT_XML:\nWORK_DIR=%s\n\n" $(pwd)

# # # qsub SMRT job with cleanP4 h5's
# # # if [ $N_FAIL_H5 -eq 0 ]; then
	# # # printf "Starting SMRT Pipe:\nWORK_DIR=%s\njob name = %s\n\n" $(pwd) "smrt${PB_JOB_ID}"
	# # # qsub -S /bin/bash -cwd -j yes -w e -b yes -M `whoami`@lbl.gov -m bea \
	# # # -l h_rt=72:00:00 -l ram.c=7.5G -pe pe_slots 16 -N smrt${PB_JOB_ID} -P prok-assembly.p \
	# # # ". /global/projectb/shared/software/smrtanalysis/$SMRT_VERSION/etc/setup.sh && smrtpipe.py --params=settings.xml xml:input.xml" 
# # # else
	# # # printf "ERROR: h5 cleaner failed on %d files\n WORK_DIR:\n%s\n\n" $N_FAIL_H5 $CLEAN_DIR
# # # fi
if [ -e $OUT_XML ]; then
	printf "Starting SMRT Pipe:\nWORK_DIR=%s\njob name = %s\n\n" $(pwd) "smrt${PB_JOB_ID}"
	qsub -S /bin/bash -cwd -j yes -w e -b yes -M `whoami`@lbl.gov -m bea \
	-l h_rt=72:00:00 -l ram.c=7.5G -pe pe_slots 16 -N smrt${PB_JOB_ID} -P prok-assembly.p \
	". /global/projectb/shared/software/smrtanalysis/$SMRT_VERSION/current/etc/setup.sh && smrtpipe.py --params=settings.xml xml:input.xml"  
else
	printf 'No input.xml: SKIP:\nWORK_DIR=%s\njob name = %s\n\n' $(pwd) "smrt${PB_JOB_ID}"
fi

