#!/bin/bash -l

# ayspunde SMRTv220
##> bash /global/projectb/sandbox/rqc/ayspunde/pacbio/SMRTv220/pb.rlink.sh

# link, not rsync, successful SMRT portal jobs to scratch:

# PACBIO_PORTAL is SDM repository of SMRT Portal jobs
# PACBIO_LOCAL is your working dir


PB_PORTAL_DIR=$1
DIRTY_DIR=$2
CLEAN_DIR=$3
USE_PB_FILT=$4

dir2cp=(data log movie_metadata reference results workflow)

# make link to portal jobs; h5's not cleaned yet
ln -s $PB_PORTAL_DIR $DIRTY_DIR

# eventually will clean h5 in*-cleanP4 dir, for now just one more set of links
if [ $USE_PB_FILT -eq '1' ]; then
	for ((i=0; i < ${#dir2cp[@]} ; i++)); do
		# option as a single list of key-value pairs
		d2c=${dir2cp[$i]}
		# printf 'linking: %s\n' $d2c
		ln -s $PB_PORTAL_DIR/$d2c $CLEAN_DIR/$d2c && printf 'linked: %s\n' $d2c
	done
fi

chgrp -R genome `pwd` &
