#!/bin/bash -l

# ayspunde 2014-03-19
# clean (remove) P4 spike-ins from h5 smrtDC files, 
# re-run cmd-line smrt pipe with original analysis settings (presumably optimized at SMRT-Portal) 

# assumptions: rsync/rlink-ed SMRT portal jobs to scratch: pb.rsync.sh | pb.rlink.sh
# called from clean-P4 (s123/012345-cleanP4) directory: manually by DRIVER 

## bash /global/projectb/sandbox/rqc/ayspunde/pacbio/DEFAULT/pb.cleanp4.sh


CURRENT_VER='/global/projectb/sandbox/rqc/ayspunde/pacbio/DEFAULT'
SMRT_VERSION='2.1.1'
# SMRT_VERSION='2.2.0p1'

P4_SPIKE_IN="$CURRENT_VER/../ref/spike/4kb_Control.fasta"
BVER='211'
BLASR2USE="$CURRENT_VER/../BLASR/blasr_$BVER/blasr"
CLEANER_SUBSHELL="$CURRENT_VER/pb.cleaner_subshell.sh"
CMD_JIB_ID_FILE="$CURRENT_VER/../CMD_JOB_ID.txt"

# blasr, smrtpipe
module load smrtanalysis
    

# false JOB_ID: needed to bring SMRT job into SMRT Portal for review without conflicting with original jobID 
CMD_JOB_ID=$(cat $CMD_JIB_ID_FILE)
(( CMD_JOB_ID -- ))
echo $CMD_JOB_ID > $CMD_JIB_ID_FILE

# cleaning part: identify contaminated reads, create clean*h5
CLEAN_DIR=`pwd`
DIRTY_DIR=$(echo $CLEAN_DIR | sed  -e "s/-cleanP4.*//")
PB_JOB_ID=$(echo $(basename $DIRTY_DIR) | sed 's/^0//')

printf "Starting cleanP4:\nDIRTY_DIR=%s\nCLEAN_DIR=%s\n\n" $DIRTY_DIR $CLEAN_DIR

# no changes to settings.xml and input.fofn 
# input.xml will be modified downstream, need a copy of original for re-runs (dirty.input.xml)
cp $DIRTY_DIR/settings.xml $CLEAN_DIR
cp $DIRTY_DIR/input.fofn   $CLEAN_DIR
cp $DIRTY_DIR/input.xml $CLEAN_DIR/dirty.input.xml

mkdir -m 775 $CLEAN_DIR/cleanP4
cd $CLEAN_DIR/cleanP4
cp $CLEAN_DIR/input.fofn  ./

# #run blasr to make list of reads with P4 spike-ins:
# on high-mem node send BLASR jobs in background, on low-mem run sequentially (#&)
touch p4c.alignment.$BVER.txt
printf "Starting BLASR:\nWORK_DIR=%s\nBLASR=%s\n\n" $(pwd) $BLASR2USE
while read BLASR_INP; do
	ls -l $BLASR_INP $P4_SPIKE_IN
	$BLASR2USE $BLASR_INP $P4_SPIKE_IN -bestn 1 >> p4c.alignment.$BVER.txt &
done < ./input.fofn
wait
ls -l p4c.alignment.$BVER.txt
printf "Done with BLASR:\nWORK_DIR=%s\n\n" $(pwd) 


## to avoid ugly problems caused by python/biopython/smrtanalysis loads/unloads
## it's just a call to cleaner, but from the clean environment
## cleaner_full.py input.fofn p4c.alignment*txt
printf "Starting CLEANER:\nWORK_DIR=%s\n\n" $(pwd)
bash -l $CLEANER_SUBSHELL
N_DIRTY_H5=$(cat input.fofn | wc -l)
N_CLEAN_H5=$(ls clean*h5 | wc -l)
printf "CLEANER finised:\nWORK_DIR=%s\n" $(pwd)
if [ $N_DIRTY_H5 -eq $N_CLEAN_H5 ]; then
	printf "No problems: nCleanH5 == nInputH5 =%d\n\n" $N_DIRTY_H5
else
	printf "\nWARNING: nCleanH5=%d < nInputH5=%d\n\n" $N_CLEAN_H5 $N_DIRTY_H5
fi

# make new input.xml:
# point to the clean*h5s, change jobID to some artificial number in case decide to upload back to SMRT Portal without deleting original job
# organize clean*h5 in same hierarchy as original h5s: required to match with chemistry (metadata.xml)

printf "Starting OUT_XML:\nWORK_DIR=%s\n\n" $(pwd)
INPUT_XML=$CLEAN_DIR/dirty.input.xml ; 	ls -l $INPUT_XML
OUT_XML=$CLEAN_DIR/input.xml ;   		
if [ -e $OUT_XML ]; then mv $OUT_XML $OUT_XML.old; fi

while read xml_lane; do
	OUT_XML_lane=$(echo $xml_lane | sed -e "s:##*h:.CMD:" )
	OUT_XML_lane=$(echo $OUT_XML_lane | sed -e "s:/projectb/shared/pacbio:"$CLEAN_DIR":")
	printf "%s\n" "$OUT_XML_lane" >> $OUT_XML
    # <data_ref= location of metadata.xml
    if (( $(expr "$OUT_XML_lane" : '<data ref=') > 0 )) ; then
		meta_xml_dir_in=$(echo $xml_lane | sed  -e 's/<data ref="//' -e  's/">//')
        meta_xml_dir_out=$(echo $OUT_XML_lane | sed  -e 's/<data ref="//' -e  's/">//')
		if [ ! -d $meta_xml_dir_out ]; then
			mkdir -p -m 775 $meta_xml_dir_out
		fi
		meta_xml_fp_in=$meta_xml_dir_in/*metadata.xml
		ls -l $meta_xml_fp_in
		echo "Moving metadata.xml to $meta_xml_dir_out"
		cp $meta_xml_fp_in $meta_xml_dir_out
	fi
    # <location>/ = h5 file paths
    if (( $(expr "$OUT_XML_lane" : '<location>') > 0 )) ; then
        h5_fp_dirty=$(echo $xml_lane | sed  -e 's/<location>//' -e  's/<\/location>//')
        h5_fp_out=$(echo $OUT_XML_lane | sed  -e 's/<location>//' -e  's/<\/location>//'  )
        h5_out_dir=$(dirname $h5_fp_out)  ; echo $h5_out_dir
        h5_out_base=$(basename $h5_fp_out) ; echo $h5_out_base
        h5_fp_clean=$CLEAN_DIR/cleanP4/clean.$h5_out_base
        if [ ! -d $h5_out_dir ]; then
			mkdir -p -m 775 $h5_out_dir
		fi
        if [ -e $h5_fp_clean ]; then # clean*h5 were made by cleaner.py and sitting in cwd
            ls -l $h5_fp_clean
            mv $h5_fp_clean $h5_fp_out
        else    # hack: no clean*h5s for XL-C2 h5s were made by cleaner.py; grab originals
            ls -l $CLEAN_DIR/cleanP4/$h5_base
            printf "\nWARNING: no local clean*h5: bringing original %s \n\n" $h5_base 
            cp $h5_fp_dirty $h5_fp_out
        fi		
	fi
done < $INPUT_XML


cd $CLEAN_DIR
chgrp -R genome $CLEAN_DIR ; chmod -R 775  $CLEAN_DIR
ls -l $OUT_XML
printf "DONE with OUT_XML:\nWORK_DIR=%s\n\n" $(pwd)


if [ -e $OUT_XML ]; then
	case $SMRT_VERSION in
		'2.1.1')
		printf "Starting SMRT Pipe:\nWORK_DIR=%s\njob name = %s\n\n" $(pwd) "smrt${PB_JOB_ID}"
		qsub -S /bin/bash -cwd -j yes -w e -b yes -M `whoami`@lbl.gov -m bea \
			-l h_rt=72:00:00 -l ram.c=5G -pe pe_slots 8 -N smrt${PB_JOB_ID} -P prok-assembly.p \
			". /global/projectb/shared/software/smrtanalysis/$SMRT_VERSION/current/etc/setup.sh && smrtpipe.py --params=settings.xml xml:input.xml" 
		;;
		'2.2.0p1')
		printf "Starting SMRT Pipe:\nWORK_DIR=%s\njob name = %s\n\n" $(pwd) "smrt${PB_JOB_ID}"
		qsub -S /bin/bash -cwd -j yes -w e -b yes -M `whoami`@lbl.gov -m bea \
			-l h_rt=12:00:00 -l ram.c=5G -pe pe_slots 8 -N smrt${PB_JOB_ID} -P prok-assembly.p \
			". /global/u2/s/smrt_dev/smrtanalysis/current/etc/setup.sh && smrtpipe.py --params=settings.xml xml:input.xml" 
		;;
	esac
else
	printf 'No input.xml: SKIP:\nWORK_DIR=%s\njob name = %s\n\n' $(pwd) "smrt${PB_JOB_ID}"
fi


