#!/bin/bash -l

# ayspunde 2014-01-27
# assuming in the project (NNN-cleanP4)/prelimQC
# bash /global/projectb/sandbox/rqc/ayspunde/pacbio/2014-01-24/pb.prelim_qc.sh

## !! 
## -- deduplication currently manual process, make sure that 
## $this_asm_dedup is ready before processing
## !!

# # ### Uncomment for stand-alone, from (NNN-cleanP4)
# this_work_dir='prelimQC'
# mkdir -m 775 $this_work_dir
# cd $this_work_dir

### SET--START
module load hmmer
module load mummer 
module load qaqc/prod

CURRENT_ROOT='/global/projectb/sandbox/rqc/ayspunde/pacbio'

P4_SPIKE_IN="$CURRENT_ROOT/ref/spike/4kb_Control.fasta"
BVER='211'
BLASR2USE="$CURRENT_ROOT/BLASR/blasr_$BVER/blasr"

SINGLE_COPY_PL='/projectb/sandbox/rqc/prod/pipelines/external_tools/singleGeneCopy/estimateGenomeCompleteness.pl'
SINGLE_COPY_HMM_BACTERIA='/projectb/sandbox/rqc/prod/pipelines/external_tools/singleGeneCopy/data/bacteria_hmms'

### SET--DONE

this_asm='polished_assembly.fasta'
if [ -e ../data/$this_asm ]; then
	ln -s ../data/$this_asm ./$this_asm
elif [ -e ../data/$this_asm.gz ]; then
	gunzip -c ../data/$this_asm.gz > ./$this_asm
else
	printf '\n\nERROR: no %s in DIR %s\n\n' $this_asm $(realpath ..) 
	exit 1
fi

this_deg='celera-assembler.deg.fasta'
ln -s ../data/9-terminator/$this_deg ./$this_deg

this_asm_deg='polished_assembly.deg.fasta'
cat $this_asm $this_deg > $this_asm_deg

## checking for duplications

## opt1: mummer
# module load mummer 
seqN=$( basename $(realpath ./../..))
nucmer $this_asm_deg $this_asm_deg
mummerplot -l --color -t png out.delta
# S1 ,E1 ,S2 ,E2 ,LEN1 ,LEN2 ,'%_IDY' ,LEN_R ,LEN_Q ,COV_R ,COV_Q ,TAG1,TAG2
show-coords -r -c -l out.delta > nucmer.coord.$seqN.csv
printf 'S1 ,E1 ,S2 ,E2 ,LEN1 ,LEN2 ,PCT_IDY ,LEN_R ,LEN_Q ,COV_R ,COV_Q ,TAG1 ,TAG2' >> nucmer.coord.$seqN.csv
mv out.png out.$seqN.png

# ## opt 2: BB dedup
# this_asm_dedup='polished_assembly.deg.dedup.fasta'
# dedupe.sh in=$this_asm_deg out=$this_asm_dedup minidentity=99
## - currently not working on hairpins, most common form of atrifacts so far


##
##manual editing after this step
##
this_asm_dedup='polished_assembly.deg.dedup.fasta'
cat $this_asm_deg > $this_asm_dedup

##
## megablast: SILVA and Collab_16S
##
# module load qaqc/prod

LLLL_16SREFSEQ='/global/projectb/sandbox/rqc/qcdb/collab16s/collab16s.fa'
SILVA_16SEQ='/global/dna/shared/rqc/ref_databases/qaqc/databases/silva/2011.04.silva/SSURef_106_tax_silva.fasta'
REFSEQ_MICROBIAL='/global/dna/shared/rqc/ref_databases/ncbi/CURRENT/refseq.microbial/refseq.microbial'

run_megablast.sh $this_asm_deg $LLLL_16SREFSEQ -p 90 &
run_megablast.sh $this_asm_deg $SILVA_16SEQ -p 90 & 
run_megablast.sh $this_asm_deg $REFSEQ_MICROBIAL -p 90 & 

## checking that P4 spikein were cleared
## do not check clean*h5, blasr ignores qFlag (set to zero by cleaner.py) 
## - and therefore detecting spike-in even in clean*h5s
CORRECTED_FASTA='corrected.fasta'
ln -s ../data/$CORRECTED_FASTA ./$CORRECTED_FASTA
FASTA2CHECK=$CORRECTED_FASTA
ALIGN_SUMM='spike.align2corr.txt'
# $BLASR2USE $FASTA2CHECK $P4_SPIKE_IN -bestn 1 -header > $ALIGN_SUMM
qsub -S /bin/bash -cwd -j yes -w e -b yes \
-l h_rt=12:00:00 -l ram.c=8G  -N p4Corr$seqN -P prok-assembly.p \
"$BLASR2USE $FASTA2CHECK $P4_SPIKE_IN -bestn 1 > $ALIGN_SUMM" 

FILTERED_FASTA='filtered_subreads.fasta'
ln -s ../data/$FILTERED_FASTA ./$FILTERED_FASTA
FASTA2CHECK=$FILTERED_FASTA
ALIGN_SUMM='spike.align2filt.txt'
# $BLASR2USE $FASTA2CHECK $P4_SPIKE_IN -bestn 1 -header > $ALIGN_SUMM
qsub -S /bin/bash -cwd -j yes -w e -b yes \
-l h_rt=12:00:00 -l ram.c=8G  -N p4Filt$seqN -P prok-assembly.p \
"$BLASR2USE $FASTA2CHECK $P4_SPIKE_IN -bestn 1 > $ALIGN_SUMM" 


## check for single-copy
## 
# module load hmmer/3.0
this_asm_dedup='polished_assembly.deg.dedup.fasta'
SINGLE_COPY_LIST_BACTERIA=$(pwd)/'singleCopyGeneList.bacteria.txt'
SINGLE_COPY_SUMM=$(pwd)/'summary.bacteria.txt'
$SINGLE_COPY_PL --header -countFile $SINGLE_COPY_LIST_BACTERIA $SINGLE_COPY_HMM_BACTERIA $this_asm_dedup > $SINGLE_COPY_SUMM &

