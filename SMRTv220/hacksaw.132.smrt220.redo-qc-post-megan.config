# Hacksaw pipeline config file v 1.3.0 
# for use with Hgap3 outputs

# Runs smrtpipe all-in-one.
execute.all.runSmrtPipe=0

# post analysis
execute.addDegenerateContigs=0
execute.circularizeContigs=0
execute.polishCircularizeContigs=0
execute.assemblyQc=1
execute.gcVsCovTaxPlotter=1
execute.ncbiScreen=1
execute.convertHdf5ToFasta=1
execute.createStatsFile=1
execute.createQdReport=1
execute.symlinkFiles=1
execute.cleanupFiles=1

# Envionment vars
#
env.SMRTPIPE_EXE=/global/projectb/shared/software/smrtanalysis/2.1.1/current/analysis/bin/smrtpipe.py

# for gcCovTaxPacbio.py
env.BB_TOOLS_PATH=/usr/common/jgi/utilities/bbtools/prod-v33.40/bin
env.MAP_PACBIO_EXE=/usr/common/jgi/utilities/bbtools/prod-v33.40/bin/mapPacBio.sh
env.PILEUP_EXE=/usr/common/jgi/utilities/bbtools/prod-v33.40/bin/pileup.sh
env.GC_TOOLS_PATH=/projectb/sandbox/rqc/prod/pipelines/external_tools/gc_cov/DEFAULT/bin
env.SHRED_FA_EXE=/projectb/sandbox/rqc/prod/pipelines/external_tools/gc_cov/DEFAULT/bin/shredFa.pl
env.SCAFF_TO_CONTIGS_EXE=/projectb/sandbox/rqc/prod/pipelines/external_tools/gc_cov/DEFAULT/scaffToContigs.pl
# for assemblyQc.pl
env.TAXMAPPER_EXE=/projectb/sandbox/rqc/prod/pipelines/external_tools/taxMapper/20130807/taxMapper.pl
env.KMER_FREQUENCIES_JAR=java -jar /projectb/sandbox/rqc/prod/pipelines/external_tools/tetramerK/20120823/KmerFrequencies.jar -n
env.SHOW_KMER_BIN_R=xvfb-run -a Rscript --vanilla /projectb/sandbox/rqc/prod/pipelines/external_tools/tetramerK/20120823/showKmerBin.R
env.TRIM_TOPOLOGY_EXE=/usr/common/jgi/qaqc/jgi-qaqc/detectCircle/trimAndTopologyUsingPb.py
env.GNUPLOT_EXE=/usr/common/jgi/math/gnuplot/4.6.2/bin/gnuplot
env.PRODIGAL_EXE=/usr/common/jgi/annotators/prodigal/2.50/bin/prodigal
env.FASTX_QUALITY_STATS=/usr/common/jgi/seqtools/fastx/0.0.13.2/bin/fastx_quality_stats
env.MEGABLAST_EXE=/usr/common/jgi/aligners/blast/2.2.26/bin/megablast
env.FORMATDB_EXE=/usr/common/jgi/aligners/blast/2.2.26/bin/formatdb
env.BLASTALL_EXE=/usr/common/jgi/aligners/blast/2.2.26/bin/blastall
env.BOWTIE_INDEX_EXE=/usr/common/jgi/aligners/bowtie/0.12.8/bin/bowtie-build
env.BOWTIE_EXE=/usr/common/jgi/aligners/bowtie/0.12.8/bin/bowtie
env.SAMTOOLS_EXE=/global/projectb/shared/software/smrtanalysis/2.0.1/analysis/bin/samtools
env.JAVA=/usr/common/usg/languages/java/jdk/oracle/1.7.0_45_x86_64/bin/java
env.RSCRIPT=/global/common/genepool/usg/languages/R/3.0.1/bin/Rscript
env.XVFB_RUN=/projectb/sandbox/rqc/prod/pipelines/external_tools/xvfb/xvfb-run
env.ZIP_EXE=/bin/gzip

# Module load file
file.JGImoduleFile=../config/Hacksaw/hacksaw_module_load.txt

# Data files
#
hgap.assemblyOutFile=data2/polished_assembly.fasta.gz
hgap.filteredSubreadsFastaFile=data2/no_control_filtered_subreads.fasta
hgap.filteredSubreadsFastqFile=data2/no_control_filtered_subreads.fastq
hgap.referenceIndexFile=reference/sequence/reference.fasta.index
hgap.assemblyPlusDegenerateFasta=prelimQC/polished_assembly.dedup.fasta

# used for symlinking only
hgap.correctedFastqFile=data2/no_control_corrected.fastq
hgap.correctedFastaFile=data2/no_control_corrected.fasta

# Contig circularization
#
#circularize.assemblyOutFile=circularize/finalContigs.fasta
#circularize.referenceUploaderDirName=reference_cir
#circularize.quiverXmlFile=quiver_cir.xml
#circularize.quiverInFile=circularize/finalContigs.fasta
#circularize.quiverOutFile=data/polished_assembly.fasta.gz

# FastqToCA
#
#fastqToCA.params=-technology sanger -type sanger -libraryname reads

# Celera
#
#celera.params=-d data -p celera-assembler

# ReferenceUploader
#
#referenceUploader.params=-c -n reference

# Ncbi Pre-screening
#
ncbiPreScreen.outputFile=ncbi_screen.out

# Stats files
#
statsFile.celera=data2/consensus.fasta.stats
statsFile.hacksaw=hacksaw_stats.txt

# Pipeline dirs 
#
dir.assemblyQcDir=assemblyQC
dir.gcCovDir=assemblyQC/gc_cov
dir.ncbiPreScreen=assemblyQC/ncbi_screen
dir.configFiles=config
dir.finalDir=final
dir.circularize=circularize

# Assembly QC
#
assemblyQc.megablastFoF=../config/Hacksaw/gcCovByTaxDatabase.fof
assemblyQc.configFile=/global/projectb/sandbox/rqc/ayspunde/pacbio/SMRTv220/assemblyQc.post-megan.config

# Misc
#
file.imageNotFound=../config/image_not_avail.jpg

# Reporting specifics
#
report.estGenomeSize=5000000
report.method=PacbioOnly
report.title=JGI IMPROVED QUALITY DRAFT QC AND ASSEMBLY REPORT
report.qdReport=final/QD.finalReport.txt

# Scripts
#
script.assemblyQc=assemblyQc.pl
script.gcCovTaxPlot=gcCovTaxPacbio.py
script.fastaStats=fasta_stats2.linux
script.gc=gc.pl
script.drawHistogram=drawHistogram.pl
script.fastaLength=fastalen.pl
script.qdReporter=hacksaw_txt.pl
script.ncbiPreScreen=/projectb/sandbox/rqc/prod/pipelines/external_tools/ncbiScreening/ncbiPreScreen.pl

# DESCRIPTION OF KEY/VALUE PAIRS:
#
# execute.errorCorrector=0|1 - runs error corrector (only applies if -smrtpipe
#    option IS NOT specified.
# execute.fastqToCA=0|1 - convert fastq to .frg file (only applies if -smrtpipe
#    option IS NOT specified.
# execute.celeraAssembler=0|1 - runs celera assembler (only applies if -smrtpipe
#    option IS NOT specified.
# execute.referenceUploader=0|1 - creates index files of the reference (only
#    applies if -smrtpipe option IS NOT specified).
# execute.quiver=0|1 - runs quiver (only applies if -smrtpipe option IS NOT
#    specified).
# execute.runSmrtPipe=0|1 - runs hgap pipeline as a whole (mimic run from smrt
#    portal. Only applies if -smrtpipe option IS specified.)
# execute.assemblyQc=0|1 - runs assembly qc tools.
# execute.gcVsCovTaxPlotter=0|1 - runs GC vs coverage plotter.
# execute.convertHdf5ToFasta=0|1 - convert hdf5 files to fasta files for
#    computing stats.
# execute.createStatsFile=0|1 - creates stats file (default=hacksaw_stats.txt).
# execute.symlinkFiles=0|1 - creates symlinks in final dir.
# execute.cleanupFiles=0|1 - remove intermediate files.
#
# config.errorCorrection=<file> - xml file for running error corrector
#    (smrtpipe.py). If a relative is specified, hacksaw looks for the file in
#    the pipeline's install config path.
# config.celeraAssembler=<file> - config file for running celera (runCA). If
#    a relative is specified, hacksaw looks for the file in the pipeline's
#    install config path.
# config.celeraAssembler=<file> - config file for running celera (runCA). If
#    relative is specified, hacksaw looks for the file in the pipeline's install
#    config path.
#
# ...
