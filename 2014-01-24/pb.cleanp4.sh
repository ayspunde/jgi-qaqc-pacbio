#!/bin/bash -l

# ayspunde 2014-01-24

# to clean P4 spike-in
# assuming rsync-ed SMRT portal jobs to scratch: pb.rsync.sh 
# to kick-in hacksaw call pb.hacksaw.sh

## assuming run by pb.DRIVER.sh, and already in -cleanP4 dir
## for stand alone use:
## bash /global/projectb/sandbox/rqc/ayspunde/pacbio/2014-01-24/pb.cleanp4.sh

CURRENT_ROOT='/global/projectb/sandbox/rqc/ayspunde/pacbio'
# SMRT_VERSION='2.0.1'
SMRT_VERSION='2.1.1'
P4_SPIKE_IN="$CURRENT_ROOT/ref/spike/4kb_Control.fasta"
BVER='211'
BLASR2USE="$CURRENT_ROOT/BLASR/blasr_$BVER/blasr"
CLEANER_SUBSHELL="$CURRENT_ROOT/2014-01-24/pb.cleaner_subshell.sh"
CMD_JIB_ID_FILE="$CURRENT_ROOT/CMD_JOB_ID.txt"

# blasr, smrtpipe
module load smrtanalysis
    

# false JOB_ID: needed to bring SMRT job into SMRT Portal for review without conflicting with original jobID 
CMD_JOB_ID=$(cat $CMD_JIB_ID_FILE)
(( CMD_JOB_ID -- ))
echo $CMD_JOB_ID > $CMD_JIB_ID_FILE

# cleaning part: identify contaminated reads, create clean*h5
CLEAN_DIR=`pwd`
DIRTY_DIR=$(echo $CLEAN_DIR | sed  -e "s/-cleanP4.*//")
PB_JOB_ID=$(echo $(basename $DIRTY_DIR) | sed 's/^0//')

printf "Starting cleanP4:\nDIRTY_DIR=%s\nCLEAN_DIR=%s\n\n" $DIRTY_DIR $CLEAN_DIR

# no changes to settings.xml and input.fofn 
# input.xml will be modifed downstream
cp $DIRTY_DIR/settings.xml $CLEAN_DIR
mkdir -m 775 $CLEAN_DIR/cleanP4
cd $CLEAN_DIR/cleanP4
cp $DIRTY_DIR/input.fofn  ./

# #run blasr to make list of reads with P4 spike-ins
touch p4c.alignment.$BVER.txt
printf "Starting BLASR:\nWORK_DIR=%s\nBLASR=%s\n\n" $(pwd) $BLASR2USE
while read BLASR_INP; do
	ls -al $BLASR_INP $P4_SPIKE_IN
	$BLASR2USE $BLASR_INP $P4_SPIKE_IN -bestn 1 >> p4c.alignment.$BVER.txt &
done < ./input.fofn
wait
ls -al p4c.alignment.$BVER.txt
printf "Done with BLASR:\nWORK_DIR=%s\n\n" $(pwd) 


## to avoid ugly problems caused by python/biopython/smrtanalysis loads/unloads
## it's just a call to cleaner, but from the clean environment
## cleaner_full.py input.fofn p4c.alignment*txt
printf "Starting CLEANER:\nWORK_DIR=%s\n\n" $(pwd)
bash -l $CLEANER_SUBSHELL
N_DIRTY_H5=$(cat input.fofn | wc -l)
N_CLEAN_H5=$(ls clean*h5 | wc -l)
printf "CLEANER finised:\nWORK_DIR=%s\n" $(pwd)
if [ $N_DIRTY_H5 -eq $N_CLEAN_H5 ]; then
	printf "No problems: nCleanH5 == nInputH5 =%d\n\n" $N_DIRTY_H5
else
	printf "\bWARNING: nCleanH5=%d < nInputH5=%d\n\n" $N_CLEAN_H5 $N_DIRTY_H5
fi

# make new input.xml:
# point to the clean*h5s, change jobID to some artificial number in case decide to upload back to SMRT Portal without deleting original job
# organize clean*h5 in same hierarchy as original h5s: not required, but makes things easier to trace
printf "Starting NEW_XML:\nWORK_DIR=%s\n\n" $(pwd)
INPUT_XML=$DIRTY_DIR/input.xml ; ls -al $INPUT_XML
NEW_XML=$CLEAN_DIR/input.xml ;   ls -al $NEW_XML ;  
rm $NEW_XML
N_FAIL_H5=0
while read xml_lane; do
	new_xml_lane=$(echo $xml_lane | sed -e "s:##48h:.CMD:" -e "s:$PB_JOB_ID:$CMD_JOB_ID:" )
	new_xml_lane=$(echo $new_xml_lane | sed -e "s:/projectb/shared/pacbio:"$CLEAN_DIR":" -e 's:/Analysis_Results/:/Analysis_Results/clean.:')
	printf "%s\n" "$new_xml_lane" >> $NEW_XML
	if (( $(expr "$new_xml_lane" : '<location>') > 0 )) ; then
		h5_path=$(echo $new_xml_lane | sed  -e 's/<location>//' -e  's/<\/location>//')
		h5_base=$(basename $h5_path) ; echo $h5_base
		h5_runs=$(dirname $h5_path)  ; echo $h5_runs
		mkdir -p -m 775 $h5_runs
		if [ -e $CLEAN_DIR/cleanP4/$h5_base ]; then # clean*h5 were made by cleaner.py and sitting in cwd
			ls -al $CLEAN_DIR/cleanP4/$h5_base
			mv $CLEAN_DIR/cleanP4/$h5_base $h5_path
		# else    # clean*h5s should be always made by cleaner.py; if not, we have a problem
			# ls -al $CLEAN_DIR/cleanP4/$h5_base
			# printf "\nERROR: no local clean*h5: %s \n\n", $h5_base
			# # # error, not a warning
			# (( N_FAIL_H5++ ))
		# fi
		else    # hack: no clean*h5s for XL-C2 h5s were made by cleaner.py; grab originals
			ls -al $CLEAN_DIR/cleanP4/$h5_base
			printf "\nWARNING: no local clean*h5: %s is an unmodfied original h5  \n\n", $h5_base
			xlc2_h5=$(echo $xml_lane | sed  -e 's/<location>//' -e  's/<\/location>//')
			ls -al $xlc2_h5
			cp $xlc2_h5 $h5_path
		fi		
	fi
done < $INPUT_XML

cd $CLEAN_DIR
chgrp -R genome $CLEAN_DIR ; chmod -R 775  $CLEAN_DIR
ls -al $NEW_XML
printf "DONE with NEW_XML:\nWORK_DIR=%s\n\n" $(pwd)

# # qsub SMRT job with cleanP4 h5's
# # if [ $N_FAIL_H5 -eq 0 ]; then
	# # printf "Starting SMRT Pipe:\nWORK_DIR=%s\njob name = %s\n\n" $(pwd) "smrt${PB_JOB_ID}"
	# # qsub -S /bin/bash -cwd -j yes -w e -b yes -M `whoami`@lbl.gov -m bea \
	# # -l h_rt=72:00:00 -l ram.c=7.5G -pe pe_slots 16 -N smrt${PB_JOB_ID} -P prok-assembly.p \
	# # ". /global/projectb/shared/software/smrtanalysis/$SMRT_VERSION/etc/setup.sh && smrtpipe.py --params=settings.xml xml:input.xml" 
# # else
	# # printf "ERROR: h5 cleaner failed on %d files\n WORK_DIR:\n%s\n\n" $N_FAIL_H5 $CLEAN_DIR
# # fi
if [ -e $NEW_XML ]; then
	printf "Starting SMRT Pipe:\nWORK_DIR=%s\njob name = %s\n\n" $(pwd) "smrt${PB_JOB_ID}"
	qsub -S /bin/bash -cwd -j yes -w e -b yes -M `whoami`@lbl.gov -m bea \
	-l h_rt=72:00:00 -l ram.c=7.5G -pe pe_slots 16 -N smrt${PB_JOB_ID} -P prok-assembly.p \
	". /global/projectb/shared/software/smrtanalysis/$SMRT_VERSION/current/etc/setup.sh && smrtpipe.py --params=settings.xml xml:input.xml"  
else
	printf 'No input.xml: SKIP:\nWORK_DIR=%s\njob name = %s\n\n' $(pwd) "smrt${PB_JOB_ID}"
fi

