#!/bin/bash

########### TASK metadata #############
# Task            : toFofn
# Module          : P_Fetch
# TaskType        : None
# URL             : task://028069/P_Fetch/toFofn
# createdAt       : 2014-04-10 15:59:17.792781
# createdAt (UTC) : 2014-04-10 22:59:17.792814
# ncmds           : 10
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_Fetch/toFofn.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_Fetch/toFofn.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.xml
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn
#
########### END TASK metadata #############

# Writing to log file
cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_Fetch/toFofn.sh >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_Fetch/toFofn.log;



echo "Running task://028069/P_Fetch/toFofn on $(uname -a)"

echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.xml ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.xml'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.xml.'
fi
echo 'Successfully validated input files'

# Task toFofn commands:


# Completed writing Task toFofn commands


# Task 1
rm -f /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn || exit $?
echo "Task 1 completed at $(date)"
# Task 2
echo '/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/runs/pbr1742_809/A01_1/Analysis_Results/m140110_030258_00123_c100581602550000001823091404021487_s1_p0.1.bax.h5' >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn || exit $?
echo "Task 2 completed at $(date)"
# Task 3
echo '/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/runs/pbr1742_809/A01_1/Analysis_Results/m140110_030258_00123_c100581602550000001823091404021487_s1_p0.2.bax.h5' >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn || exit $?
echo "Task 3 completed at $(date)"
# Task 4
echo '/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/runs/pbr1742_809/A01_1/Analysis_Results/m140110_030258_00123_c100581602550000001823091404021487_s1_p0.3.bax.h5' >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn || exit $?
echo "Task 4 completed at $(date)"
# Task 5
echo '/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/runs/pbr1744_810/A02_1/Analysis_Results/m140111_031230_00123_c100587812550000001823102004281421_s1_p0.1.bax.h5' >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn || exit $?
echo "Task 5 completed at $(date)"
# Task 6
echo '/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/runs/pbr1744_810/A02_1/Analysis_Results/m140111_031230_00123_c100587812550000001823102004281421_s1_p0.2.bax.h5' >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn || exit $?
echo "Task 6 completed at $(date)"
# Task 7
echo '/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/runs/pbr1744_810/A02_1/Analysis_Results/m140111_031230_00123_c100587812550000001823102004281421_s1_p0.3.bax.h5' >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn || exit $?
echo "Task 7 completed at $(date)"
# Task 8
echo '/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/runs/pbr1745_811/A01_1/Analysis_Results/m140114_025656_00123_c100581572550000001823091404021442_s1_p0.1.bax.h5' >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn || exit $?
echo "Task 8 completed at $(date)"
# Task 9
echo '/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/runs/pbr1745_811/A01_1/Analysis_Results/m140114_025656_00123_c100581572550000001823091404021442_s1_p0.2.bax.h5' >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn || exit $?
echo "Task 9 completed at $(date)"
# Task 10
echo '/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/runs/pbr1745_811/A01_1/Analysis_Results/m140114_025656_00123_c100581572550000001823091404021442_s1_p0.3.bax.h5' >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn || exit $?
echo "Task 10 completed at $(date)"



echo "Finished on $(date -u)"
# Success
exit 0
