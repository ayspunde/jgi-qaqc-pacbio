#!/bin/bash

# ayspunde 2014-01-06

# after hacksaw hack :) 
# currently no final links produced in the final; fix this
# - expectation that script run manually (no looping), to avoid messy conflicts with Stephans' names
# - assuming sitting in ./final
#>  bash /global/projectb/sandbox/rqc/ayspunde/pacbio/DEFAULT/pb.hacklinks.sh &

# raw.h5.reads.tar.gz
# filtered.subreads.fastq.gz
# error.corrected.reads.fastq.gz
# QC.finalReport.pdf - same as for other product types
# QD.finalReport.txt  - same as for other product types
# final.assembly.fasta - not zipped, since we haven't before
# final.assembly.topology.txt

LOC_DIR=`pwd`
LINK_LOG=$LOC_DIR/../create_links.log
just_check=0

# rm $LINK_LOG
# re-run final_report in case file was written before fixing nReads correction
# /global/projectb/sandbox/rqc/prod/pipelines/hacksaw/DEFAULT/bin/hacksaw_txt.pl ../hacksaw_stats.txt QD.finalReport.txt

printf "\n\nStarting LINKing for FINAL in %s\n\n" "$LOC_DIR"

# file2check=(rawh5 filtfqgz corrfqgz qcpdf qdtxt asmfa asmtopo)
file2check=(rawh5 filtfqgz corrfqgz  qdtxt asmfa asmtopo)
# file2check=(asmfa asmtopo)

for ((i=0; i < ${#file2check[@]} ; i++)); do
	fileType=${file2check[$i]}
    echo $fileType
    case $fileType in
    'rawh5')
        if [ ! -e raw.h5.reads.tar.gz ]; then
			printf "Starting file_type=%s\n" $fileType | tee -a $LINK_LOG
			if (( $just_check )) ; then
				printf "just checking\n" | tee -a $LINK_LOG
			elif  [  -d $LOC_DIR/../runs ]; then
				printf "Found /runs: use clean*h5\n" | tee -a $LINK_LOG
				ls -al ../runs/*/*/Analysis_Results/*.h5 | tee -a $LINK_LOG 
				tar cpvhzf raw.h5.reads.tar.gz ../runs/*/*/Analysis_Results/*.h5  2>&1 | tee -a $LINK_LOG &
				wait
				printf "Done with file_type=%s\n\n" $fileType | tee -a $LINK_LOG				
			else
				printf "No /runs: use original*h5\n" | tee -a $LINK_LOG
				INPUT_XML=$LOC_DIR/../input.xml
				while read xml_lane; do
					if (( $(expr "$xml_lane" : '<location>') > 0 )) ; then
						h5_path=$(echo $xml_lane | sed  -e 's/<location>//' -e  's/<\/location>//')
						echo $h5_path
						ln -s $h5_path ./
					fi
				done < $INPUT_XML
				ls -al ./*ba[xs].h5 | tee -a $LINK_LOG 
				tar cpvhzf raw.h5.reads.tar.gz ./*ba[xs].h5  2>&1 | tee -a $LINK_LOG &
				wait
				printf "Done with file_type=%s\n\n" $fileType | tee -a $LINK_LOG
				rm *bax.h5
			fi	
        else
			printf "Already been done: file_type=%s\n\n" $fileType | tee -a $LINK_LOG
		fi
		;;
    'filtfqgz')
        if [ ! -e filtered.subreads.fastq.gz ]; then
			printf "Starting file_type=%s\n" $fileType | tee -a $LINK_LOG
			if (( $just_check )) ; then
				printf "just checking\n" | tee -a $LINK_LOG
			elif [ -e  ../data/nocontrol_filtered_subreads.fastq ]; then
				ls -al ../data/nocontrol_filtered_subreads.fastq | tee -a $LINK_LOG 
				gzip -c ../data/nocontrol_filtered_subreads.fastq > filtered.subreads.fastq.gz & #2>&1 | tee -a $LINK_LOG &
				wait
				printf 'Done with file_type=%s\n' $fileType | tee -a $LINK_LOG			
			elif [ -e ../data/filtered_subreads.fastq ]; then
				ls -al ../data/filtered_subreads.fastq | tee -a $LINK_LOG 
				gzip -c ../data/filtered_subreads.fastq > filtered.subreads.fastq.gz & #2>&1 | tee -a $LINK_LOG &
				wait
				printf 'Done with file_type=%s\n' $fileType | tee -a $LINK_LOG	
			fi
        else
			printf "Already been done: file_type=%s\n\n" $fileType | tee -a $LINK_LOG
		fi
        ;;  
    'corrfqgz')
        if [ ! -e error.corrected.reads.fastq.gz ]; then
			printf "Starting file_type=%s\n" $fileType | tee -a $LINK_LOG
			if (( $just_check )) ; then
				printf "just checking\n" | tee -a $LINK_LOG
			else
				ls -al ../data/corrected.fastq | tee -a $LINK_LOG 
				gzip -c ../data/corrected.fastq > error.corrected.reads.fastq.gz 2>&1 | tee -a $LINK_LOG & #2>&1 
				wait
				printf 'Done with file_type=%s\n' $fileType | tee -a $LINK_LOG	
			fi
        else
			printf "Already been done: file_type=%s\n\n" $fileType | tee -a $LINK_LOG
		fi        
		;;          
    'qcpdf')
        if [  ! -e QC.finalReport.pdf ]; then
			printf "Starting file_type=%s\n" $fileType | tee -a $LINK_LOG  
			if (( $just_check )) ; then
				printf "just checking\n" | tee -a $LINK_LOG
			else			
				mv -v QC*pdf ./QC.finalReport.pdf 2>&1 | tee -a $LINK_LOG &
				wait
				printf 'Done with file_type=%s\n' $fileType | tee -a $LINK_LOG	       
			fi
		else
			printf "Already been done: file_type=%s\n\n" $fileType | tee -a $LINK_LOG
		fi
        ;;  
    'qdtxt')
        if [ ! -e QD.finalReport.txt ]; then
			printf "Starting file_type=%s\n" $fileType | tee -a $LINK_LOG        
			if (( $just_check )) ; then
				printf "just checking\n" | tee -a $LINK_LOG
			else		
				mv -v QD*txt ./QD.finalReport.txt 2>&1 | tee -a $LINK_LOG &
				wait
				printf 'Done with file_type=%s\n' $fileType | tee -a $LINK_LOG	 
			fi
        else
			printf "Already been done: file_type=%s\n\n" $fileType | tee -a $LINK_LOG
		fi
        ;;              
    'asmfa')
        if [ ! -e final.assembly.fasta ]; then
			printf "Starting file_type=%s\n" $fileType | tee -a $LINK_LOG	
			if (( $just_check )) ; then
				printf "just checking\n" | tee -a $LINK_LOG
			else
				if [ -e ../data2/polished_assembly.fasta ]; then 
					ls -al ../data2/polished_assembly.fasta | tee -a $LINK_LOG &
					cp -v ../data2/polished_assembly.fasta ./final.assembly.fasta 2>&1 | tee -a $LINK_LOG &
				elif [ -e ../circularize/finalContigs.fasta ]; then
					ls -al ../circularize/finalContigs.fasta | tee -a $LINK_LOG
					cp -v ../circularize/finalContigs.fasta ./final.assembly.fasta 2>&1 | tee -a $LINK_LOG &
				fi
				wait
				printf 'Done with file_type=%s\n' $fileType | tee -a $LINK_LOG	       
			fi
		else
			printf "Already been done: file_type=%s\n\n" $fileType | tee -a $LINK_LOG
		fi
        ;;  
    'asmtopo')
        if [ ! -e final.assembly.topology.txt ]; then
			printf "Starting file_type=%s\n" $fileType | tee -a $LINK_LOG		
            if (( $just_check )) ; then
				printf "just checking\n" | tee -a $LINK_LOG
			else
				ls -al ../circularize/finalContigs.fasta.topo.txt | tee -a $LINK_LOG	 
				cp -v ../circularize/finalContigs.fasta.topo.txt ./final.assembly.topology.txt 2>&1 | tee -a $LINK_LOG &
				wait
				printf 'Done with file_type=%s\n' $fileType | tee -a $LINK_LOG	      
			fi
		else
			printf "Already been done: file_type=%s\n\n" $fileType | tee -a $LINK_LOG
		fi
        ;;      
    esac
done
wait
printf "\n\nDone LINKing for FINAL in DIR=%s\n\n" "$LOC_DIR"
