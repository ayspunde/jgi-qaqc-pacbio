#!/bin/env bash

# ayspunde 2014-01-24

# driver for pb.*.sh : looping over whole project
## bash /global/projectb/sandbox/rqc/ayspunde/pacbio/2014-01-24/pb.DRIVER.RERELEASE.sh &

# PACBIO_LOCAL is your working dir
PACBIO_PORTAL='/global/projectb/shared/pacbio/jobs'
# -- PACBIO_LOCAL is your working dir -- #
PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc'
# PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/PacBio/seqqc2'
# PACBIO_LOCAL='/global/projectb/scratch/jasmynp/PacBio/Seqqc'

VERSION2RUN='/global/projectb/sandbox/rqc/ayspunde/pacbio/2014-01-24'
# VERSION2RUN='/global/u1/a/ayspunde/work/projects/pacbio/DEFAULT'

cd $PACBIO_LOCAL
BIG_LOG=$(pwd)/log.$(date +%F.%H%M).txt

# 2014-01-30
DIR2RERUN=(\
/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc/s702/027318-cleanP4 \
/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc/s651/027319-cleanP4 \
/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc/s652/027320-cleanP4 \
/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc/s653/027321-cleanP4 \
/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc/s698/027324-cleanP4 \
/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc/s699/027325-cleanP4 \
/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc/s733/027315-cleanP4 \
/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc/s862/027333-cleanP4 \
/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc/s863/027334-cleanP4 \
/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc/s933/027338-cleanP4 \
/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc/s939/027438-cleanP4 \
/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc/s950/027439-cleanP4 \
/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc/s671/027322-cleanP4 \
/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc/s700/027326-cleanP4 \
/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc/s701/027312-cleanP4 \
/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc/s732/027314-cleanP4 \
/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc/s784/027330-cleanP4 \
/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc/s928/027335-cleanP4 \
/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc/s333/027056 \
/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc/s364/027068 \
/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc/s103/027182-cleanP4-man \
/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc/s331/027031 \
# )
# # 2014-01-30
# DIR2RERUN=(\
/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc/s681/027323-cleanP4 \
/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc/s734/027316-cleanP4 \
/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Seqqc/s861/027332-cleanP4 \
)



# jName=(s365 s367 s368 's371' s372 s420)
# jID=( 27011 27023 26219 '27217' 27008 26979)
# jName=(s365 s367 s368  s372 s420)
# jID=( 27011 27023 26219  27008 26979)
# jName=(s347)
# jID=( 26972)
# jName=(s370  s378  s749  s754  s755  s757  s758  s760  s764   s861   s1043  s940 )
# jID=(  27349 27256 27752 27753 27677 27675 27678 27679 27680  27332  27513  27706)


# things2do=(prep4rerun)
# things2do=(prelim_qc)
# things2do=(cleanP4)
# things2do=(circle)
# things2do=(circle run_hacksaw_qc_only)
things2do=(final)

###
### choose how to run: for selected dirs, uncomment jName/jID; for the whole group, use $PACBIO_LOCAL/s* 
###


# for ((i=0; i < ${#jName[@]} ; i++)); do
# # for ((i=0; i < 1 ; i++)); do
# # for ((i=1; i < ${#jName[@]} ; i++)); do
	# LOC_NAME=${jName[$i]}
	# PB_JOB_ID=${jID[$i]}
	# DIRTY_DIR=$PACBIO_LOCAL/$LOC_NAME/0$PB_JOB_ID
	# CLEAN_DIR=$DIRTY_DIR-cleanP4
	# printf "\n\n\n%d\n%d\n%s\n%s\n\n\n" $i $PB_JOB_ID $DIRTY_DIR $CLEAN_DIR
	# if [ ! -d $CLEAN_DIR ]; then
		# mkdir -p -m 775 $CLEAN_DIR 
	# fi
	# cd $CLEAN_DIR

for ((i=0; i < ${#DIR2RERUN[@]} ; i++)); do
# for ((i=0; i < 1 ; i++)); do
# for ((i=1; i < ${#jName[@]} ; i++)); do
	CLEAN_DIR=${DIR2RERUN[$i]}
	printf "\n\n\n%d\n%s\n\n\n" $i $CLEAN_DIR
	if [ ! -d $CLEAN_DIR ]; then
		printf "\n!! Wrong path: %s\n-skipping to next\n\n\n" $CLEAN_DIR | tee -a $BIG_LOG
		continue
	fi
	cd $CLEAN_DIR	
	printf "Starting thigs2do in %s\n\n" `pwd` | tee -a $BIG_LOG
	
	for ((j=0; j < ${#things2do[@]} ; j++)); do
		todo=${things2do[$j]}
		echo $todo 
		cd $CLEAN_DIR
		case $todo in
		'cleanP4')
			SCRIPT2RUN=$VERSION2RUN/pb.cleanp4.sh
			WORK_DIR=$CLEAN_DIR
			cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			$SCRIPT2RUN	2>&1 | tee -a $BIG_LOG		
			;;
		'prep4rerun')
			printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd`
			if [ -d $CLEAN_DIR/assemblyQC ]; then mv $CLEAN_DIR/assemblyQC $CLEAN_DIR/assemblyQC.old ; fi
			if [ -d $CLEAN_DIR/circularize ];then mv $CLEAN_DIR/circularize $CLEAN_DIR/circularize.old;fi
			if [ -d $CLEAN_DIR/final ]; then mv $CLEAN_DIR/final $CLEAN_DIR/final.old; fi
			if [ -d $CLEAN_DIR/prelimQC ]; then mv $CLEAN_DIR/prelimQC $CLEAN_DIR/prelimQC.old; fi
			mkdir '.old' && mv -R *old ./.old
			;;
		'prelim_qc')
			SCRIPT2RUN=$VERSION2RUN/pb.prelim_qc.sh
			WORK_DIR=$CLEAN_DIR/prelimQC		
			if [ ! -d $WORK_DIR ]; then mkdir $WORK_DIR ; fi
			cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG
			;;
		'circle')
			SCRIPT2RUN=$VERSION2RUN/pb.circle.sh
			WORK_DIR=$CLEAN_DIR/circularize
			if [ ! -d $WORK_DIR ]; then mkdir -m 775 $WORK_DIR ; fi
			cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG
			;;
		'run_hacksaw_qc_only')
			SCRIPT2RUN=$VERSION2RUN/pb.hacksaw_qc_only.sh
			WORK_DIR=$CLEAN_DIR
			cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG &
			;;
		'my_qc')
			SCRIPT2RUN=$VERSION2RUN/pb.myqc.sh
			WORK_DIR=$CLEAN_DIR/myQC
			if [ ! -d $WORK_DIR ]; then  mkdir -m 775 $WORK_DIR ; fi
			cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG
			$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG
			;;
		'final')
			SCRIPT2RUN=$VERSION2RUN/pb.hacklinks.sh
			WORK_DIR=$CLEAN_DIR/final
			if [ -d $WORK_DIR ]; then 
				cd $WORK_DIR &&  printf "Starting %s in WORK_DIR %s\n\n"  $todo `pwd` | tee -a $BIG_LOG 
				$SCRIPT2RUN 2>&1 | tee -a $BIG_LOG
			else
				printf "\n\nFAILED %s: %s is not a valid dir\n\n"  $todo $WORK_DIR  | tee -a $BIG_LOG 
			fi
			;;
		*)
			printf "\n\n -- !! -- No workflow for case: %s -- ?? -- \n\n" $todo | tee -a $BIG_LOG
			;;
		esac
		cd $CLEAN_DIR
		chgrp -R genome `pwd` &&  chmod -R 775 `pwd` &
	done
done



# for s_dir in $PACBIO_LOCAL/s* ; do
	# printf "checking for cleanP4:\n%s\n\n" $s_dir
	# LOC_NAME=$(basename $s_dir)	
	# CLEAN_DIR=$s_dir/*cleanP4
	# if [  ! -d $CLEAN_DIR ]; then
		# printf "\nno CLEAN_DIR: skip to next project\n\n" 
		# continue	
	# fi
	# DIRTY_DIR=$(echo $CLEAN_DIR | sed  -e "s/-cleanP4.*//")
	# PB_JOB_ID=$(echo $(basename $DIRTY_DIR) | sed 's/^0//')
	# printf "ready for things2do:\nDIRTY_DIR=%s\nCLEAN_DIR=%s\n\n" $DIRTY_DIR $CLEAN_DIR
	# cd $CLEAN_DIR
