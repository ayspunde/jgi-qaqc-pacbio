#!/bin/bash

########### TASK metadata #############
# Task            : getChemistry
# Module          : P_Fetch
# TaskType        : None
# URL             : task://028069/P_Fetch/getChemistry
# createdAt       : 2014-04-10 15:59:17.432421
# createdAt (UTC) : 2014-04-10 22:59:17.432460
# ncmds           : 1
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_Fetch/getChemistry.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_Fetch/getChemistry.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/chemistry_mapping.xml
#
########### END TASK metadata #############

# Writing to log file
cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_Fetch/getChemistry.sh >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_Fetch/getChemistry.log;



echo "Running task://028069/P_Fetch/getChemistry on $(uname -a)"

echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/input.fofn.'
fi
echo 'Successfully validated input files'

# Task getChemistry commands:


# Completed writing Task getChemistry commands


# Task 1
makeChemistryMapping.py --metadata=/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/movie_metadata --output=/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/data/chemistry_mapping.xml --mapping_xml=/global/projectb/shared/software/smrtanalysis/2.1.1/current/analysis/etc/algorithm_parameters/2013-09/mapping.xml || exit $?
echo "Task 1 completed at $(date)"



echo "Finished on $(date -u)"
# Success
exit 0
