#!/usr/bin/env bash
## ayspunde 
## 20213-10-10: Checking for 4kb-control sequence


PB_PILOT='/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Pilot'
PB_ADAPTERS='/global/projectb/scratch/ayspunde/qc/impQD/PacBio/pb_adapters.fa'
SPIKE_IN='/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Pilot/ref/spike/4kb_Control.fasta'
BBMAP='/global/projectb/sandbox/gaag/bbtools/mapPacBio.sh'
SUMMARY_TAB=$PB_PILOT/4kb_check.131010.tab

cd $PB_PILOT
# printf "dir\tnCorrectReadsWithAdapterSeq\tnContigsWithAdapterSeq\n" >  $SUMMARY_TAB
# printf `date` > $PB_PILOT/adapter.duk.log

for dd in $PB_PILOT/[st]*/*/final; do
    echo $dd; cd $dd
    
    if [ ! -e *.finalReport.pdf ]; then
        printf "%s: no PDF, continue without counting\n" $dd
        continue
    fi
    
    mkdir -m 775 myQC; cd myQC
    # rm -f *
    
    toCheck='polished_assembly'
    if [ ! -e ../$toCheck.fasta ]; then
        gunzip -c ../$toCheck.fasta.gz > $toCheck.fasta
    else
        ln -s ../$toCheck.fasta $toCheck.fasta
    fi
    if [ -e $toCheck.fasta -a ! -e  megablast.$toCheck*parsed ]; then
        echo "megablast @ $toCheck: starting "
        run_megablast.sh $toCheck.fasta $SPIKE_IN -p 90
        # stats_$toCheck=$(pwd)/duk.stats.$toCheck.txt
        # $BBMAP qin=33 in=$toCheck.fasta ref=$SPIKE_IN outm=bbmap.$toCheck.outm scafstats=bbmap.$toCheck.scaff.stats refstats=bbmap.$toCheck.ref.stats overwrite=t >> bb.log
    else
        echo "megablast @ $toCheck: already exists "
    fi
 
    toCheck='filtered_subreads'
    if [ ! -e ../$toCheck.fasta ]; then
        gunzip -c ../$toCheck.fasta.gz > $toCheck.fasta
    else
        ln -s ../$toCheck.fasta $toCheck.fasta
    fi
    if [ -e $toCheck.fasta -a ! -e  megablast.$toCheck*parsed ]; then
        echo "megablast @ $toCheck: starting "
        run_megablast.sh $toCheck.fasta $SPIKE_IN -p 90
        # stats_$toCheck=$(pwd)/duk.stats.$toCheck.txt
        # $BBMAP qin=33 in=$toCheck.fasta ref=$SPIKE_IN outm=bbmap.$toCheck.outm scafstats=bbmap.$toCheck.scaff.stats refstats=bbmap.$toCheck.ref.stats overwrite=t >> bb.log
    else
        echo "megablast @ $toCheck: already exists "
    fi
    
    # echo "Starting BLSR alignment.."
    # for hh in $dd/*h5 ; do
        # ll $hh
        # blasr $hh $SPIKE_IN -bestn 1 -header >> h5.spike.alignment.txt
    # done
    
    
    chgrp -R genome `pwd` ; chmod -R 775 `pwd`
    # break

done
  
  
    # # # corr_fasta=$dd/corrected.fasta
    # # # final_contigs=$d/polished_assembly.fasta.gz
    # # # if [ -f $corr_fasta  ]; then
        # # # printf "Got corr reads: %s\n" $d >> adapter.duk.log
        # # # stats=$d/duk.stats.corr_reads.txt
        # # # /global/projectb/sandbox/gaag/bbtools/bbduk.sh qin=33 in=$corr_fasta ref=$pb_adapters out=dump.fa stats=$stats overwrite=t >> adapter.duk.log
        # # # if [ ! -e $stats ]; then nAdaptCorrReads='0'
        # # # else nAdaptCorrReads=$(cat $stats | awk -F'\t' '{SUM +=$2} END {print SUM}')
        # # # fi
    # # # else
        # # # printf "No corr reads?: %s\n" $d >> adapter.duk.log
    # # # fi
    # # # if [ -f $final_contigs  ]; then
        # # # printf "Got contigs: %s\n" $d >> adapter.duk.log
        # # # stats=$d/duk.stats.final_contigs.txt
        # # # /global/projectb/sandbox/gaag/bbtools/bbduk.sh qin=33 in=$final_contigs ref=$pb_adapters out=dump.fa stats=$stats overwrite=t >> adapter.duk.log
        # # # if [ ! -e $stats ]; then nAdaptContig='0'
        # # # else nAdaptContig=$(cat $stats | awk -F'\t' '{SUM +=$2} END {print SUM}')
        # # # fi
    # # # else
        # # # printf "No contigs?: %s\n" $d >> adapter.duk.log
    # # # fi  
    # # # # clean up from previous check
    # # # rm $d/mapped*fa
    # # # printf "%s\t%d\t%d\n" $d $nAdaptCorrReads $nAdaptContig >> adapter_check.130924.tab
    # # break
# # done

# ## 2013-10-09: checking for the spikes
# # module load qaqc
# # module load smrtanalysis
# # module load blast
# PBPILOT=/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Pilot
# SPIKE_IN='/global/projectb/scratch/ayspunde/qc/impQD/PacBio/Pilot/ref/spike/4kb_Control.fasta'
# cd $PBPILOT
# for dd in $PBPILOT/[st]*/*/final; do 
    # echo $dd; cd $dd
    # tDIR='myQC'
    # mkdir -m 775 $myQC; cd $myQC
    # if [ ! -e ../polished_assembly.fasta ]; then
        # gunzip -c ../polished_assembly.fasta.gz > polished_assembly.fasta
    # else
        # ln -s ../polished_assembly.fasta polished_assembly.fasta
        # run_megablast.sh polished_assembly.fasta $SPIKE_IN -p 95
    # dones
    # if [ ! -e ../filtered_subreads.fasta ]; then
        # gunzip -c ../filtered.fasta.gz > filtered.fasta
    # else
        # ln -s ../filtered_subreads.fasta filtered_subreads.fasta 
        # run_megablast.sh filtered_subreads.fasta $SPIKE_IN -p 95
    # done
    # # echo "Starting BLSR alignment.."
    # # # printf "%s\n" $(pwd) > h5.spike.alignment.txt
    # # for hh in $(pwd)/*h5 ; do
        # # ll $hh
        # # blasr $baxh5 $SPIKE_IN -bestn 1 -header >> h5.spike.alignment.txt
    # # done
    # chgrp -R genome .. ; chmod -R 775 ..
# done
