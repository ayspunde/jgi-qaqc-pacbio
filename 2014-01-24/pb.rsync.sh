#!/bin/bash

# ayspunde 2014-01-06

# rsync successful SMRT portal jobs to scratch:
## /global/projectb/sandbox/rqc/ayspunde/pacbio/2014-01-24/pb.rsync.sh
# to clean P4 spike-in call pb.cleanp4.sh
# to kick-in hacksaw call pb.hacksaw.sh

## change these:
# jName - my own ID(s) for the organism (derived from JIRA ticket N); free-form
# jID - jobID from SMRT Portal, exact

# jName=(s370  s378  s749  s754  s755  s757  s758  s760  s764   s861   s1043  s940 )
# jID=(  27349 27256 27752 27753 27677 27675 27678 27679 27680  27332  27513  27706)

# jName=(s750 s1011 s1041 s1064 s1065 s1066 s1067 s1068  s805  s973  s975)
# jID=( 27800 27731 27713 27714 27715 27716 27717 27718 27537 27721 27730) 

# # 2014-02-26
# jName=(s703   s703  s735  s803   s804  s930   s972)
# jID=(  27313  27351 27390 27539  27538 28036  27708) 

# 2014-03-03
jName=(s781  s781  s782  s782  s783  s860  s860  s929  s929  s934  s934  s934  s970  s970  s972  s972  s974  s974  s975  s1209 s1209 s1210 s1210 s1225 s1225 s1225 s1286 s1286 s1312 s1312)
jID=(  28044 28045 28124 28048 28051 28057 28058 28055 28056 28099 28100 28101 28097 28098 28060 28061 28092 28090 28127 28069 28070 28071 28072 28102 28103 28104 28108 28110 28111 28112)

# PACBIO_LOCAL is your working dir
PACBIO_PORTAL='/global/projectb/shared/pacbio/jobs'
# PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc1'
# PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc2'
PACBIO_LOCAL='/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc3'

for ((i=0; i < ${#jName[@]} ; i++)); do
    LOC_NAME=${jName[$i]}
    PROJECT_DIR=$PACBIO_LOCAL/$LOC_NAME
    PB_JOB_ID=${jID[$i]}
    echo $i $PROJECT_DIR $PB_JOB_ID
    if [ -d $PROJECT_DIR ]; then printf "$PROJECT_DIR already exits: ready for rsync\n"
    else mkdir -m 775 -p $PROJECT_DIR
    fi
    
    cd $PROJECT_DIR
    pwd
    L1NN=$(echo $PB_JOB_ID | sed 's/\([0-9]\{2\}\).*/\1/')
    PORTAL_JOB=$PACBIO_PORTAL/0$L1NN/0${PB_JOB_ID}
    if [ -d $PORTAL_JOB ]; then
        printf "$PORTAL_JOB\n\n"
        printf "$PORTAL_JOB\n\n" > $(basename $PORTAL_JOB).rsync.log
        rsync -vr $PORTAL_JOB ./ >> $(basename $PORTAL_JOB).rsync.log &
    fi
    cd ..
done
