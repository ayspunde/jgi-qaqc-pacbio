#!/bin/bash

########### TASK metadata #############
# Task            : consensus_003of007
# Module          : P_PreAssembler
# TaskType        : None
# URL             : task://028069/P_PreAssembler/consensus_003of007
# createdAt       : 2014-03-19 16:22:07.412199
# createdAt (UTC) : 2014-03-19 23:22:07.412224
# ncmds           : 1
# LogPath         : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_PreAssembler/consensus_003of007.log
# Script Path     : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_PreAssembler/consensus_003of007.sh
#
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.chunk003of007.ids
# Input       : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/corrected.bnk
# Output      : /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/consensus.chunk003of007.ctg
#
########### END TASK metadata #############

# Writing to log file
cat /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/workflow/P_PreAssembler/consensus_003of007.sh >> /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/log/P_PreAssembler/consensus_003of007.log;



echo "Running task://028069/P_PreAssembler/consensus_003of007 on $(uname -a)"

echo "Started on $(date -u)"
echo 'Validating existence of Input Files'
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.chunk003of007.ids ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.chunk003of007.ids'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.chunk003of007.ids.'
fi
if [ -e /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/corrected.bnk ]
then
echo 'Successfully found /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/corrected.bnk'
else
echo 'WARNING: Unable to find necessary input file, or dir /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/corrected.bnk.'
fi
echo 'Successfully validated input files'

# Task consensus_003of007 commands:


# Completed writing Task consensus_003of007 commands


# Task 1
make-consensus -A -b /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/corrected.bnk -i /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/layout.chunk003of007.ids -L > /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc31/s1209/028069-cleanP4/consensus.chunk003of007.ctg || exit $?
echo "Task 1 completed at $(date)"



echo "Finished on $(date -u)"
# Success
exit 0
